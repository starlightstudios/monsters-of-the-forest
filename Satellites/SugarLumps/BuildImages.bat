SugarLumps.exe -Redirect "BuildImagesLog.txt" -Call BuildImages.lua
cd Output/
if exist "MOTF.slf" (
	copy "MOTF.slf" "../../../Games/MonstersOfTheForest/Data/MOTF.slf"
	rm "MOTF.slf" 
	echo "Copying MOTF.slf"
)