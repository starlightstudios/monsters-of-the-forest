--[Characters]
--Character portraits.
local sBasePath = fnResolvePath()
local ciNoTransparencies = 16

--Setup
ImageLump_SetCompression(1)
local saEmotions = {}

--Grid positioning
local iGridMaxX = 4
local iGridMaxY = 3
local iaGridX = {3, 703, 1403, 2103}
local iaGridY = {3, 771, 1539}
local iGridW = 694
local iGridH = 762

--[Characters]
--For now, everyone only has a neutral portrait.
ImageLump_Rip("Characters|Alonso|Neutral",     sBasePath .. "Alonso.png",     iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Antgirl|Neutral",    sBasePath .. "Antgirl.png",    iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Cohen|Neutral",      sBasePath .. "Cohen.png",      iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Frederick|Neutral",  sBasePath .. "Frederick.png",  iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Guard|Neutral",      sBasePath .. "Guard.png",      iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Isaac|Neutral",      sBasePath .. "Isaac.png",      iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Ron|Neutral",        sBasePath .. "Ron.png",        iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)
ImageLump_Rip("Characters|Spidergirl|Neutral", sBasePath .. "Spidergirl.png", iaGridX[1], iaGridY[1], iGridW, iGridH, ciNoTransparencies)

--Irene, being the main character has more portraits.
local sCharacterName = "Irene"
local sCharacterPath = sBasePath .. "Irene.png"
saEmotions[1] = {"Neutral", "Happy", "Blush", "Smirk"}
saEmotions[2] = {"Sad", "Surprised", "Offended"}
saEmotions[3] = {"Cry", "Laugh", "Angry"}
for y = 1, iGridMaxY, 1 do
    for x = 1, iGridMaxX, 1 do
        if(saEmotions[y][x] == nil or saEmotions[y][x] == "SKIP") then
            
        else
            local sEmotionName = saEmotions[y][x]
            ImageLump_Rip("Characters|" .. sCharacterName .. "|" .. sEmotionName, sCharacterPath, iaGridX[x], iaGridY[y], iGridW, iGridH, ciNoTransparencies)
        
        end
    end
end

--Finish
SLF_Close()