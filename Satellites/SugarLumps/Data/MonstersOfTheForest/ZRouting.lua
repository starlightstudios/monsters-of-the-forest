-- |[Monsters of the Forest]|
--Images specific to the Monsters of the Forest VN.
local sBasePath = fnResolvePath()
local ciNoTransparencies = 16

--Setup
SLF_Open("Output/MOTF.slf")
ImageLump_SetCompression(1)

-- |[Backgrounds]|
ImageLump_Rip("BG|ForestA", sBasePath .. "Backgrounds/ForestBackA.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestB", sBasePath .. "Backgrounds/ForestBackB.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestC", sBasePath .. "Backgrounds/ForestBackC.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestD", sBasePath .. "Backgrounds/ForestBackD.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestE", sBasePath .. "Backgrounds/ForestBackE.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestF", sBasePath .. "Backgrounds/ForestBackF.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BG|ForestG", sBasePath .. "Backgrounds/ForestBackG.png", 0, 0, -1, -1, ciNoTransparencies)

-- |[Characters]|
LM_ExecuteScript(sBasePath .. "Characters/ZRouting.lua")

--Finish
SLF_Close()