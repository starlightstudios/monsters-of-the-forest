-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "MOTF"
local sGamePath       = "Root/Paths/System/Startup/sMOTFPath" --Must exist in the C++ listing.
local sButtonText     = "Play Monsters of the Forest"
local iPriority       = gciOtherGames_Counter
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iNegativeLen    = -23 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "000 Launcher Script.lua"

--Add the game entry. Does nothing if it already exists.
fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)

--Add a new path to the game. This informs the engine where it can be launched from.
local iGameIndex = fnGetGameIndex(sGameName)
if(iGameIndex == 0) then return end
fnAddGamePath(gsaGameEntries[iGameIndex].sSearchPaths, zLaunchPath)

--Other Games Counter
gciOtherGames_Counter = gciOtherGames_Counter + 1