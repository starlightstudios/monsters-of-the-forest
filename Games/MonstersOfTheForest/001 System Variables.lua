--[System Variables]
--Creates all variables and constants that the rest of the scenario depends on. Everything should
-- be stored in gzTextVar to prevent conflicts with Adventure Mode.

--Build Functions
LM_ExecuteScript(gzMotFVar.sRootPath .. "Functions/fnCutsceneBlocker.lua")
LM_ExecuteScript(gzMotFVar.sRootPath .. "Functions/fnCutsceneWait.lua")
LM_ExecuteScript(gzMotFVar.sRootPath .. "Functions/fnInstruction.lua")

--Dependent
--LM_ExecuteScript(gzMotFVar.sRootPath .. "Functions/fnGetIndexOfItemInRoom.lua")
