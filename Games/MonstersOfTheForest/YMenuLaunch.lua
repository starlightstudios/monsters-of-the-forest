-- |[ ====================================== Menu Launcher ===================================== ]|
--Script called when this game is selected from the main menu.
local iGameIndex = fnGetGameIndex("MOTF")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Go to system boot.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Run the setup scripts.
bQuitsToMenu = true
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
