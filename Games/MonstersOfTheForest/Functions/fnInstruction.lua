--[Function: fnInstruction]
--Creates and registers a cutscene event using the provided instruction string.
--Built from: ./ZLaunch.lua
--Example: fnInstruction([[io.write("Example instruction.\n")]])
function fnInstruction(sInstruction)

	--Argument check.
	if(sInstruction == nil) then return end
	
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end