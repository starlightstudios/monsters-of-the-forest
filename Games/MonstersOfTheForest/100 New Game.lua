
--[=[
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end
]=]

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.
if(sTopicString == "Begin Scene") then
    
    --Variables.
    --variables to call: sgender, iyoufight, iantintimacy, friendsex, antdiscovery, 
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iYouFight = VM_GetVar("Root/Variables/Characters/Scenario/iYouFight", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iFriendSex = VM_GetVar("Root/Variables/Characters/Scenario/iFriendSex", "N")
    local iAntDiscovery = VM_GetVar("Root/Variables/Characters/Scenario/iAntDiscovery", "N")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: He's obviously having a less good time than Frederick.[BLOCK][CLEAR]") ]])
    if(iAntDiscovery == 1.0) then
        fnInstruction([[ WD_SetProperty("Append", "Narrator:  Alonso gives me a glance that says whichever way I vote he'll vote as well. He really doesn't care, and he still feels guily about not helping to fight the spider.[BLOCK][CLEAR]") ]])
        
    --if antdiscovery is 0
    else
        fnInstruction([[ WD_SetProperty("Append", "Narrator:  Alonso gives me a glance that says whichever way I vote he'll vote as well. He really doesn't care, and he still feels guily about having the threesome.[BLOCK][CLEAR]") ]])
    end
    
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Which way should I vote? [BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "MushroomEndings") 
  
elseif(sTopicString == "MushroomEndings") then
    WD_SetProperty("Hide")
    --rescue choices
    --Reboot the dialogue.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Vote to stay an ant-girl\", " .. sDecisionScript .. ", \"AntGirl\") ")

elseif(sTopicString == "AntGirl") then
    --Variables.
    --variables to call: sgender, iyoufight, iantintimacy, friendsex, antdiscovery, 
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iYouFight = VM_GetVar("Root/Variables/Characters/Scenario/iYouFight", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iFriendSex = VM_GetVar("Root/Variables/Characters/Scenario/iFriendSex", "N")
    local iAntDiscovery = VM_GetVar("Root/Variables/Characters/Scenario/iAntDiscovery", "N")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (For all the discomfort in becoming an ant-girl it's ultimately a blessing.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Besides being unique in the whole world is hardly a bad thing. After all everyone is.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll vote to stay an ant-girl.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I vote to remain being an ant, female following naturally. Alonso votes the same.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron looks frustrated and a little hurt. But we are team of equals, so he can't really go against it.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen looks happy, although that's more that the issue was settled than any side won. He's seen many a fellowship of adventurers torn apart by internal disagreement.[BLOCK][CLEAR]") ]])
    
--switch to roundtable mode here (what does that look like)
fnInstruction([[ WD_SetProperty("Append", "Alonso: From there life continued much as it had before we managed to have such an epic misadventure.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  We took jobs looking for money and adventure![BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We had barely managed to get above water on equipment and supplies when a number of terrifying rumors began to spread.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Entire graveyards emptied, homesteads destroyed but the crops left untouched. Above it all, a pale bone white figure wearing a crown of black.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: A lich, in our time no less. There is no greater thread to the living.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: There isn't simply not much anything like us could do against such a thing. Well besides from die I guess.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The queen put out a call for adventurers to help deal with the threat. We wanted to stay well away, it could cast spells of distingration as easily as we can drink a cup of coffee.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Cohen however was of the sort that might do something. We all begged him not to go.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  He however got a guilty look on his face. He said he owed a favor to the queen for an incident.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Something about how he got with the queen and then her daughter in short order and the queen didn't have him executed on the spot.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: At the meeting there we many adventurers we had only ever heard of in tales.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Witches and wizards I admired, including the great polaris, witch of the northern star. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Thieves who could steal your underclothes just by looking at you. Including John of the golden city who once stole the moon's song and gave it back as a gift.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Men of courage and valor who had in their own ways turned tides. Including Odd who slew a great sea serpent with only his wits and then went on to train generations of fighting men for the great tournament of arms.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: All of them were assembled to deal with the threat.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Being just apprentices we weren't in any of the important meetings. Beyond that we were still set apart by being ant-girls. Nobody had really seen ant-girl adventurers before.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: The first step Cohen claimed was to find the container that held the liches soul. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I asked how big a jar you need for a soul. It was a solid joke.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen didn't see the humor. He instead clarified that it wasn't necessarily a jar it could be any valuable object. Like a vase.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We were quickly directed to help int racking the patrols of the undead. The undead march without tiring so it took every bit of improved strength of stamina. Frederick wasn't much a help.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: Look it's not my fault I'm like super afraid of the undead okay. When I was a kid a moving corpse broke into the barn and ate livestock until stomach burst. Then it kept going until it's jaw dropped off.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: He'd start putting out a fearful scent whenever he spotted one. It wasn't hard to shush though and if anything helped us track them.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I'm glad my terror was useful.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Finally we managed to find the lich's lair. It was a horrifying tomb built into a mesa shaped like a skull.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: It was pretty cool.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Focus Ron.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: They assembled teams of adventurers to challenge the tomb and find the soul vase. Cohen volunteered to be in the first team, we worried terribly.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I remember he shook each of our hands and then said 'Younglings, I never thought I would have to say this but, there is a very good chance I am not going to come back. It has been an honor and a privilege to work with you, and I am sad I won't see the great adventurers you will grow up to be'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  He then told us where he'd hid all the treasure he hadn't found time to spend over the years.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: 12 hours later the team returned. Two irreplaceable heros were dead and the third was missing both arms past the elbow. Like they had just been pinched off.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Cohen put his hand to head and claimed that the tomb could not be plundered without incalculable loss. His only hope he felt was that the mages might have something. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I remember the look on your face Isaac/Irene.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Without looking smug I asked him 'You getting lazy old timer?' he seemed perplexed by that. So I kept going. 'It's not like you to say a problem needs wizards to solve. We have the solution right here.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: He didn't get it at first.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: What are ants known for?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Being awesome![BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: That and digging. The plateu was just made of loam. We could tunnel through it with relative ease. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen still cautioned us to be careful. The tomb was riddled with traps and we'd need to be sneaky in case the lich came back.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Still it was the best plan we had. Cohen arranged things so that a nearby city would be ready to help us destroy the soul vase.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: So we dug around the death trap. It was a backbreaking work and weeks of effort.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: But we really were built for it. our arms were strong and we can see in the dark. Beyond on that we had an excellent sense for underground spaces. It's hard to describe but we could always tell just how big the tunnel was and where it might collapse.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  We peaked in on the tomb a couple of times.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Being very careful not to reach inside because we like living.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Horrible horrible things in there. and a couple of traps. Remember they church they had in there for no reason? Or the giant lever over molten rock?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: It was bizarre. Eventually we found what we was sure was the inner sanctum. We were very sure because we had already found the fake one.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  This was a small room with the walls covered in shelves. The shelves were just stuffed with treasure and knick nacks. Which of course did a good job of hiding the soul vase.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Fucking Ron tried to look with magic for clue but all that did was make him scream constantly.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: You have no idea of the amount of danger we were in. It scares me to this day.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Once we got Ron calmed down Cohen had everyone take a knee.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: He said looking as gruff as he always did 'the mind is the adventurers most dangerous weapon. Let's think this through and figure out what the flactry actually is. What do we know?'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The dungeon had been designed to punish mistakes.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Which of course suggested that picking up the wrong thing would trip an alarm.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  From when we had peaked in, the dungeon had looked like it was designed to punish greed. So whatever the soul jar was it was going to be something that wasn't valuable, or valuable only to the lich.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen felt that whatever it was wouldn't trigger the alarm, so that an adventurer who took it by chance wouldn't destroy it to try and silence the alarm.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  It was tedious work to break open the wall in multiple places to make sure we could survey all the treasure. Finally we found three things that fit what were looking for.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  A comb, a whittling knife and a brass ring.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen produced a number of oranges and string to knock things off the shelves. I asked him why oranges and he said it so you could eat the oranges afterwards.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: He was so wise.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Being very careful not to touch anything we used hooks to put the items in bags. Then we made sure no one ever carried more than one at a time.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: After that was a race to the nearest city to destroy the damn things. If the lich ran us down death was certain.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: You all were quite hard on the horses as I call.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Sorry Ron but it really was life or death. After half a day of riding and looking over our shoulder we finally spotted the city walls of Ris.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The walls were thick, and surrounded by smoking piles of corpses. The undead had been beaten back a few times.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I personally felt a lot better once inside the walls. We were safe, we handed off the bags and watered the horses.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Then I looked to Cohen and he wasn't smiling. That shook me to my core.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Cohen had a sad smile in a calm tone he said 'You younglings love eachother like family. But I need your most solemn oath that you will do whatever it takes to prevent this city from falling. Even if it means sacrificing another.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Why do you think he asked for our oaths? Seems foolish to ask if we might say no.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: He asked because he never thought we would say no Ron. He trusted us, but wanted us to make sure we knew how dangerous it was.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  We all hesitated, our part was over we thought. Still when Cohen asks for your oath like that you consider it carefully.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We eventually agreed. So we gave him our most solumn oath.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I remember how happy he was, and how he tried not to show it. But then he grew serious. 'In 24 hours the lich will have three times the number of undead required to take this city.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I never thought it could be that bad so I said 'Then we have to flee, to stay is to die.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen wasn't having it though. 'if we try to run with the flactery we will be run down by the lich. Our best and probably only chance to end this is to ride it out.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I didn't understand what we could do in the face of odds that overwhelming.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Thankfully some of the queen's legion was stationed at the city. Even some adventurers Cohen knew. The best fighters would work to protect the temple where they were destroying the soul vase.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Our part was to help hold the walls for as long as possible. Every man woman and especially large child that could fight was drafted to help.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: After all if we failed, if the walls fell and the lich recovered the soul vase they would all die. Same for us. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen shook everyone's hand and then got to work organizing the defense. I did my best to emit calming scents and keep us all from panicking.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We looked to our arms and armor. Then a shout came up that they had spotted something from the walls.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I remember climbing up the walls. A couple of the guardsmen were passing a wine skin.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  For a short blissful minute I was confused at what I was seeing, what they had shouted about.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  It reminded me of a seething mass of dark insects as seen from a distance. A great swarm drawing ever closer, except not on the ground but in the fields surrounding the city.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  And then I realized, they weren't bugs. Each bit of the seething mass was a corpse, a former person. I thought I knew fear, or had known true fear. I was mistaken. To see a horde of corpses far larger than any host of living I had ever seen shook me to my core.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  The mass stretched in every direction as far as I could see. The Lich's horde was without end.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I don't remember much after that. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Yeah I was right behind you, you dropped to your knees and we all moved to comfort you. Fear was coming off of Frederick like steam off of a pot.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I think I made a joke about Frederick curing his fear of the undead?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: You might of.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: As far as I recall Frederick didn't say anything. Just got up and drained the wine flask.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The undead came on like an endless rainstorm. They didn't need strategy or tactics, they simply clambered over one another to attack the defenders like a flood. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I saw many a man dragged down and devoured on the spot. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Our group proved pretty effective actually. We had great teamwork and we didn't tire easily. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I remember you spent most of your time trying to keep Frederick from losing it completely or Ron from using all of his magic at once. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Even without a member Ron still tends to be a bit premature.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Hey![BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The lich's plan was apparent from the walls. They would exhaust and spread out our defenses as much as possible. Then the lich would punch through and recover the soul vase by itself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Plan mostly worked. The undead just came and came and came.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I blunted a baker's dozen of swords on the undead. I eventually switched to using a sharpend shovel just to spare myself the trouble.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Frederick did surprisingly well for someone out of his mind with fear. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: He had that manic energy, and once stopped screaming constantly he managed to not run out of breath so often.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Yep super don't remember.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We were rotated on and off the walls reguarly, often assigned to where the defenders were having a hard time.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We spent just about every moment we weren't fighting resting in a heap.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Those who couldn't fight brought us meals and warm drinks. It was really nice of them. We became something of a curiousity as I recall.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I was really popular among the children for some reason. They all wanted to to feel my chitin or play mulitple games of play rock paper scissors at once.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I also had to give a surprising number of shoulder rides. You don't see that many children in adventuring, even less when spending weeks digging.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: To see them made me wonder if I could have children. Your average regular ant can't but that's what queens are for.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We held for a day and half. But then a signal fire was lit upon the keep. The wall had been breached and we had to withdraw to prepared barricades.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I had a good time setting the walls on fire as we withdrew.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Our group was called to hold the town's main road. We lost Ron though, his magic would help protect the flactery against the lich.[BLOCK][CLEAR]") ]])
--remove Ron here
fnInstruction([[ WD_SetProperty("Append", "Irene: It was like a leak in a dike. The undead poured in like a rotting hungry tide.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: I remember looking up and briefly seeing past all the smoke a skeletal figure against the moon. It sent a shiver down my spine.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Then I looked at the oncoming dead and I had shivers all over.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Every who could swing a stick manned the barricades. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The fighting in the forest was hard, the fighting on the walls was long but this was chaos.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The mob of civilians would swing and step back. The undead would flow forward. We would set more of the city on fire to buy space. Eventualy most of the city past the barricades were on fire.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We'd kill undead only to have the mob step back. We'd drive the mob forward only to have men devoured and for them to lose cohesion.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: It took every bit of scent communication to keep the mob coordinated enough to avoid routing. I can't image how bad it was elsewhere.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Then the sound and flashes of light started. High level magic makes a particular kind of pearling thunder when it's fired. All we could do is hope Ron was okay.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: When I'd finally gotten the barricade stabilized, and managed to force the undead back a bit an umber scent drifted through the air.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Ron needed help, and if he needed help things were verging on disaster.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: There wasn't anything to do, if I wasn't making sure the barricade held it would fall and we would all die.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Without us there though, he probably wouldn't survive himself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Me and frederick gave a wordless nod and then ran through the deserted streets to where the soul vase was being destroyed.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  My memory starts to come back here. You were ahead of me and ran around the corner before I did. Then you jumped back and threw up.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Yep. There was square in front of the temple. The square was coated in the remains of near on a 100 men. They were all smears and spare limbs. Like something had simply popped them apart.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: Across the square the lich was dueling the defenders. Not of a much of a duel though. The lich floated up above the carnage as all kind of magic splitted around him.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Then he'd snap his fingers and a wave of destruction would flow towards the defenders. A mage would fall dead and the cycle would repeat.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: It was way above our paygrade. Going into the square was suicide.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We were both giving off fear like nothing else. But I took deep breath and focused on finding Cohen. He had to know what to do.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  That's not quite how I remember it. Anyway we tracked the umber scent to a ruined and smoldering building. It was like some had ripped out the guts of the building and spilled them all over the street.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Ron and Cohen were sheltering there. Ron ran forward and hugged both of us when he saw us.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: It got bad. It got really bad.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen just had a tired smile and said 'You came. It's not looking good, we need to buy time.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  So much had gone wrong. So I was frank with Cohen. 'You didn't bring us here to die did you?'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen wasn't phased though. 'No, never.' He had plan but it was desperate. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: He was holding a simple shaft of willow. For a moment it reminded me of a child's favorite wand.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Cohen continued confident. 'This is a wand of dispel magic. If it touches the lich it will dispel enough of his magic to make him retreat. Hard part is getting it there.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  He handed me the wand and I hefted it a couple of times. Then he asked me 'Think you can throw that?'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I could of course. But a throw like that you have to be inside 15 paces to make sure it hits. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen drew the plan in the smoot. It was a simple cover play. endgame was Frederick making the throw. Ron engages from the building line.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Once the lich had his attention turned, frederick makes the run from behind and someone else makes it from the front. Front runner's job is to buy Frederick as much time as possible.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Downside was that the front runner was going to die. Ron too probably.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I think Cohen planned to volunteer. He was sniffing the air and said 'I'm old, so I'll make the run. It's be-'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I cut him off. I was going to make the run, I'm the toughest so it just made sense. Besides if it didn't work he had to manage the defense of the keep.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  He had the proudest smile I ever saw. He said that he'd take as many of them with him as he could.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I had a secret weapon, with our antenna Ron could give me a hint about what the lich was going to do.[BLOCK][CLEAR]") ]]) 
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Ron wasn't sure it was faster than the lich's magic though.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I didn't think I was going to live that long anyway.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: We all looked to frederick and Cohen said 'Don't miss'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I was nervous.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Anxiety was coming off of frederick like a pot, and he was shaking from head to toe.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: So I got Ron's attention and began to emit scents of calm confidence. Ron got the hint and echoed my scents.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: It almost worked, but Frederick was still shaking. I didn't know you could until I underlied the scents with a complusion to buck up and calm down.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  It was so strange when it worked. I could almost feel the calm flow into me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: We all shook hands. I wasn't happy with it but what could we do?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen ruffled my hair, and then told me in his most serious voice 'Give them hell youngling.' Then I nodded at Frederick.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  I nodded back, I wasn't going to miss that throw if it killed me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Then we split. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I picked my way through the ruined buildings. Then I found a good spot with a straight shot to the lich and took a deep breath.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Then a sad blue scent drifted through the air.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I sprinted as fast I dared through the carnage. It was very slick. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I heard a noise to my side as Ron cast a spell to get the Lich's attention. Cohen shouted something foul to help distract. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: They both ducked back into cover as waves of death flowed towards them. I shouted myself to get the lich's attention. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The lich looked at me with it's empty eye sockets and then I knew was going to die. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: A deep purple scent came across my vision and I dove to your left. The corpses where I was standing explode in a red spray. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I'm on my feet again as fast as I can be. The lich makes ready to snap it's fingers again. A scent with strong geel of direction hits me. Left? Right?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Right right right! I step to the right without a second to spare. there is a rush of air to my left and a glance down showed my sleeve had been clipped. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I reach striking distance, adrenaline pumping through every inch of my body. If I can just cover these last few feel I'll finally be able to hit the lich.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The lich snapped both of it's fingers, bringing up it's hands as it does so. There might of been a safe space between it's hands but I wasn't fast enough.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: There was an immense pain in both my right arms and right leg. Things become unreal. I heard something hit the ground. Feeling distracted I glanced down. It was both my right arms. My right arms now ended just past my shoulder.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I feel forward unable to stand. The pain was immense. I could dimly hear a hollow sound, something must of hit the lich. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The brown stuff that I used for blood flowed freely. I thought briefly of trying to stop the flow, but I was so tired and it probably wouldn't of made a damn bit of difference.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The last thing I saw was Frederick's panicking face.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Then darkness closed in.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator:  The city fell, two years later the kingdom. After a campaign of many years the neighboring countries manage to isolate and destroy the lich's horde.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator:  A wave of human settlers gathers, hoping to settle lands untouched by human hands for nearly a decade.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator:  They are however repulsed by a unified coalition of monsters. After suriving the lich's attacks for so long, the battered human armies are no match.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator:  A unified nation of monsters arises, sending a beacon throughout the world.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator:  Ending: a noble pointless sacrifice[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: .....[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Nah I'm just pulling your leg. I couldn't help myself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I awoke quite surprised. It took me a second to realize what had me so surprised.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I wasn't dead! [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I woke up on a rough bed in a cramped room, and next to me was Ron reading his book.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I'd never seen someone wake up surprised before. It was quite surprising.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Like you noticed, you were busy reading. I had to clear my throat twice. You were very surprised.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I was getting little worried. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Awww[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Yeah worried that you were being lazy.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Hehe. I immediately asked what happened after I passed out.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: The lich just fell over damnedest thing I ever saw. You also kept glancing towards your right side.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I think you rolled your eyes but you really can't tell when an ant-girl rolls their eyes.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I pulled the sheet back, for someone bold enough to charge a lich you can be quit the ehicken.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: My limbs were still attached, I examined them to make sure. THere was a silvery line where they had popped off before.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: That's well outside of what Ron can do. So I asked him what happened.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I can do healing magic! Just not you know as well as fire magic.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: still you picked a good place to lose some limbs. The were healers among the people defending the temple. You lost a lot of blood though.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: It was touch and go. I was worried.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Then I asked what happened after the lich's soul vase was destroyed. I never forget your delivery Ron.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I said it's neat that when the master of undead is destroyed they all go feral.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: You said it so nonchalantly, like you were learning a nursury rhyme.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Anyway with the lich dead the undead lost all cohesion, becoming little more than ravenous horde. So we evacuated everyone back into the keep.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen must of seen this coming as he had also evacuated all of the beer in the town into the keep as well.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I was the guest of honor at the drinking party that ensued. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: It was good that we had beer because watching the horde begin to devour itself inside the city was not a pretty sight. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Still waiting for rescue plasterd beats waiting for rescue sober.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: After about two weeks the queen's legion finally managed to cut a path to the keep.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Partying is great, partying for two weeks to not think about nearby death less fun.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: from there we were whisked away with the other defenders to the capital.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We got a parade with a standing ovation. I always hoped for such things when I became an adventurer. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  It was a lot for some former farm boys. We took it with our usual magnanmity. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: We were considered great heroes for penetrating the horrifying tomb. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: And our bravery in holding the city itself. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: They held a great banquet with the queen in attendance. We got to sit at the same table as the queen in recognition of our achievements.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: As I recall Cohen spent most of it making eyes at the queen.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  She was making them right back surprisingly.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: That dinner was interesting because Frederick managed to figure out how to send images via scent. We used it responsibly of course.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Definitely not to send what figured the various nobles looked like in various states of undress.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: Definitely. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: After that we got to sleep in the castle.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The beds were frighteningly comfortable. Like they cut the wings off harpies.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: The next morning Cohen came in the room looking unspeakably smug. He gathered us all around and then had a serious talk.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: He had that proud smile 'Alright younglings. I had a talk with the queen, and she is going to offer you a boon.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Real adventurer stuff! We were all excited. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: He kept going saying 'What she wants you to say is that you want a place to call your own. A city to work out of. Then she will retain you as knights of the realm.'[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: I was worried that meant we were going boring and legit.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen insisted that it didn't mean that. Just meant that the queen would call on us to defend the realm from time to time. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: He also added that if we got bored we should just skip the civilized lands for a while.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: In a twist the city we were to work out of was the city of Ris itself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Which was kind of awkward because two thirds of the city were destroyed and we were kind of responsible for that. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: At least it wasn't just me this time.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick: Still we were considered the saviors of the city having bought the time required to save it. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: They men and women began to dye their arms brown in a sign of respect. I never thought that I'd deserve such a thing.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We got to work rebuilding the city, four tireless arms certainly helped. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: From there we began to quest out of the city, doing our best to keep it safe.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We broke up undead encampments, discouraged trolls from paying a visit, and helped to move a nest of harpies.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: In time the work became too much for just us four. So we decided to take on more hands. This of course was my department so I ordered an expedition to the forest.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We made a sport of it. Searching for the mushrooms and teaching the spiders why they shouldn't of messed with some well armed ant-girls.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: They had it coming.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Ron did eventually manage to recreate the effects of the white fluid. Did blow a few holes in his wizards tower though.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I was experimenting! That's what you do as a wizard! Even an ant-girl wizard![BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene:The first person we welcomed to our ranks had helped Alonso hold the barricades. He was a good friend to us all.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: We stood around him as he drank the potion and then watched over him when he changed.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: When he stood up again on unsteady legs he was sister, an equal amongst us. We took his hands and smiled.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: I felt a curious feeling of fulfillment in making another sister for some reason.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  The next evening Cohen gathered all of us around. He said that we had become adventurers we could be proud of and that he was off to find other adventurers.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: That man's first love always was adventure, and his second just the act of travel.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We were all still depressed to see him go. We missed him a lot.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: He told me once that he was proud of what I'd accomplished in finding a way to turn others into ants. I always kept that with me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: Still we weren't worried about him. I once saw him kill a man with his bear hands.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  Watching him get the bear hands was the frightening part.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: In time our order became one of ant-knights going onto great success. We established branches throughout the kingdom. [BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: The kingdom became known for it's ant-knights. The queen even bestowed us the honor of serving in her personal guard. We started as farm boys and made the royal guard![BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Frederick:  She thought that since we were all women we couldn't get up too much trouble. We didn't correct her of this notion.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: For some reason I always felt a deep pride and satisifaction to see so many of my sisters about.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Ron: I had planned at first to return to normal, but in time I grew to value the companionship of my sisters more than needing to fit in. Ants may be apart but we are rarely alone.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: We look forward to our next adventure. I know Cohen would be proud of what we have done.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The End (Myrmidons!)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    fnInstruction([[ MapM_BackToTitle() ]])
  end
  











--[=[
--[New Game]
--Launches a new game. Resets all variables and builds the VN.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[New Game]
--Default starting argument.
if(sTopicString == "New Game") then

    --Create an AdventureLevel. It has no data but allows everything else into the rendering pipeline.
    AL_Create()

    --Wait a bit.
    fnCutsceneWait(15)
    fnCutsceneBlocker()

    --Launch the dialogue, set it to Visual Novel mode.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Let me tell you of the days of high adventure.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: When the saga of your origins is written will you be remembered as a boy or as a girl?[BLOCK]") ]])

    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Boy\", " .. sDecisionScript .. ", \"Boy\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Girl\",  " .. sDecisionScript .. ", \"Girl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"You tell me\",  " .. sDecisionScript .. ", \"Wildcard\") ")
    fnCutsceneBlocker()

--[Decision: Repeat]
--Allows the player to retry if they picked the wrong decision, like an idiot.
elseif(sTopicString == "Repeat") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Dialogue.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: When the saga of your origins is written will you be remembered as a boy or as a girl?[BLOCK]") ]])

    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Boy\", " .. sDecisionScript .. ", \"Boy\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Girl\",  " .. sDecisionScript .. ", \"Girl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"You tell me\",  " .. sDecisionScript .. ", \"Wildcard\") ")
    fnCutsceneBlocker()

--[Decision: Boy]
elseif(sTopicString == "Boy") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: You are a boy. Is this correct?[BLOCK]") ]])

    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ConfirmBoy\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Repeat\") ")
    fnCutsceneBlocker()

--[Decision: Confirm Boy]
elseif(sTopicString == "ConfirmBoy") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Variables.
    VM_SetVar("Root/Variables/Characters/Player/sGender", "S", "Boy")
    
    --Change Irene's properties to Isaac's properties.
    WD_SetProperty("Add Name Remap", "Irene", "Isaac")
    WD_SetProperty("Register Voice", "Irene", "MOTF|Voice|IreneM")
    
    --Change the portraits to the Isaac versions. Only neutral is present.
	DialogueActor_Push("Irene")
        for i = 1, #gsaEmotions, 1 do
            DialogueActor_SetProperty("Add Emotion", gsaEmotions[i], "Root/Images/MOTF/Characters/Isaac|Neutral", true)
        end
    DL_PopActiveObject()
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Actor In Slot", 0, "Irene", "Neutral") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHARTIME|Irene|Middle|TELEPORT|1]Very well.[SOFTBLOCK] Your name is Isaac.[BLOCK][CLEAR]") ]])
    
    --Run the script again, but with "Resume". This merges the story sections together.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "Begin Scene")

--[Decision: Girl]
elseif(sTopicString == "Girl") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: You are a girl. Is this correct?[BLOCK]") ]])

    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Yes\", " .. sDecisionScript .. ", \"ConfirmGirl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"No\",  " .. sDecisionScript .. ", \"Repeat\") ")
    fnCutsceneBlocker()

--[Decision: Confirm Boy]
elseif(sTopicString == "ConfirmGirl") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Variables.
    VM_SetVar("Root/Variables/Characters/Player/sGender", "S", "Girl")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Actor In Slot", 0, "Irene", "Neutral") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHARTIME|Irene|Middle|TELEPORT|1]Very well. Your name is Irene.[BLOCK][CLEAR]") ]])
    
    --Run the script again, but with "Resume". This merges the story sections together.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "Begin Scene")

--[Decision: Wildcard]
--Randomly decides the player's gender.
elseif(sTopicString == "Wildcard") then

    --There is a 50% chance for each case.
    if(LM_GetRandomNumber(1, 100) <= 50) then
        LM_ExecuteScript(LM_GetCallStack(0), "ConfirmBoy")
    else
        LM_ExecuteScript(LM_GetCallStack(0), "ConfirmGirl")
    end

end
]=]
