-- |[Mister Pixel 10 Kerning]|
--Sets the kerning values for the listed font.

--Set the main scaler.
SugarFont_SetKerning(1.0)

--Letters groupings.
SugarFont_SetKerning(string.byte("i"), -1, 2.0)
SugarFont_SetKerning(-1, string.byte("i"), 1.0)

--Punctuation groupings.
SugarFont_SetKerning(-1, string.byte(","), 2.0)
SugarFont_SetKerning(-1, string.byte("."), 2.0)
SugarFont_SetKerning(-1, string.byte("'"), 2.0)
SugarFont_SetKerning(-1, string.byte(":"), 2.0)
SugarFont_SetKerning(-1, string.byte("!"), 2.0)
SugarFont_SetKerning(string.byte("'"), -1, 2.0)
SugarFont_SetKerning(string.byte(" "), string.byte("'"), 6.0)

--Specific punctuations.
--SugarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

--Numbers.
--SugarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

--Specific letters.
SugarFont_SetKerning(string.byte("a"), string.byte("i"), 0.0)
SugarFont_SetKerning(string.byte("a"), string.byte("t"), 1.0)
SugarFont_SetKerning(string.byte("A"), string.byte("r"), 0.0)
SugarFont_SetKerning(string.byte("A"), string.byte("t"), 2.0)
SugarFont_SetKerning(string.byte("c"), string.byte("t"), 2.0)
SugarFont_SetKerning(string.byte("i"), string.byte("e"), 1.0)
SugarFont_SetKerning(string.byte("i"), string.byte("p"), 2.0)
SugarFont_SetKerning(string.byte("i"), string.byte("n"), 1.0)
SugarFont_SetKerning(string.byte("i"), string.byte("s"), 1.0)
SugarFont_SetKerning(string.byte("i"), string.byte("v"), 2.0)
SugarFont_SetKerning(string.byte("e"), string.byte("d"), 0.0)
SugarFont_SetKerning(string.byte("e"), string.byte("t"), 1.0)
SugarFont_SetKerning(string.byte("f"), string.byte("e"), -1.0)
SugarFont_SetKerning(string.byte("f"), string.byte("l"), -1.0)
SugarFont_SetKerning(string.byte("f"), string.byte("o"), -1.0)
SugarFont_SetKerning(string.byte("g"), string.byte("m"), 0.0)
SugarFont_SetKerning(string.byte("h"), string.byte("o"), 2.0)
SugarFont_SetKerning(string.byte("j"), string.byte("o"), -2.0)
SugarFont_SetKerning(string.byte("n"), string.byte("f"), 3.0)
SugarFont_SetKerning(string.byte("n"), string.byte("o"), 2.0)
SugarFont_SetKerning(string.byte("n"), string.byte("t"), 3.0)
SugarFont_SetKerning(string.byte("o"), string.byte("f"), 3.0)
SugarFont_SetKerning(string.byte("o"), string.byte("t"), 1.0)
SugarFont_SetKerning(string.byte("P"), string.byte("u"), 0.0)
SugarFont_SetKerning(string.byte("q"), string.byte("u"), 2.0)
SugarFont_SetKerning(string.byte("s"), string.byte("t"), 3.0)
SugarFont_SetKerning(string.byte("t"), string.byte("a"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("e"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("i"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("h"), 0.0)
SugarFont_SetKerning(string.byte("t"), string.byte("s"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("t"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("u"), -1.0)
SugarFont_SetKerning(string.byte("T"), string.byte("h"), -1.0)
SugarFont_SetKerning(string.byte("T"), string.byte("i"), -1.0)
SugarFont_SetKerning(string.byte("u"), string.byte("i"), 2.0)
SugarFont_SetKerning(string.byte("u"), string.byte("f"), 3.0)
SugarFont_SetKerning(string.byte("u"), string.byte("t"), 2.0)
SugarFont_SetKerning(string.byte("v"), string.byte("e"), 0.0)
SugarFont_SetKerning(string.byte("v"), string.byte("i"), 0.0)
SugarFont_SetKerning(string.byte("w"), string.byte("e"), 1.0)
SugarFont_SetKerning(string.byte("w"), string.byte("i"), 0.0)
SugarFont_SetKerning(string.byte("W"), string.byte("h"), -1.0)
SugarFont_SetKerning(string.byte("y"), string.byte("g"), -1.0)
SugarFont_SetKerning(string.byte("y"), string.byte("o"), 1.0)

--Letter-to-punctuation.
SugarFont_SetKerning(string.byte("n"), string.byte(" "), 2.0)
SugarFont_SetKerning(string.byte("t"), string.byte("."), 0.0)
SugarFont_SetKerning(string.byte("y"), string.byte("."), -1.0)
SugarFont_SetKerning(string.byte("y"), string.byte("."), 2.0)
SugarFont_SetKerning(string.byte(" "), string.byte("i"), 8.0)
