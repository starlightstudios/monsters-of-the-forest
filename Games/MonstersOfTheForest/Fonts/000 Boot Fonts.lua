-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by Adventure Mode
if(gbBootedAdventureFonts == true) then return end
gbBootedAdventureFonts = true

-- |[Setup]|
--Paths.
local sFontPath = "Data/"
local sKerningPath = "Data/Scripts/Fonts/"
local sLocalPath = fnResolvePath()

--Special Flags
local ciFontNoFlags    = 0
local ciFontNearest    = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge       = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade   = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS   = Font_GetProperty("Constant Precache With Special S")
local ciFontSpecialExc = Font_GetProperty("Constant Precache With Special !")

-- |[ ====================================== Font Registry ===================================== ]|
-- |[Mister Pixel]|
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Mister Pixel 120 DFO", sLocalPath .. "MisterPixel.ttf", "Null", 120, ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Mister Pixel 90 DFO", sLocalPath .. "MisterPixel.ttf", "Null", 90, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 30 DFO", sLocalPath .. "MisterPixel.ttf", sLocalPath .. "MisterPixel30_Kerning.lua", 25, ciFontEdge + ciFontDownfade + ciFontSpecialExc)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 20 DFO", sLocalPath .. "MisterPixel.ttf", sLocalPath .. "MisterPixel20_Kerning.lua", 20, ciFontNearest + ciFontEdge + ciFontDownfade + ciFontSpecialExc)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 16 DFO", sLocalPath .. "MisterPixel.ttf", sLocalPath .. "MisterPixel16_Kerning.lua", 16, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 8 O", sLocalPath .. "MisterPixel.ttf", sLocalPath .. "MisterPixel16_Kerning.lua", 8, ciFontNearest + ciFontEdge)

-- |[Oxygen]|
Font_SetProperty("Downfade", 0.95, 0.75)
Font_Register("Oxygen 27 DF", sLocalPath .. "Oxygen-Regular.ttf", sLocalPath .. "OxyReg27_Kerning.lua", 27, ciFontDownfade + ciFontSpecialS)

-- |[Sanchez]|
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 70 DFO", sLocalPath .. "SanchezRegular.otf", sLocalPath .. "Sanchez60_Kerning.lua", 70, ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 60 DFO", sLocalPath .. "SanchezRegular.otf", sLocalPath .. "Sanchez60_Kerning.lua", 60, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 50 DFO", sLocalPath .. "SanchezRegular.otf", sLocalPath .. "Sanchez60_Kerning.lua", 30, ciFontNearest + ciFontEdge + ciFontDownfade)

--Sanchez 35 and 22 are created by the EngineFonts.lua booter.

-- |[Trigger]|
Font_SetProperty("Downfade", 0.99, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 DFO", sLocalPath .. "TriggerModified.ttf", sLocalPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge + ciFontDownfade)

-- |[ ========================================= Aliases ======================================== ]|
--Mister Pixel 120 DFO
Font_SetProperty("Add Alias", "Mister Pixel 120 DFO", "Adventure Combat Damage Effect")

--Mister Pixel 90 DFO
Font_SetProperty("Add Alias", "Mister Pixel 90 DFO", "Adventure Combat Header")

--Mister Pixel 30 DFO
Font_SetProperty("Add Alias", "Mister Pixel 30 DFO", "Adventure Combat Description Simple")

--Mister Pixel 20 DFO
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Description")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Costume Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Doctor Bag Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Exp")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat VicItem")

--Mister Pixel 16 DFO
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Ally Bar")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Portrait UI")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adlev Generator Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level UI")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Context Menu Main")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Classic Level Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Dance Score")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Equipment")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level Reinforcement")

--Mister Pixel 8 O
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Mug")

--Oxygen 27 DF
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Main")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Decision")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Credits Oxy")

--Sanchez 70
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Journal Big Header")
Font_SetProperty("Add Alias", "Sanchez 70 DFO", "Adventure Menu Equipment Double Header")

--Sanchez 60
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat New Turn Font")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat Confirmation Big")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Menu Skills Confirmation Big")

--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Acting Name Font")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Confirmation Small")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Target Cluster Selection")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Ability Title")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Campfire Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Costume Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Equipment Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu File Select Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Gemcutter Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Inventory Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Options Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Doctor Bag Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Vendor Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Journal Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Character")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Confirmation Small")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Field Abilities Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Status Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Form Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "String Entry Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Classic Level Main")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Inspector Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Inspector Names")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Platina")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Doctor")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Help Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Credits Sanchez")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Equipment Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu File Select Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Gemcutter Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Inventory Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Options Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Status Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Doctor Bag Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Vendor Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Form Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Journal Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Job")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Skills")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Field Abilities Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Campfire Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "String Entry Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adlev Generator Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Help Mainline")

--Trigger 28 DFO
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Main")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Prediction")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Main")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Mainline")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Resistance")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Health")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Effect")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Journal Stats")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Inspector Effects")

--Trigger 50 DFO
Font_SetProperty("Add Alias", "Sanchez 50 DFO", "Adventure Combat Description Header")
