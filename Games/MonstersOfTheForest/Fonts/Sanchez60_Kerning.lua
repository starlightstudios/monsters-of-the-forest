-- |[Oxygen-Regular Kerning]|
--Sets the kerning values for the listed font.

--Set the main scaler.
SugarFont_SetKerning(1.0)

--Letters groupings.
--SugarFont_SetKerning(string.byte("i"), -1, 3.0)
--SugarFont_SetKerning(-1, string.byte("i"), 2.0)

--Punctuation groupings.
SugarFont_SetKerning(-1, string.byte(":"), 2.0)
SugarFont_SetKerning(-1, string.byte("!"), 3.0)
SugarFont_SetKerning(-1, string.byte("`"), 3.0)
SugarFont_SetKerning(-1, string.byte("["), 4.0)
SugarFont_SetKerning(-1, string.byte("]"), 6.0)
SugarFont_SetKerning(-1, string.byte("'"), 6.0)
SugarFont_SetKerning(-1, string.byte(";"), 6.0)
SugarFont_SetKerning(-1, string.byte("|"), 6.0)
SugarFont_SetKerning(-1, string.byte("\\"), 6.0)
SugarFont_SetKerning(-1, string.byte("-"), 6.0)
SugarFont_SetKerning(-1, string.byte("="), 6.0)
SugarFont_SetKerning(-1, string.byte(":"), 6.0)
SugarFont_SetKerning(-1, string.byte("\""), 6.0)
SugarFont_SetKerning(-1, string.byte("."), 6.0)
SugarFont_SetKerning(-1, string.byte("?"), 6.0)
SugarFont_SetKerning(-1, string.byte("<"), 6.0)
SugarFont_SetKerning(-1, string.byte(">"), 6.0)

--Specific punctuations.
--SugarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

--Numbers.
--SugarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

--Specific letters.
SugarFont_SetKerning(string.byte("D"), string.byte("o"), 3.0)
SugarFont_SetKerning(string.byte("g"), string.byte("s"), 3.0)
SugarFont_SetKerning(string.byte("s"), string.byte("a"), 2.0)
SugarFont_SetKerning(string.byte("o"), string.byte("g"), 3.0)

--Letter-to-punctuation.
--SugarFont_SetKerning(string.byte("r"), string.byte(","), 0.0)
