-- |[Oxygen-Regular Kerning]|
--Sets the kerning values for the listed font.

--Set the main scaler.
SugarFont_SetKerning(1.0)

--Letters groupings.
--SugarFont_SetKerning(string.byte("i"), -1, 3.0)
--SugarFont_SetKerning(-1, string.byte("i"), 2.0)

--Punctuation groupings.
SugarFont_SetKerning(-1, string.byte(":"), 2.0)
SugarFont_SetKerning(-1, string.byte("!"), 3.0)
SugarFont_SetKerning(-1, string.byte("."), 2.0)
SugarFont_SetKerning(-1, string.byte(","), 2.0)

--Specific punctuations.
--SugarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

--Numbers.
--SugarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

--Specific letters.
--SugarFont_SetKerning(string.byte("a"), string.byte("f"), 2.0)

--Letter-to-punctuation.
--SugarFont_SetKerning(string.byte("r"), string.byte(","), 0.0)
