--[Scenario Variables]
--[ ==================================== Scenario Variables ===================================== ]
--This is where variables related to the scenario and not the engine go.

--[Background Names]
--Add background path remaps. This makes paths easier to use in-stride.
WD_SetProperty("Add Background Remap", "ForestA", "Root/Images/MOTF/Backgrounds/ForestA")
WD_SetProperty("Add Background Remap", "ForestB", "Root/Images/MOTF/Backgrounds/ForestB")

--[Variables]
DL_AddPath("Root/Variables/Characters/Player/")
VM_SetVar("Root/Variables/Characters/Player/sGender", "S", "Boy")

DL_AddPath("Root/Variables/Characters/Scenario/")
VM_SetVar("Root/Variables/Characters/Scenario/iTalkedToFrederick", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iOpenedLeftCocoon", "N", 0.0)
VM_SetVar("Root/Variables/Characters/Scenario/iAntRescue", "N", 0.0)
  VM_SetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N", 0.0)
  VM_SetVar("Root/Variables/Characters/Scenario/iYouFight", "N", 0.0)