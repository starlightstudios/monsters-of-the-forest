 --The trapped ant scene, which can +1 the ant tf counter, otherwise leads to the minimally transformed route.
 
 --[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
 
 fnInstruction([[ WD_SetProperty("Append", "Narrator: I creep forward, peering cautiously between the leaves.[BLOCK][CLEAR]") ]])
    	fnInstruction([[ WD_SetProperty("Append", "Narrator: I can't see anything.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying to cover the tremor in my legs, I enter the small clearing my pocket knife in hand. The clearing is no larger than a wagon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The clearing is dominated by a spider-web larger than I am strung between two trees. And there is ant-girl trapped there. She must of thrashed something terrible, as she is cocooned in the web.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:    [MOVECHAR|Irene|Middle|QUADINOUT][ADDCHARPOS|Handmaiden|-1|Neutral|Middle+Body] [EMOTION|Irene|Neutral] Freeing her would be hard, she is far up there and I'm shorter than I used to be. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Seemingly to pass the time she is swinging in the cocoon upon the web. One hand has snaked though the web to make the rustle I heard.[BLOCK][CLEAR]") ]])
  
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her large black eyes look at you hopefully, which quickly becomes a confused look. As you move closer and she gets a better look at you she seems crestfallen. Afterwards she regards you cautiously, obviously not sure if the situation has just taken a turn for the worse. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] (Doesn't look like she's bait for some spider-girl trap.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I could probably rescue her, assuming I want to put in the effort. I'm here for my friends, not to rescue a monster-girl.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (She seems familar somehow, and I can't help but feel sorry for her.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (However I'm only free myself due to a lucky turn, she needs helps as much as I did. But if something goes wrong and rescuing her tips off the spiders then I'm putting my friends in peril.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (I'm sure Cohen would say do it. He was always pro-rescue the damsel in distress. I'm not sure she counts as a damsel though.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "RescueDecision") 
  
  elseif(sTopicString == "RescueDecision") then
 	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Rescue her\", " .. sDecisionScript .. ", \"Rescue\") ")
    
    elseif(sTopicString == "Rescue") then
--Rescue her:
 VM_SetVar("Root/Variables/Characters/Scenario/iAntRescue", "N", 1.0)
  VM_SetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N", 1.0)
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
     
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Isaac|Happy] Well I can't make things worse.[BLOCK][CLEAR]") ]])
  --[EMOTION|Isaac|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene:  It's great that I'm in a place where things can't get worse.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I had better shut up before I really jinx myself.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Isaac|Neutral] (I admire her presistence but she did herself more harm than good in trying to break free. She has worked herself further up in the web that I am tall.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Being shorter than I was that's less than I would think.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm not going to be able to destroy the web with this pocket knife. Be hard pressed if I had a machete.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmm, I don't need the web destroyed so much as I need the cocoon within reach. So she just needs to fall down here.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Isaac|Happy] (Good thing I misspent so much of my childhood. Spiderweb doesn't burn, it's what is in the spider-web that burns. The web itself just kind of melts when near fire.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Isaac|Neutral] (These webs are fresh enough to be safe if I use fire to rescue the ant-girl. Last thing I want to do is cook her.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I cast about for the driest wood I can find. The ever-present gloom makes this hard. Wet wood will make smoke, smoke is bad for both of us.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally in desperation I show what tinder I have been able to gather to the ant-girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a great deal of effort the ant-girl contorts her free arm to point at at the web.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I pause trying to figure out what she means.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The hell is she pointing at?)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The web is what's causing the problem. Impressive no matter how you look at it though.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Spiders must catch a good number of birds now that I think of it.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Oh! She is pointing at the leaves and branches stuck in the web.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (The driest stuff in a forest is the trees. I should be able to use those safely.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It takes doing but I am able to break off the wood that anchors the web to the nearby trees. The web is so huge that it barely makes a difference.[BLOCK][CLEAR]") ]])
  
  
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long I make a small smokeless fire, Then I quickly ignite a long wooden branch that is difficult to lift weven with the strength in my newly hard arms.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry] (Come on you bastard!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Clumsily and standing on now very durable tip-toes I bring the flame to the web around the cocoon. The web shrinks away from the flame, and the cocoon quickly proves too heavy to remain up there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The cocoon falls with an undignified thump.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a struggle, I cut open a slit in the cocoon. (Okay now that has to be a double entandredre.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls smiles at me as I work. Once the cocoon has been split, I get to work clearing the spider webs off the ant-girl.[BLOCK][CLEAR]") ]])
  --maybe have the Handmaiden show up here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] She's absolutely covered in white stuff, it's less sexy than you would imagine.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] She uses that as an opportunity to pull me down with all four of her arms.[BLOCK][CLEAR]") ]])
  --shift to kneel here
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Isaac|Smirk] Hey![BLOCK][CLEAR]") ]])
  --go to null here, might one day have a sex scene cg but let's not get optimistic here
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Irene][REMCHAR|Handmaiden][BACKGROUND|Null] As I lie next to her in the soft remains of the cocoon the fatigue of the day catches up with me briefly. The ant-girl gives me a a look of pure fascination.[BLOCK][CLEAR]") ]])
  --go to black screen or have Isaac actor disappear
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she kisses me suddenly. Her arms gently cradling my head as her smaller hands feel my hips.[BLOCK][CLEAR]") ]])
--NOTE TO SELF: add female case here, also is this rhythm okay?
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Wow! Not what I was expecting. Maybe not the right time either.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I am sort of on a quest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I need to rest and it wouldn't be a bad to relax.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Did I just feel a tingle?)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Sex Skip") 
  
  elseif(sTopicString == "Sex Skip") then
 	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Kiss back\", " .. sDecisionScript .. ", \"Kiss back\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"I'd like to skip the sex scene please\", " .. sDecisionScript .. ", \"Skip\") ")
    
    elseif(sTopicString == "Kiss back") then
--call gender variable:
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Might as well kiss back.[BLOCK][CLEAR]") ]])
     if(sGender == "Girl") then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (She's not bad looking.)[BLOCK][CLEAR]") ]])
end
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Oh what the hell, if I am to die let's die with a smile in my heart.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Hmm I've been listening to Cohen recount the poetic edda too much.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I kiss back, the ant-girl is thrilled and enthusiasticly continues kissing.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she pushes me back. One of her hands caresse my hair as she looks into my eyes with a warm smile.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: In the soft embrace of the cocoon the sounds of the forest are muted. Under the ant-girl's touch and onto the soft silk I detense a little.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: If the spiders find me here there isn't much I can do about it anyway.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: My breathing evens out as the adrenaline starts to recede. I smile at the ant-girl. Just me and her in our own private sanctuary.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl studies my face as her antenna twitch. Well probably, opaque eyes make it hard to tell. Her expression seems caught between many things at once.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally she smiles at me. Then she takes my hand and begins to message my palm and fingers. The hand itself is hard but she still manages to produce a ticklish sensation.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Her hands move up my arm messaging as they go. Finally they reach the uneven border between my smooth skin and the chard chitin. She seems to be trying to get me to relax.[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I wonder how the others are doing. Are they alive?)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Holy hell I miss them. All I'm really doing is probably getting myself killed in a spectacular fashion.)[BLOCK][CLEAR]") ]])
else
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I set out on a great adventure and everyone's gotten captured. I don't think it was my fault, but I hope they are alive. )[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Holy hell I miss them. Is me going into the forest just an elaborate version of suicide? )[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Here I am wasting time with her when my friends need help.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: My attention is brought back to the ant-girl as she pinches my skin. She gives me a look that has a hint of a pout. Then she moves her face close to mine and begins to kiss me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I kiss back trying to match her enthusiasm.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I guess this makes me a real adventurer, getting it with an exotic person in a faraway place. Cohen would be proud. For all the good that does me.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I need to stop getting distracted, it's not fair to her. I agreed to this, I should see it through.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I begin to kiss back enthusiastically. The ant-girl physically signals her excitement, wrangling my tongue with experience.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She softly pets my head and messages my side. Her smaller set of hands begin to trace soft lines up and down my thighs. She's a skilled kisser and before long I'm losing focus.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I'm almost so distracted that I don't notice the gentle tingle resume in my arms. It works it way up my arms.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I push the ant-girl back just long enough see the dark chitin spread up my arms like water spilling from a cup. The feeling is comforting, and the ant-girl kisses me on the cheek to try and keep me distracted.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long the dark material has worked it's way to my shoulder. It tickles as it covers my shoulder and armpit, leaving the joint hard and shiny.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl smiles at my giggles. She plants a kiss on my finished shoulder. The sensation resumes on my thighs.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl messages my thighs as the brown material moves up gently. It moves with a concerning speed. The brown material stops below my new abdomen.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: As my leg is encased there is a strange pulling feeling, as the leg moves outwards connecting into my torso from the side.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It is a far cry from human anatomy now.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I know I should panic, but the unnatural calm and the way the ant-girl messages where the legs connect to my body keep me from panicking.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My arms and legs now look insectoid. The ant-girl pulls back a little showing off identical looking arms and legs. She smiles hopefully at me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I guess it's not so bad. It seems to work for her.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a tickling beneath my armpits, with some contorting I see a line of black chitin growing from my spine to make a plate of chitin beneath my armpit.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl messages these plates with her smaller arms.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a tantalizing slowness a pair of second, simpler arms emerges from the plates. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They hang limply for a second, the three fingered hand not moving.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying as hard as I can I can't get them to move.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: With a soft touch the ant-girl meets my useless hands, interlocking her smaller fingers with mine. Then she kisses me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a tingle near the base of my neck and my spare hands can move against hers.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl meets my gaze and smiles slightly. Then everything goes dark as I go blind.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It's hard to avoid screaming in terror. I am helped by the ant-girl hugging me to her. Her embrace is motherly and she pats my head to calm me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I can see again. Glancing towards the tree tops, the gloom of the forest seems less deep.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Scrambling I produce my mirror. I have the same opaque black eyes as the ant-girl. They seem unavoidably alien and featureless. The comforting look the ant-girl gives me helps.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: As I examine myself in the mirror, I see that my hair is rapidly changing color, and shortening to a uniform length.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl wears her hair the same way.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: My face tingles all over as it rapidly softens; becoming more girlish[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] I never thought I'd look so cute. I'll have a hard time acting cute though.)[BLOCK][CLEAR]") ]])
else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (I'm even less intimidating now! Good thing I still got the attitude to make up for it.)[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Narrator: This is followed by a strong stroking feeling on my sides, it's a confident almost thrumming sensation.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I feel a flush of arousal, it catches me off guard. It's not the usual type of arousal.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: This inspires me to stroke the ant-girls sides. She seems to enjoy it, and messages my stomach muscles. After so much danger it's comforting to have someone close.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: As the sensation continues my waist becomes thinner and thinner, my insectoid abdomen fills out in response.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: When the sensation finishes, my waist is inhumanly thin, and my abdomen taut and mature. The ant-girl gently rubs my waist, happy with this development.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: To celebrate she resumes making out with me, her hands tracing gentle lines all over my much changed body.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a loud popping sound, like you might make with your mouth. The ant-girl seems surprised, and she stops spooning to examine closely.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The source of the sound is an unexpected slit towards the end of my abdomen. It's not particularly large and not knowing what it might be I reach to probe it.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl however beats me to it and gently probes with a chitinous finger, I can't help but gasp .[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Holy hell that's sensitive.)[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Holy hell that's a lot more sensitive than I was expecting!)[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Narrator: The slit is tightly closed but extremely sensitive. With a devilish grin, the ant-girl probes again bringing another moan.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Whew, it won't be long if she keeps that up. My legs are shaking.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I make needful eyes at the ant-girl. She gives me a knowing look.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It builds slowly but gradually there is a double noogie sensation near my hairline. This is followed by a sudden feeling of relief, then to my surprise I sprout a pair of insectoid antenna.[BLOCK][CLEAR]") ]])
--might reorder this to be antenna then abdomen
fnInstruction([[ WD_SetProperty("Append", "Narrator: They mock me by swaying too and fro at the upper edges of my vision. They keep flicking to avoid my grabbing, but I finally catch one. Closer examination shows they are duplicates of the ant-girls.[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
     fnInstruction([[ WD_SetProperty("Append", "Narrator: I let out an angry needful sigh. I was pretty closing to finishing, but pain really kills my arousal, female or not.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I let out an angry needful sigh. I was pretty closing to finishing, but pain really kills my arousal.[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Narrator: I look back to the ant-girl, only to see that she is wreathed in aura of indescribable colors.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Twitching my antenna I focus on one. It's a warm happy smell tinged with a feeling of love.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (How can I understand that from a smell?)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Further examinations are cut off by the ant-girl softly kissing me. She then pulls back, and sits up motioning for me to sit on her lap.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Okay, she seems more experienced in this sort of dynamic.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It takes some work, and tucking my abdomen under my legs to sit on her lap. To my surprise I discover that I'm the same height as her now.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She gently kisses the back of my neck, holding me close with her small arms around my thin waist. It's the most natural thing in the world to lean back against her. Then her larger pair of hands begin to gently stroke around my vagina.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She starts by gently tracing the chitin framing my pelvis and moves inward slowly until she's stroking my pubic mound directly.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I was hot and bothered earlier and her hard chitinous finger enters me easily. I've never been happier for hard chitin fingers.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She begins to gently masturbate me, as she kisses me and messages my new chitin with her unoccupied hands.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: I spend a happy few minutes under a cloud of building arousal. Finally I glance back at the ant-girl. She gives me a mischievous look.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Her small set of hands find my abdomen’s small slit and begin to tease it relentlessly. I let out a long low moan.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I need to remember to always watch out for the second pair of hands!)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Not wanting to be selfish I try to do the same to her, but even with some scrambling and contorting I find her abdomen has no slit.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she sticks two of her small fingers into my second slit and I lose focus.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long I'm on the edge of orgasm, the feeling building through my body. Finally it's too much. Like a damn breaking it bursts forth.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It'd be a screaming orgasm but I suck a finger to keep quiet.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: In a warm haze, I flop back into the soft cocoon. The ant-girl follows suit, kisses me and then embraces me tightly. Despite her hard chitin, it's a very soft embrace.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long however the urgency of my quest comes back to my mind.)[BLOCK][CLEAR]") ]])
  --add back here, including background
  --[BACKGROUND|ForestC]
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][ADDCHARPOS|Handmaiden|-1|Neutral|Middle-Body][ADDCHARPOS|Irene|-1|Smirk|Middle][BACKGROUND|ForestB](I feel bad to leave her. But the others need me.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hopefully she won't take it too badly. I'm sure Frederick would make a joke about loving them and leaving them.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] (Heh, I'm not sure I should tell them about this part when I see them.)[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Narrator: I sit up unsteadily, feeling my much changed body. I try to put strength back into my legs. I'm sorer than I expected.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl looks up at me giving me a smile with slitted eyes.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: She places a gentle hand on my thigh obviously not wanting me to leave.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Sad] (Oh crap! I was the only one to climax from that.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It's just rude to not help her finish. Also, I don't want her to tell people I'm bad at sex. )[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] (How to convey that to her? She hasn't talked so far.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Laugh] Blushing a little I made a crude sexual motion to her, while using my own body for reference.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: She doesn't get it the first time. She sits up giving me a curious look. So still blushing I have to repeat myself.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] The ant-girl finds this hilarious, she laughs silently so hard it knocks her over. Finally she gets up again and sits next to me.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Skip") 
  
  
  elseif(sTopicString == "Skip") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
     --[BACKGROUND|ForestC]
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Handmaiden|-1|Neutral|Middle-Body][ADDCHARPOS|Irene|-1|Smirk|Middle][BACKGROUND|ForestB] Still giggling silently, although putting a hand over mouth to be polite, she begins to clean off the webs that are sticking to my hard chitin.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] I really do look just like the ant-girl. Which I guess makes me an adventurer turned into an ant-girl. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: When I'm cleaner I pull on my discarded clothes. It's more for vanity than functionality at this point. Also I need enough clothes to be recognized when I rescue the others.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The colors are still all around me. I twitch my antenna and examine myself curiously. I am wreathed in warm happy colors, I 'sniff' and focus on one. It smells of warmth, contentment and orgasm.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Must be part of being an ant-girl I suppose. Shame I don't understand them enough to really use them.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My companion hasn't said a word since I met her. It's probably how ant-girls like her talk.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I wonder if she can make noise and simply prefers not to.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Still a better sense of smell isn't a bad thing, maybe it'll help me get the jump on some spiders. )[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Heh, this abdomen is kind of silly large. I wonder how the ant-girls deal with it. )[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It reminds me of a mother's stomach.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (That was a weird thought. Just gonna ignore that for the time being.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl finishes cleaning me off and looks at me expectantly.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/240 1 trapped ant.lua", "AntTF2+0")
 end