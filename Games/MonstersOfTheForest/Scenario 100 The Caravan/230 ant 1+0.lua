--the +0 choice, leads into the ant cocoon scene
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)
--[Main Scene]
--Previous scripts rejoin here.
if(sTopicString == "Begin Scene") then
  
      --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])

fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmmm, well I'm not the greatest adventurer.)[BLOCK][CLEAR]") ]])
 if(sGender == "Boy") then
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Haven't slain any dragons yet.)[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Haven't been to any distant lands.)[BLOCK][CLEAR]") ]])
end
--NTS: have male/female difference here
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (But I've had some excellent lessons from Cohen. He taught me and the others the basics of tracking quarry.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Admittedly this is a lot tougher than rabits but still.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The trick isn't to examine every tree. That'd be murder on my neck. It's to figure out the pattern of the trees they move between.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Thinking back to the path I followed to here, spiders like to walk across web strung between two close trees or swing from one tree to another.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (In the first case I need to look for trees growing unusually close or that have strong branches reaching out to another.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (In the second case I need to look trees a bit shorter than normal and have branches broken off where the spider would land.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (I can do this. I have to do this.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] It's slow going. More than once I have to backtrack to find the trail. Still my harder arms and legs mean I spend less time avoiding underbrush and brambles.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen always aid to be slow and steady when tracking something. Both so you don't lose the trail and so you don't wander into something dangerous.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: More than once I lose the trail and have to backtrack. To avoid getting turned around, I mark the trees with my hard fingers.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: It hurts way less than it normally does doing that.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: A good sign is when I almost stumble into a giant spider-web strung between two trees.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Whew! That was close.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry] (Damn spiders probably put the webs to catch anyone approaching their nest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I get caught in that I'm dead meat. I don't have a knife large enough to cut myself out with.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Still not that dangerous if you are paying attention.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Although I'm not one to talk.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I stride forward a bit more cautiously. It's slow going though and makes my neck hurt something fierce.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Narrator: It takes a while but I know I'm on the right track as the rate of webs increases[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Eventually,I can hear a slight rustle. I can see a gap in the trees suggesting a clearing.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Surprised] (I don't have much of a chance of fighting whatever is ahead.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Still if I lose the trail and get turned around in this mess I might never find my way out, to say nothing of finding my friends.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac](I'll take a peak and see what it is before panicking.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac](Is what I'd say if I wasn't panicking already.)[BLOCK][CLEAR]") ]])
     fnCutsceneBlocker()
LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/240 trapped ant.lua", "Begin Scene")
end