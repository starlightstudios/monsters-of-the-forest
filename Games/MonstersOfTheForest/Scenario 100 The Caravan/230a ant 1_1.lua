--Climb the tree scene, which leads to ant 1+1 tf scene
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Best way, best way.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I absentmindedly bring thumb and forefinger together as I think. It makes a slight clicking noise.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Huh, guess my fingers are harder now.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (Hell I can't think of anything.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Out of ideas I examine the tree again, it's a virgin tree covered in thick porous bark.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Huh, that wouldn't be too bad to climb. )[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (And hey if climb it I should be able to see the next tree in the spider's path!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Probably for the best that I don't climb every tree, I don't have all day. Or any food or water.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll use it to figure out what the pattern is. I'm a genius.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The tree would have been difficult to climb before, but my hard fingers and toes make it a lot easier.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Even if I do get my hard fingers and toes stuck in the tree a few times.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle+body+body|QUADINOUT] Before long however I find myself far up in the tree.[BLOCK][CLEAR]") ]])
  --Change background here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider's path is a modest thing, a few cleared branches, a bit of webbing to increase the area to stand.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: And a thin alabaster strand of web tied securely even further up into the canopy of trees.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: On a whim I examine the alabaster strand closely, it smells purple?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Up here it's more obvious than ever just how huge the forest is, it seems to stretch off infinitely and I can only distantly see blue skies.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I wonder if this is like that ocean thing that Cohen always describes?)[BLOCK][CLEAR]") ]])
 	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Too large to be properly understood in any real sense.)[BLOCK][CLEAR]") ]])
    if(sGender == "Girl") then
 	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Maybe once we get out of here we can go there.)[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Focus! And try not to look down.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I scan the trees around me, trying hard to not put the ground in sight.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Far in the distance I see another platform like the one I'm on. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The branches in between here and there have been carefully trimmed. It creates a clear corridor suspended in the air.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Huh they really do swing between the trees. That's amazing.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Ah, I'm going to have to look down to climb down aren't I?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Wait a minute I just got the best idea.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (This is either going to kill me or make an awesome story. Given how things are going so far, what's another deadly risk?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I take the strand of web in my hand and test it a few more times. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's securely anchored, and I eye my destination carefully. Then with elaborate care I knot the strand around one of my hard hands and my belt.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Sad] Then I accidentally look down and take a deep breath. The ground is still very very far away.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I close my eyes and take a deep breath. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There isn't enough space for more than a few steps of running start but I take them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The world drops out from under me as I leap.[BLOCK][CLEAR]") ]])
--have portrait move to the left with each dialog
--[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene:[MOVECHAR|Irene|Middle+body|QUADINOUT]  Hol-[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [MOVECHAR|Irene|Middle|QUADINOUT] Holy H-[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [MOVECHAR|Irene|Middle-body|QUADINOUT]Holy Hell![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: For a few terrifying moments it feels like I'm flying as the pendulum motion carries me forward.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSpecial]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a desperate scrabble to grab the hand hold I need.[BLOCK][CLEAR]") ]])
--go to kneeling
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My hand slips and my heart almost stops.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: But my new hands are stronger than my old ones and I manage to pull myself onto the platform. [BLOCK][CLEAR]") ]])
--go to full portrait
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][MOVECHAR|Irene|Middle-body-body|QUADINOUT] [EMOTION|Irene|Happy] (Woo! I feel invincible! Hell I am invincible)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (There's the next tree!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm not doing that again!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:The climb down is reasonably easy. Among other things the adrenaline makes me feel unstoppable. Also I really don't want to be off the ground right now.[BLOCK][CLEAR]") ]])
  --change background here
  --[EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle|QUADINOUT] I almost kiss the ground when I finally reach the bottom. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The tiredness in my armes and legs reminds me why stupid ideas tend to be stupid.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Breathing heavily I put my back to the tree, just need to take a little breather.[BLOCK][CLEAR]") ]])
  --Kneel here
  --go to 230b ant 1+1 TF
 fnCutsceneBlocker()
LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/230b ant 1_1 TF.lua", "Begin Scene")
end
--[=[
  note to self: the ant-girl is very affectionate and touchy feely if the player does ant-intimacy. She tries to be a bit of a mother and is trying to reinforce the positive parts of being an ant.

The player kisses back but is still a bit lost.

The ant-girl pulls back slooking a bit sheepish. Describe the cocoon around her. it's soft and dampens out the noise of the woods. Your own private sanctuary. As you take a few deep breaths the adrenaline starts to fade.

The ant-girl studies your face as her antenna twitch. Then she gently takes your hand begins to message it. My hands and fingers are hard now but she still manages to produce a ticklish sensation. She moves up my arm messaging the margin where my hard chitin gives way to smooth skin. It tickles. 

Have a thinking moment here: If boy: I wonder how the others are doing. Are they alive? Am I just getting myself killed. 

The ant-girl pinches you and then kisses you.

I guess the ant-girl isn't bad looking, but here I am getting it on with a monster-girl. I guess that's what adventurers do. I'm not being fair though. I should focus on her.

If female: I set out on a great adventure and everyone's gotten captured. I don't think it was my fault, but I hope they are alive. Is me going into the forest just an elaborate version of suicide? 

The ant-girl pinches you and then kisses you.

So I need a way to address the player character's feelings about getting it with a girl. It'd probably be for the best to just say it's not a big deal. She isn't bad looking. 

The player kisses back enthuastically and the ant-girl enjoys it. She begins to message the player's thighs and sides with all four arms. The player tries to keep up. 

starts to tf here, what's in this tf?:
arms go to full chitin
legs go to full chitin
second arm comes in
Eyes change
hair changes
the ant-girl kisses your forehead as the antenna come in
tight slit comes in, the ant-girl gives you a smile so full it almost splits her face.
]=]