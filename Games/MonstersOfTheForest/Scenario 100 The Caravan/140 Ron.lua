--[Talk to Alonso]
--Subscript to handle talking to Alonso.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Begin]
--Start of the conversation.
if(sTopicString == "Begin") then
	WD_SetProperty("Hide")
	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    --[ADDCHARPOS|Ron|-1|Special|Middle+HfBody]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody] Ron is sitting near the campfire feverishly reviewing his magic tome. He mumbles various magical formula and conjures the occasional flame in his hand.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I don't know Ron as well as the others, we come from the same village but his family transported things to market. They weren't much richer than the other families but it still set them apart.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (They had the money to get him taught to read, write and perform magic. His joining up as our group's mage was probably so he wouldn't cast fertility spells for farmers his whole life.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Cows and fire don't mix, and I know which one he prefers. It's for the best, really.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] I casually massage my much reduced eyebrows.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Yep, definitely for the best.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He sees me approach.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Neutral][EMOTION|Irene|Neutral] Oh hello, just going over everything. Cohen said to be careful once enter the forest. Should we be worried?[BLOCK]") ]])
    
    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Don't worry\", " .. sDecisionScript .. ", \"Worry\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"No harm in being careful\",  " .. sDecisionScript .. ", \"Care\") ")
    fnCutsceneBlocker()
    
--I got you.
elseif(sTopicString == "Worry") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (We should be fine, it's a routine part of a boring job.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: [EMOTION|Irene|Smirk] We can take anything this forest can throw at us, so long as we stick together.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He smiles, relieved.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Right, of course we can. Sorry. Nerves just getting the better of me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He pauses.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Neutral] Still... Cohen is rarely wrong about stuff like this. We should be alert.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: I didn't say not to be alert, just not to be worried.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] He nods and returns to his book. I return to my preparations.[REMCHAR|Irene][REMCHAR|Ron][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")

--FEAR.
elseif(sTopicString == "Care") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Though:[VOICE|Isaac] (It can't hurt to be careful. He may be nervous, but it's hard to sneak up on a paranoiac.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: [EMOTION|Irene|Offended] It won't hurt to keep a solid guard and *watch* out for one another.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I put a particular emphasis on watch, hoping that Ron will finally learn to check his targets.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He pales a little at that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: R-right, it won't hurt. Still we need to be careful, if there really are monsters in this forest they probably have all kinds of tricks.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]I nod and return to my preparations. Ron seems to read his book with increased fervor, if that's even possible.[REMCHAR|Irene][REMCHAR|Ron][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")
    
end
