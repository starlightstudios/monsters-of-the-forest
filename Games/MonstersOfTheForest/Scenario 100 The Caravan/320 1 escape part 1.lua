--the escape sequence, meant to resolve the character arcs introduced earlier, also where the hypothetical mushrooms choice would be
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
     local iYouFight = VM_GetVar("Root/Variables/Characters/Scenario/iYouFight", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] Everyone hauls ass out of the spider's nest, fighting only when necessary to get further away.[BLOCK][CLEAR]") ]])	
  --movement here
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Hey look they have a bucket line going.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm not sure how much good it's going to do them, a lot of the trees are on fire.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: I was thorough, you can thank me later. Although if we have some time I can finish the job.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Not now! We don't need them more pissed off.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Happy] Look weapons and supplies! Grab whatever you can! No treasure though.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Alonso|Neutral] Not the time for treasure Fredrick.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  Aw.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: What would spiders even have as treasure?[BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Neutral]  If we could focus.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Everyone: On it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: In a later age what happens next would probably be described as run and gun. It's the present though so I'm going to call it run and melee.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Irene|Fighting][EMOTION|Frederick|Fighting][EMOTION|Ron|Fighting][EMOTION|Alonso|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Everyone runs as hard and as far as they can. [BLOCK][CLEAR]") ]])	
  --add spiders on the edge here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|SpiderA|-1|Neutral|LEdge] I turn back and see spiders tracking us despite their home burning behind them.[BLOCK][CLEAR]") ]])
  --Put spiders behind here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Our only heading is away from the burning nest.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Persistence isn't something you normally attribute to spiders, but most spiders aren't taller than you. [BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The spiders are at first content to simply show themselves sporadically. As if to remind everyone that they were following.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone shares worried glances, but since nothing can be done we keep moving. We try to lose them when we can.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|SpiderB|-1|Neutral|LEdge] After a few hours of running however, the attacks start. Small patrols of spider-girls attack trying to recapturing everyone.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron does his best to fend them off, but everyone else pitches in with the weapons they stole from the nest. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: At the start these are uncoordinated attempts, easy enough to fend off or evade. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long however they are trying to drive us into ambushes and chasing us relentlessly.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|SpiderA][REMCHAR|SpiderB] After fending off another such attack Alonso calls a stop.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][EMOTION|Alonso|Neutral][EMOTION|Ron|Neutral][EMOTION|Frederick|Neutral]Everyone crouches into a rest and shares what water they have been able to carry.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Alright, why are they still chasing us? Did anyone manage to steal anything actually valuable?[BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm not angry just impressed if you did.[BLOCK][CLEAR]") ]])
  --everyone laughs, then looks at frederick
    --Everyone looks at Frederick
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso's delivery nail's Cohen's tone and we all giggle.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Not me. I was trying though. We did do a lot of property damage, but this seems like a grudge.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: Maybe we are the valuable stuff? It's not trivial magically to transform us like this.[BLOCK][CLEAR]") ]])
	--[EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso:  Nah, they would of guarded us better then. I don't know, this is more effort than they put into capturing us. I agree with Frederick this seems like a grudge.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Offended](What the hell is up that spider queen's but?)[BLOCK][CLEAR]") ]])
     if(iYouFight == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Smirk](I did punch her in the box.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral](What did she say?)[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Smirk](Ron did singe her a bit.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Smirk](Heh she probably wasn't expecting that or me getting him free.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Smirk](Wait.)[BLOCK][CLEAR]") ]])
end
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Surprised](Ohhhhhh. Hell.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: It might be me. I think the spider-queen mistook me for a regular ant.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Ah, well there goes that idea. No returning that treasure.[BLOCK][CLEAR]") ]])	
--SD: Alonso smiles [EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Looks like it's gonna be a glorious battle out of here.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Ron: What?[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Happy]
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] The very least we can do to pay back Isaac for rescuing us is not handing him over to the spiders.[BLOCK][CLEAR]") ]])	
else
  	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] The very least we can do to pay back Irene for rescuing us is not handing her over to the spiders.[BLOCK][CLEAR]") ]])	
end
  --[EMOTION|Ron|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: Oh right, of course. Sorry.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Thanks guys.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Irene|Fighting][EMOTION|Alonso|Fighting][EMOTION|Frederick|Fighting][EMOTION|Alonso|Fighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] With nowhere to go but out, the fighting resumes in earnest. [BLOCK][CLEAR]") ]])	
--NOTE TO SELF: Should probably sketch these out more, but then I have to compress to a single encounter or have multiple small encounters, both of which draw out the scene. Something of a tradeoff
-- [EMOTION|Alonso|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator:Alonso is the leader, and he uses everyone's talents well. He also makes sure to keep everyone in as good spirits as he can manage.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's subtle but he always redirects negativity, and calls for rests when someone in particular is struggling to keep up.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He also makes a special point to keep checking in with me.[BLOCK][CLEAR]") ]])	
  -- [EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Are you sure you are okay? You keep pushing yourself further than the rest of us.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] I'm fine, I feel pretty good actually. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You aren't enhancing your ant-ness by using your ant features or anything are you?[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Alonso|Neutral][EMOTION|Irene|Neutral] It's fine, it's not. But even if it was; it's more important that we get out of here. If we can find Cohen again he will have a cure.[BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] Just pace yourself, okay?[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] Ron brings the literal firepower, but he has to pace himself on using spells so he doesn't exhaust himself too much.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Additionally his imprecise approach means that every time he casts a spell it signals to everyone where in the forest we are.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  We manage to get him to tone it down a little but he pouts interminably. [BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Still nothing covers an escape like everything suddenly catching fire.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I worried a bit about Frederick, he's always beens scrappy but never that good in a straight fight.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: But this isn't a straight fight. It's a desperate struggle, somewhere between being hunted and a free for all brawl.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick's good at tracking and getting away unseen. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: More than once we manage to evade an enemy patrol due to him being able to lead us away.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He takes our compliments with his usual magnanimity.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'm the greatest![BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|KAngry]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Angry] Get down before they spot you![BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Fighting][EMOTION|Frederick|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] As for me, I do my best. Thankfully my best is better than normal due to my new arms and legs.[BLOCK][CLEAR]") ]])	
  --Maybe focus the camera on each character during their episode?
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I'm able to lead the others, and when they stop to rest I have the strength to hide our tracks and set false trails.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My ability to see scents comes in handy on occasion, tipping me off to spiders once or twice. Frustratingly the spiders seem to know to hide their smell.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Sad][EMOTION|Frederick|Sad][EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: By the end of the first day the attacks have died down and things are looking up.[BLOCK][CLEAR]") ]])	
    
    fnInstruction([[ WD_SetProperty("Append", "Narrator: One patrol however keeps up the attack, they must be as lost as we are as they keep scrambling blindly forward.[BLOCK][CLEAR]") ]])	 
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If we can just defeat them, or at least restrain them we will be home and clear.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (We need the rest everyone is flagging. I'm gonna sleep for a week when all this is over.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Frederick keeps resting his eyes for a suspiciously long time. When we wake him up, he says he was just thinking really hard.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alonso keeps forgetting hand signals. Which is a problem when we need to keep quiet.)[BLOCK][CLEAR]") ]]) 
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I just hope I don't have to also carry Ron.)[BLOCK][CLEAR]") ]])
    --[BACKGROUND|ForestD]
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] The patrol retreats into a small valley, the floor of the valley is dotted with a number of tall straight trees. The ground itself is bumpy and crisscrossed by a number of sunken streams.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: It must be morning or just about, as there is a heavy mist rising from the ground.[BLOCK][CLEAR]") ]])	
        fnInstruction([[ WD_SetProperty("Append", "Irene: Should we pursue? That looks like dangerous ground. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso rubs his eyes and thinks carefully. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: I think we should, if we can just keep this patrol from reporting back then the spiders won't have a chance of finding us.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Frederick, make sure you know the way back out, I'd hate to go around in circles down there.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Aye aye, I've tracked goats across worse for sure.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Really?[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Well maybe not worse, but as bad certainly.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Sure you have Frederick.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: One last push we can do this.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Happy] Yeah we can do this.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral]  Come on everyone![BLOCK][CLEAR]") ]])	
    
    fnInstruction([[ WD_SetProperty("Append", "Narrator: We rush into the valley, ready for one final fight.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|SpiderB|-1|Neutral|REdge][ADDCHARPOS|SpiderB|-1|Neutral|REdge+150] The patrol stops running a good distance into the valley, seemingly tired.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] The spider-girls give spirited battle, not giving an inch if they can help it. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I lead the fighting with a stolen spider spear. Turns out a spare pair of arms really helps to use a spear. Especially ones as long as the spiders use.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Their leader is an older silver haired spider girl. She fights back with a will and grin.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: A suspiciously smug grin.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Still I fight with a reservoir of desperate energy, if I can just defeat her the other spider-girls will surrender for sure.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The others fight with similar desperation.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Why aren't they running?[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral]  Not the time Ron.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: No he has a point, why are they sticking here? If they can fight this hard they aren't too exhausted to run. I'd run or just climb a tree.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Oh fuck. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Surprised]  It must be a trap, we need to get away. Ron you got anything?[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: I got just the spell, I need time though.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I do my best to draw the spider-girls away form Ron. Frederick backs me up the best he can, but I can see the fear in his eyes.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Suddenly in the distance there are horns.[BLOCK][CLEAR]") ]])	 
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I can distantly see the scents in the air upwell as if something is forcing the air towards us.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: We must be surrounded by a lot of spider-girls[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Irene] The spider-girl I'm dueling takes advantage of my distraction. In a slick move trips me with her spear and disarms me at the same time.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: As quick as misfortune there is a rope around my legs and I'm being dragged off away from my friends. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: In despiration I drive all four hands into the soft muddy ground. The spider girl at the other end is stronger though and it doesn't make much of a difference.[BLOCK][CLEAR]") ]])	
    --move frederick forward here
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Then suddenly Frederick is standing over me, my spear in his hands. He cuts the rope dragging me and faces down the silver-haired spider girl.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Rage pratically pours off of him.[BLOCK][CLEAR]") ]])	
    --might need to change this line for when you can't see smells
    fnInstruction([[ WD_SetProperty("Append", "Frederick: If you take them I will gut you where you stand.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The silver haired spider-girl takes a hesitant step back, she may be a veteran but is obviously unwilling to die here[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Irene|-1|Smirk|Middle-HfBody-Body] I pull my legs free and then put a comforting hand on Frederick's back as I stand. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:Told you would have the courage when it counted. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Cohen wasn't lying about the wetting yourself part.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] I hope you are joking.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Cover your eyes![BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: We all cover our eyes, and series of bright flashes goes off making an high pitched popping noise.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The already chaotic scene goes into anarchy as the blinded spider-girls crash into one another.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|SpiderA][REMCHAR|SpiderB] Silently we all move to hide. We eventually end up squished together beneath a fallen tree.[MOVECHAR|Irene|Middle|QUADINOUT][MOVECHAR|Frederick|Middle-HfBody|QUADINOUT][MOVECHAR|Alonso|Middle+Hfbody|QUADINOUT][MOVECHAR|Ron|Middle+Body|QUADINOUT][BLOCK][CLEAR]") ]])	
    --real squished up here
    fnInstruction([[ WD_SetProperty("Append", "Narrator: In the distance we can see multiple spider-girl patrols moving through the mist. Alonso starts quietly cursing.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Fuck, fuck. I've got us all captured. We should never of gone in here. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: We can get out of this. There has to be a way.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene: It's not your fault Alonso. We are all exhausted. We should of recognized it for a trap from the start. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Besides, we wouldn't of made it this far without you. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Yeah I would of probably gotten us caught. You guys aren't as good at sneaking as I am.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Too many decisions for me. I'd crack under the pressure. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Thanks. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Pep talk aside, I think we might be finished. I don't think we can outrun that many. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Wait! I got it![BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: We have to split up. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Split up?[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Well all this time we have been fighting together right? So they won't expect us to split up.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Laugh] It's brilliant![BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: And in the valley we will be able to meet up again at the entrance. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] I can probably guide one team.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: And I'll guide the other. Ron you are a genius.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron smiles.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Frederick][REMCHAR|Alonso][MOVECHAR|Irene|Middle+Hfbody|QUADINOUT][MOVECHAR|Ron|Middle-HfBody|QUADINOUT] Frederick leads Alonso one way. I lead Ron the other way. Frederick makes a lame joke about my spare hands restraining Ron. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The moment a patrol passes by he almost starts casting out of fear. It takes four hands to restrain him.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The scents help me keep my heading. Ron is practically blind in the valley, so I hold his trembling warm hand. Despite some close calls before long we meet up at the front of the valley.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody+Body] That was a close one. Once again good thinking Ron.[BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Ron: Thanks! I was worred about not being able to lear-. [BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Save the self-congratulations for later, we have to get moving![BLOCK][CLEAR]") ]])	
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] You can tell us about it later Ron.[BLOCK][CLEAR]") ]])	
    --[BACKGROUND|ForestC]
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] We start running again, however things however things aren't looking good. The others are exhausted.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] No amount of morale can replace sleep and food. We can't stop here though, this is spider country.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Suddenly a desperate idea strikes me. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (I'm only doing better than them because I'm not quite myself anymore.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (It hasn't messed me up too badly.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (I hope.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (I could give them the mushrooms. It might be what makes the difference between freedom and captivity.)[BLOCK][CLEAR]") ]])
  --this is where the mushrooms choice would be
--Well mushrooms can't make things real worse
--Do something real audacious
	
--Do something real audacious
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Nah, I don't need to expose them to what has changed me.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Instead I need to get bold and buy them time.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Alright everyone rest up, I'm going to buy us some time.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Alonso|Sad][EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Are you feeling quite alright?[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Ron: Maybe the mushrooms have finally gotten to them.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: I'm just going to distract the spiders for a while. Stop acting like I'm crazy.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  You are sounding kind of crazy. Going off by yourself won't work. Splitting up only worked to escape.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] I got this Frederick, now quit cramping my style.[BLOCK][CLEAR]") ]])	
  --have actors move towards Isaac actor in this scene
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I flex internal muscles and try to mark my surroundings as strongly as possible with a bright blue scent.[BLOCK][CLEAR]") ]])	
  -- [EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Apparently the first part of their grand plan is look constipated.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Ron|Happy][EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Ron: So like most of their plans then?[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] Ron and Frederick high five while I flip them off twice as hard as any regular human.[BLOCK][CLEAR]") ]])	
  --move Frederick and Ron together
  --[EMOTION|Ron|Neutral][EMOTION|Frederick|Sad][EMOTION|Alonso|Neutral]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Alright you fucks I'll be back in an hour.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Alonso|Sad] Alonso moves to stop me, he puts his hands on my shoulders looking at me earnestly.[BLOCK][CLEAR]") ]])	
  --have Alonso move towards Irene here
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You are sure you will make it back? No suicide missions.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] I'm sure. I'm gonna lead the spiders on a chase to remember. I'll be back in a bit and then we can get moving.[BLOCK][CLEAR]") ]])	
  --Remove Actors here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Ron] With that I head off into forest.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Irene|Special]
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] Here's to hoping that I am as slick as I think I am.[BLOCK][CLEAR]") ]])	
--NOTE TO SELF: another place where I might be running long in the tooth.
--[EMOTION|Irene|Fighting]
--add spiders here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's a simple plan, make a hell of a distraction, keep running and use the scents so I don't get more hopelessly lost.[BLOCK][CLEAR]") ]])
  --Lots of motion here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I thrash plants, start fires and sing to get the spiders to chase me.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It works and I lead them on a merry chase.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My stronger legs give me the speed and stamina to stay barely ahead of the spiders.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The scents work at an impressive distance and give me a guiding light to navigate by.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: In the confusion of the forest this helps more than you might think. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: That and so far from Ron's fires the scents seem sharper, giving me a better idea where the spiders are.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long it seems I have every spider around chasing me.[BLOCK][CLEAR]") ]])	
  -- [EMOTION|Irene|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised]The spiders close in and for a moment it seems that I might of actually done something foolish for once.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Smirk] Then I see a a tree at an odd angle and an idea strikes.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I shake the tree trying to find the right speed to shake it loose.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My extra arms help give me the strength to vibrate it just right.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Come on![BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Then I hit just the appropriate frequency and it shakes loose. Starting a chain reaction of falling trees.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy]The spiders afraid of being squished like regular spiders back off.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I slip and slide in a desperate rush to return to the spot I marked.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone is surprised to see me when I return.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene:[MOVECHAR|Irene|Middle-Body-HfBody|QUADINOUT] [EMOTION|Irene|Laugh][ADDCHARPOS|Ron|-1|Neutral|Middle-HFBody][ADDCHARPOS|Alonso|-1|Neutral|Middle+Body+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle+HfBody] I leave for a little while and everyone gets all mopey.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Ron: You returned![BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We were about to go looking for you. I figured we find you caught in a web or something.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You mad bastard.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Heh. I told you I had it handled.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Irene:  Although we really need to start to running. For some reason the spiders seem super angry.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Laugh] You can't do anything without pissing them off.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Neutral] Hey, at least they are consistent.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The time to rest helps the others keep running. I could probably of used some of that, but such is life.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Ron|Fighting][EMOTION|Alonso|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] It's hard fighting from there on though. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|SpiderA|-1|Neutral|Ledge][ADDCHARPOS|SpiderB|-1|Neutral|Ledge+Body] The spiders are hunting in us in a coordinated way.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: When we drive one group back, another is always ready to swap in. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I step up my efforts in an attempt to make up the gap.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I lead every charge as a whirling dervish of death and destruction.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Which I compliment by becoming so positive that everyone starts to loathe me. [BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Sad][EMOTION|Alonso|Sad][EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised] By the end of the second day however we are all stumbling in our steps and despite everything the spiders are right behind.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Ron|Fighting][EMOTION|Alonso|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|SpiderA][REMCHAR|SpiderB] The trees ahead tremble and everyone gets ready for real bad time.[BLOCK][CLEAR]") ]])	
  fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/330 1 escape part 2.lua", "Begin Scene")
  end