  --final part of the escape sequence, leads to endings
  
  --[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.
--*AI make this a tearjerker you bastard, how to do that? Forma is cuddly, but smiling sadly? CHeck twine game again.
if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I awake with a start, gasping deeply.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: After a bit of coughing I am able to take some breaths of blissful air. [BLOCK][CLEAR]") ]])
    if(iAntIntimacy == 0.0) then
      --[BACKGROUND|ForestG]
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestB] [ADDCHARPOS|Handmaiden|-1|Neutral|Middle+Body][ADDCHARPOS|Ron|-1|Neutral|Middle-Body] Forma is looking down at me smiling. Next to her is Ron who seems relieved.[BLOCK][CLEAR]") ]])
--note to self: spot for $antintimacy is 1
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma is naturally silent, but she lets out a corona of warm scents flavored with relief.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestB][ADDCHARPOS|Handmaiden|-1|Neutral|Middle+Body][ADDCHARPOS|Ron|-1|Neutral|Middle-Body] Forma is resting my head on her thighs, looking down at me with concern.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron is sitting next to me holding one of my hands.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma is naturally silent, but she lets loose a prominence of relief that temporarily blinds my scent-o-vision.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: When I can discern scents again I try to let her know I'm okay via scents.[BLOCK][CLEAR]") ]])
end	
	fnInstruction([[ WD_SetProperty("Append", "Ron: We were worried for a bit there.[BLOCK][CLEAR]") ]])
  --[ADDCHARPOS|Frederick|-1|Happy|Middle-HfBody]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-Body] And you call me lazy.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy]  
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]I stand unsteadily, Forma helping me up. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[ADDCHARPOS|Irene|-1|Neutral|Middle] I'm not the one who needs to be scared awake Frederick.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Angry] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You watch cattle and try to not nod off![BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Alonso|-1|Neutral|Middle+body+body] Alonso gives me a hardy pat on the back.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Knew you could take it. Now if everyone is ready, I would like to leave.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: [ADDCHARPOS|Cohen|-1|Neutral|Middle+body+body+body-10] The ants said you would pull through, I never doubted it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: They were worried but won't admit it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] [EMOTION|Cohen|Neutral][EMOTION|Frederick|Neutral][EMOTION|Alonso|Neutral][EMOTION|Handmaiden|Neutral][EMOTION|Irene|Neutral] The area outside the hut has been reduced to ash and ruin. Burning hell on earth on comes to mind[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Holy hell. I've never seen it's like.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Goddamn, this frightening.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Fire must always be respected younglings.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Cohen|Neutral] Ron has an expression similar to man who caught more fish than they were allowed and in the process caught the biggest one.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ants frantic uprooting of anything near the hut worked, and probably saved everyone's life. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's careful going through the charred remains of the forest, many of the trees have fallen and embers are everywhere.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone including the ant-girls shoots dirty looks at Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He surveys the devastation and starts to whistle a happy tune.[BLOCK][CLEAR]") ]])
--note to self: I'm pretty sure this would lethal actually, the amount of carbon monoxide would be insane
--[EMOTION|Cohen|Happy][EMOTION|Frederick|Happy][EMOTION|Alonso|Happy][EMOTION|Handmaiden|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] After a good deal of hiking we reach a large stream. The water is wonderfully cold to drink.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The stream managed to stop the fire, and I can almost smell the purer air coming from the otherside.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: We all stop in the stream to wash off the soot, leaving large dirty trails behind us. [BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick is staring as the ant-girls wash off. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Something caught your eye Frederick?[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Alonso|Neutral] They are good looking damn you. [BLOCK][CLEAR]") ]])
  -- [EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene: It's weird because I look like them. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'm sorry![BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso has started giggling.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Sorry about him, we are just not quite to used to it. We will try better. [BLOCK][CLEAR]") ]])
--Note to self: this part might need cutting, has too much a trans allegory feel to me which I'm probably bungling.
--[EMOTION|Frederick|Happy][EMOTION|Handmaiden|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised] Forma makes a particular effort to scrub me clean. It's borderline painful but I do wind up shiny.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special][EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She looks a bit like a mother cat afterwards, and is giving off a proud scent.[BLOCK][CLEAR]") ]])
  --[BACKGROUND|ForestE]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] With that we cross the stream, resuming our hike. I didn't miss not having to deal with underbrush, although my hard chitin makes it easier.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Cohen|Neutral][EMOTION|Frederick|Neutral][EMOTION|Alonso|Neutral][EMOTION|Handmaiden|Neutral][EMOTION|Irene|Neutral] We eventually hit a sunken road, it's stones all but overgrown.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Hey look here![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Affixed to the side of trees at regular intervals are small wooden lanterns. They mark out an ancient path.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: How strange, I wonder what they did?[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
  fnInstruction([[ WD_SetProperty("Append", "Ron: They are magical to be sure, but don't know what they do. Maybe if I had my book.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Lanterns like those light the way. Be cautious if you ever meet something that can actually light those.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Huh, bet there is a story there. Still given how given Cohen is to tell stories if he isn't volunteering he's too tired to tell it.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Ron|Neutral][EMOTION|Frederick|Neutral] Finally after a few more hours following this road, the trees begin to thin and I can see blue skies.[BLOCK][CLEAR]") ]])
--[EMOTION|Cohen|Happy][EMOTION|Frederick|Happy][EMOTION|Alonso|Happy][EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy]Finally I can see a field beyond the forest. Everyone lets out a sigh of relief.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] It took more than I had, but I managed to get my friends out of forest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Handmaiden|Ledge+hfbody|QUADINOUT][MOVECHAR|Irene|Middle+body|QUADINOUT][MOVECHAR|Ron|Middle|QUADINOUT][MOVECHAR|Frederick|Middle-body|QUADINOUT] The ants stop marching with us at the edge of the forest, they seem to afraid to go too far.[BLOCK][CLEAR]") ]])
  --large antintimacy fork, mostly about how she doesn't want you to go
     if(iAntIntimacy == 0.0) then
         --[EMOTION|Handmaiden|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Forma looks unmistakably sad. [BLOCK][CLEAR]") ]])
--note to self: so in twine version, only mushrooms has the marriage ending. With a bit of elbow grease I might make it work for regular and sisters. Not sure it's worth it. Not sure it works thematically in ant 2 she can't communicate, and the other marriage endings imply it might be a cruelty. 
--NOTE TO SELF: Forks for $antlove is 1
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Ledge+body+body|QUADINOUT] She hugs me, then still gripping me loosely she maneuvers her antenna into mine[BLOCK][CLEAR]") ]])
  --move Isaac and Handmaiden actor together, maybe focus in the camera on the two of them
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It tingles)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (I owe her so much but I can't even communicate with her)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I.. Thank you.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen, tell her what I said.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen:  She knows.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma nods slightly, a sad small smile on her face. [BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle+hfbody|QUADINOUT][MOVECHAR|Handmaiden|Middle-hfbody|QUADINOUT][REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Cohen][REMCHAR|Ron] [EMOTION|Irene|Blush]She takes this as an opportunity to drag me behind a nearby tree for her own farewell. Everyone else, even the ant-girls look pointedly distracted.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:  Forma begins to make out with me in a manner best described as frenzied. Her larger hands roam all over my body and her smaller pair threatens the slit on my abodomen. She rubs her body against my needfully.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: She's obviously ready to go right here.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Happy] I gently but persistently push her back. That's not how I'd like this to go.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (She saved my life. Well closer to we saved eachother.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I can't even say I know her that well. But I'd like to.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It's a whirlwind romance but hell that's what adventure, no living is about.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: You saved my life. Please Forma, let me show you the world beyond this forest.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I don't know much about ant-girls. I don't know what you are leaving behind so I can't speak to that.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: But I left my own home to go on a wild adventure. It's been amazing and terrifying and just so much.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Come with me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] Well assuming you can understand any of that.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Happy][MOVECHAR|Irene|Middle|QUADINOUT] I take one of her hands in three of mine, and point with my other large hand towards the open plain. At the same time I try to make the warmest most inviting scents I can.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma gives me a look that's truly conflicted. She lets go of my hands. Looking sad moves back towards the forest.[MOVECHAR|Handmaiden|Middle-body|QUADINOUT][BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Sad] (I guess that's a no.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (She wants me to stay, but I have to go. I've got my own adventures to find.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] I give her a sad smile. Forma returns the smile knowingly.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: As I pat her on the head, she hugs me with all four arms.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Blush]  We hug for a while and then I kiss her.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: It won't rank among the greatest kisses in the world, but to me and to her it means everything.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally she releases me. She curtseys deeply, she might be crying a little.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] [ADDCHARPOS|Ron|-1|Neutral|Redge-50][ADDCHARPOS|Cohen|-1|Neutral|Redge-body][ADDCHARPOS|Frederick|-1|Neutral|Redge-body-body][ADDCHARPOS|Alonso|-1|Neutral|Redge-body-body-body][MOVECHAR|Irene|Ledge+hfbody+body|QUADINOUT][MOVECHAR|Handmaiden|Ledge+hfbody|QUADINOUT]We return to everyone else. My friends are all giving me supportive looks. The ant-girls are as emotionless as ever, however there is an overwhelming cloud of green tinged with impropriety and perhaps envy.[BLOCK][CLEAR]") ]])
    end
  --[=[Your ant-girl takes this as a opportunity to drag you behind a nearby tree for her own farewell.

She starts making out which you in a manner best described as frenzied. Her hands roam all over your body and her smaller pair begin to tease the slit on your abdomen. At the same time she starts to hump our leg. She is obviously ready to go right here right now. 

You push her away and then smile at her sadly. She returns the look. She obviously wants you to stay. You pat her head comfortingly, and she hugs you with all four arms. (She also takes this as an opportunity to kiss your neck.)

You hug her for a while and then kiss her romantically. Perhaps it won't ever be rated one of the greatest kisses in the world, but to you and her it means everything. She releases you and the gives you a deep curtsy. She might be crying a little.

The pair of you walk out to where the others are waiting. Your friends have an smug expression. From the ant-girls however, there is a overwhelming cloud of green tinged with impropriety and perhaps envy. The ant-girls all bow to your group in unison and then head back into the forest.
end
]=]

  --[=[
  So how to make this properly sad? 
  
  The handmaiden pulls you behind a tree to make out. You make out a bit and then pull her towards the open plain.
  Adventure changed your life, she can join you. There are so many things you want to show her.
  You'll admit you don't really know her, but you like to get the chance.
  The ant-girl looks into your eyes, releases a note of particularly deep sadness and then carresses your hair before turning away.
  
  ]=]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She lines up her ant-girls and the bow to your party in unison.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then just as silently as they arrived they head back into the forest.[REMCHAR|Handmaiden][BLOCK][CLEAR]") ]])
  --exunt
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Cohen|Neutral][EMOTION|Frederick|Neutral][EMOTION|Alonso|Neutral][EMOTION|Irene|Sad][EMOTION|Ron|Neutral] Everyone was sad to see them go, they had become sisters in arms in our time of need. [BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
   local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Ending\")"
    fnInstruction(sString)
  
  
  
elseif(sTopicString == "Ending") then
  --variables
      local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
  
  WD_SetProperty("Hide")
     fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
      fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --[EMOTION|Frederick|Happy] 
    --[BACKGROUND|ForestF]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[BACKGROUND|ForestB] Well I don't think I'm wrong when I say that was a skin of the teeth adventure.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We nearly died and we don't even have any treasure or acclaim to show for it.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Irene|Smirk] We are alive and have our freedom, that's quite a reward.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Ron: Also I got to show why fire magic is the best magic.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Mad][EMOTION|Frederick|Mad][EMOTION|Alonso|Mad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] Everyone takes a chance to curse at Ron for almost killing us all again. The language is too foul to be printed unfortunately.[BLOCK][CLEAR]") ]])
--NOTE TO SELF: Add sequence where it's part of everyone cursing at Ron, including Forma doing silently
--[EMOTION|Cohen|Happy][EMOTION|Frederick|Happy][EMOTION|Alonso|Happy][EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] We eventually reach a small town close to the edge of the forest. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A group of girls, including an ant-girl walking through town wearing destroyed rags attracts more than a few stares.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:Alonso predictably doesn't care. Frederick is too tired to show off. Ron still has enough energy to be embarrassed though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: We finally reach the local tavern.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen busies himself with ordering and hitting on the barmaid.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone else spends their time examining themselves in relative silence.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally the food arrives, never have beans and chickens tasted so good.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone eats with a frantic speed till Cohen tells them to pace themselves so they don't get sick.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Narrator: Even when not eating themselves sick everyone is silent. They all seem to be examining their changed forms.[BLOCK][CLEAR]") ]])
    	fnInstruction([[ WD_SetProperty("Append", "Narrator: Well except for Cohen, he's halfway to scoring with the barmaid.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral][REMCHAR|Cohen][REMCHAR|Frederick][REMCHAR|Ron][REMCHAR|Alonso][MOVECHAR|Irene|Middle|Teleport] (Now that I'm not in mortal peril I should probably think about what comes next.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (These new arms of mine saved my life. They gave the me strength and speed I needed to make the rescue work.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (They would be damn handy elsewhere, but the only ant-girls I ever saw were in that forest, I'd be forever marked out.)[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
     	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alternatively I might try to just take the ant out of ant-girl and become a human woman. )[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The others wouldn't mind or judge, so it's what I'm comfortable with really.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Finally I could try to get back to what I was, being a man does have it's perks)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Oh full belly I missed you.)[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alternatively I might try to just take the ant out of ant-girl and go back to being a human-girl. )[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I suppose I could try to become a man, it seems to have it's perks.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The others wouldn't mind or judge, so it's what I'm comfortable with really.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Oh full belly I missed you.)[BLOCK][CLEAR]") ]])
    end
--To stay an ant-girl would be best
--To aim for womanhood
--To get back to the state I worked so hard for
--Alternative ending
fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/400 1 Endings.lua", "Begin Scene")
  end