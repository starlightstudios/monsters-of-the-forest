--Ant 1+1 tf scene
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
     --[EMOTION|Irene|KSpecial] 
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Offended]  (Oh hell, I'm tingling. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If I have learned anything so far that's not a good sign. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] (Also that I should always eat as much breakfast as I can, that wouldn't solve any of my problems but current events are bringing it into sharp focus. [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Maybe I should start putting snacks in my clothes like Ron does.)[BLOCK][CLEAR]") ]])
  --go to kneel or black screen here
  -- [REMCHAR|Irene]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The tingle starts in my arms, it gently probes the margin betwen the hard brown chitin and my flesh[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Like water gently spilling from a cup it works it's way slowly, surely up my arm. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: it tickles as it covers my shoulder and armpit, leaving the joint hard and shiny.[BLOCK][CLEAR]") ]])
--If male:
   if(sGender == "Boy") then
           fnInstruction([[ WD_SetProperty("Append", "Narrator: Hey I can put my palm to my shoulder![BLOCK][CLEAR]") ]])
end
 	fnInstruction([[ WD_SetProperty("Append", "Narrator: The tingle starts at your knees, it is unnerving to watch my knee lengthen and grow a plate of chitin to cover the joint.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The brown margin slowly creeps up my leg, spreading out from my thigh and gradually encasing the entire leg, finishing just below my new abdomen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a strange pulling feeling, as the leg moves outwards connecting into my torso from the side.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It is a far cry from human anatomy now.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Thick waves of the unnatural calm are the only thing keeping me from panicking at this point.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My arms and legs now look fully insectoid. It reminds me of something but I can't quite name it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a tickling beneath my armpits, with a bit of contorting I see a line of black chitin growing from my spine to make a plate of chitin.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a tantalizing slowness a pair of second, simpler arms emerges from the plates. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They hang limply for a second, the three fingered hand not moving[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Offended] (Move darn it!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Move!)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Angry] (Move damn you!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral](Huh, nothing.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a tingle near the base of my neck and suddenly the spare hands are making fingers guns.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The spare arms don't have the full range of reach or motion as the original set, but they seem damn handy)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (That must be it, nothing else could possibly change.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Now let's get going.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Cry][BACKGROUND|Null] Then I suddenly go blind. Terror rears it's ugly head.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Only the unnatural calm and a greater fear of being found while blind stops me from crying out.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] Then just as suddenly I can see again. Somehow better than before, the shadows of the forest seem less oppressive and deep.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] A quick examination with the mirror shows that my eyes have become opaque and black, giving me an alien look.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As I examine myself in the mirror, I see that my hair is rapidly changing color, and shortening to a uniform length[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My face gently tingles all over as it rapidly softens becoming more girlish[BLOCK][CLEAR]") ]])
if(sGender == "Boy") then
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (Yeah, I look cute. The others are going to tease me.)[BLOCK][CLEAR]") ]])
else
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (I'm even less intimidating now! Good thing I still got the attitude to make up for it.)[BLOCK][CLEAR]") ]])
end    
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a strong double noogie sensation on my forehead, as a pair of brown soft antenna sprout.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They mock my confusion by gently waving.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: This is followed by a strong stroking feeling on my sides, it's a strong confident sensation.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: In turn this lights an internal fire, which is unexpected.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: That helps distract from the fact that my waist is gradually sinking and my insectoid abdomen seems to be filling out in response.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: By the time it's finally over my waist is inhumanly thin, and my abdomen is taught and mature.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Examinations of the alien abdomen find a tight slit towards the end.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I probe it gently, not quite sure what to expect.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Hnnng![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Holy hell, let's not do that again)[BLOCK][CLEAR]") ]])
  --probably stop the black screen here, may need a scents effect. Not sure it's in the budget or engine compatible though
  --[ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I get to my feet slowly, with a sigh. Today is just plain pushing it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I am quickly distracted by a number of indescribable colors hanging in the air.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Those were better mushrooms than I thought)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (Aw crap I'm gonna have to throw up aren't I?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm pretty great but making the rescue while seeing stuff that isn't there is probably beyond me.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] As I think this I notice that I am excreting a green color myself, tinged with consternation.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying to focus on this color is like shoving my hand into bag made of a million fabrics. It's too much information at once.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Still with a bit of trying, I manage to use muscles I didn't know I had and stop making the color.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I give myself a quick look over.[BLOCK][CLEAR]") ]])
--have portrait turn and move slightly during this sequence.
	fnInstruction([[ WD_SetProperty("Append", "Irene: I look like some sort of ant person girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Maybe? [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Or some sort of ant person.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: No.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: A kind of ant girl person.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Ant girl![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I look like an ant-girl.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] I hope everyone recognizes me while I make the frigging rescue.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  [EMOTION|Irene|Neutral] This gives a feeling of paranoia about not being recognized, so I keep enough of my clothes to be an identifying mark.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] The colors are all around me as I continue. [BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/240 1 trapped ant.lua", "Begin Scene")
  end