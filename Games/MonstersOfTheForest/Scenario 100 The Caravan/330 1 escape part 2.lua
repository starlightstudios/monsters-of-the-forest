--second part of the escape sequence, where cohen rejoins the party. In no rescue leads straight to endings
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.
--*AI: so how Forma acts aound the player is slightly different in AI, she's more nuturing and even less willing to see the player character in danger. Read the twine part for inpsiration first.
if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  --if $antrescue
   fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle-hfbody-body-body|QUADINOUT][MOVECHAR|Ron|Middle-hfbody-body|QUADINOUT][ADDCHARPOS|Cohen|-1|Neutral|Middle-HfBody-body]Cohen steps through the trees looking pleased with himself. [BLOCK][CLEAR]") ]])
  if(iAntRescue == 1.0) then
  --fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Cohen|-1|Happy|Middle-HfBody] [ADDCHARPOS|Handmaiden|-1|Happy|Middle-HfBody] Cohen steps through the trees looking pleased with himself. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Knew I'd find you younglings eventually.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KHappy] [EMOTION|Alonso|KHappy][BLOCK][EMOTION|Ron|KHappy][EMOTION|Irene|KHappy][CLEAR]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Irene][REMCHAR|Frederick][REMCHAR|Alonso][REMCHAR|Ron] Everyone collapses in relief. [BLOCK][CLEAR]") ]])
--SD: party goes to kneeling
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Handmaiden|-1|Neutral|Middle+HfBody] He is followed by a patrol of ant-girls, lead by Forma. Despite her similarity to the other ant-girls she stands out to me.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special]
     if(iAntIntimacy == 0.0) then
       fnInstruction([[ WD_SetProperty("Append", "Narrator: She sees me collapsed and gives me a few comforting pats on the head.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator: She sees me collapsed, she runs over and looks me over carefully. Then with a hint of a smile she kisses my forehead.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Using both hands she pets my hair as she releases a cloud of happy scents. Must of been worried.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I see Frederick's curious stare and I try to play it cool.[BLOCK][CLEAR]") ]])
end
  --Have forma to Irene actor
	fnInstruction([[ WD_SetProperty("Append", "Alonso: How the hell did you get away, Cohen?[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: What you think a few patrols of spiders at once were enough to force me back?[BLOCK][CLEAR]") ]])
--Need to put a beat here
	fnInstruction([[ WD_SetProperty("Append", "Everyone: ....[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen:  [EMOTION|Cohen|Neutral] Oh fine, they forced me back. So I went back to town and got some supplies.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Then I tracked you younglings. Good job on the fires Ron, they were hard to miss. Good job leading them this far Alonso.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Cohen|Neutral] [EMOTION|Alonso|Neutral] What's with all the ants? I figured we were the only ones with an ant in our party.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody-Body] (Up yours Frederick.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Angry]Up yours Frederick.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[REMCHAR|Irene][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body]  Sorry I didn't mean it like that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Irene|Neutral][EMOTION|Frederick|Neutral][REMCHAR|Frederick] As I was tracking you I ran into this patrol. The lead ant claimed to know you. Said you had some fool idea to attack the spider nest by your lonesome.[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 1.0) then
fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral]That's all she said?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen just looks at me and chuckles. [BLOCK][CLEAR]") ]])
end

  --[EMOTION|Cohen|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Good job rescuing her by the way.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[ADDCHARPOS|Irene|-1|Happy|Middle-HfBody-Body] Wait you can communicate with her?[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Irene|Neutral] Some of the ants know sign language. Used as a... oh damn I can't remember the word. I remember it in Bastard Slaver though. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Trade language?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Thank you Ron. Trade lanague, especially for other races that don't speak much.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Good job on the escape younglings. But let me and the girls take the lead from here. Hard part is just starting.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma smiles at you as you talk to Cohen. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen also brought food and some weapons for everyone. The fighting is still going to be hard but it might be winnable now. [MOVECHAR|Cohen|REdge-50|QUADINOUT][ADDCHARPOS|Frederick|-1|Neutral|REdge-Body][ADDCHARPOS|Alonso|-1|Neutral|REdge-Body-Body][ADDCHARPOS|Ron|-1|Neutral|REdge-Body-Body-Body][MOVECHAR|Irene|REdge-Body-Body-Body-Body|QUADINOUT][MOVECHAR|Handmaiden|REdge-Body-Body-Body-Body-Body|QUADINOUT][BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He also knows the way out, which helps immensely.[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 1.0) then
       fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma makes a show of making sure I'm behind her as she brandishes her weapon. She's probably trying to show off. [BLOCK][CLEAR]") ]])
end
--[EMOTION|Handmaiden|Fighting][EMOTION|Ron|Fighting][EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Cohen|Fighting][EMOTION|Alonso|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Still despite the obvious difficulties it seems like the spider-girls have a hard on to capture us.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: How spider-girls have a hard on for anything is not going to be addressed here.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They attack often and in such numbers it seems obvious that the greater part of the spider nest has been mobilized against us. [BLOCK][CLEAR]") ]])
  --(if: $youfight is 1)
          --fnInstruction([[ WD_SetProperty("Append", "Narrator: I know punches to the box hurt but I didn't think they hurt that bad.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls led by Forma are helpful, but constrained by the fact that only Cohen can talk to them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It takes a bit of work and some improvising but eventually I work out a system where Forma and I emit colors to eachother as shorthand for commands.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma for her part delights in trying to protect me. Which is good because the spiders seem to really dislike me in particular.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Worried] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: More than once she grabs me by the scruff of the neck, pulls me to safety and then attacks the spiders with such ferocity that even they back up.[BLOCK][CLEAR]") ]])
--note to self: should probably expand above 2 lines into their own episode.
--[EMOTION|Handmaiden|Fighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Despite her cute appearance Forma proves to be a fearsome combatant. The ant-girls only have simple digging tools as weapons, but Forma's skill resembles an experienced fencer.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special]
    if(iAntIntimacy == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She also puts off a corona of warm happy colors every time she protects me. Which is kind of embarrassing.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator: She kisses me on the cheek every time she protects me and then follows up with a supportive head pat. It's embarassing.[BLOCK][CLEAR]") ]])
end  
  -- [EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Hmm, is something going on? You keep blushing whenever that one ant-girl looks at you?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Frederick's sharp as always.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] No Frederick, just an ant thing.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Special]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I mean I don't blame you, these are some good looking girls.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] What the hell man?[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The color drains from his face as he realizes what he just said.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Oh right, my bad.[BLOCK][CLEAR]") ]])
--[EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Handmaiden|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral]  The spiders push us back, controlling our route somewhat. Still whenever I start to get worried I look at Cohen's audacious smile.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally there is a showdown, the spider girls come in from all sides. The ants move as one forming a tight circle around Ron. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My group does our best to bolster their ranks. Everyone cackles madly, Ron looks besides himself with joy.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: One spider girl with silver hair has it out for me in particular, making a nasty stab.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If I parry back I'll run into Ron, but if I parry to the side I'll put myself at risk.)[BLOCK][CLEAR]") ]])
  --lots of motion here
--Parry back
--Parry to the side
--note to self: parry to the side starts the queen showdown route. If you do it, Forma covers the gap you leave then kisses you on the cheek.

--Parry to the side:
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry] (I got this. No spider is getting the better of me![BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Offended]  The hit is heavy and it forces me back. I use three arms apply force, my last arm windmills wildly trying to keep my balance.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My arm ends up jogging Ron's arm just as he is casting a spell.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A fire ball flies wild, up above the trees and off into the distance. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As the spiders retreat I spot a tower of flame briefly sprout in the distance.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Weird, well it's not my problem. Not like I need any more problems right now.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Sad][EMOTION|Alonso|Fighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Handmaiden|Neutral][EMOTION|Ron|Neutral][EMOTION|Frederick|Neutral]After a monumental effort the attack is beat off. Everyone cheers. Well the ant-girls just look happy.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone except for Cohen who is consumed in staring at a dark cloud on the horizon. [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (Uh oh.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] He and Forma climb a nearby tree.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Sad][EMOTION|Handmaiden|Worried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Both return looking worried. Forma is giving off a cloud of nervous green.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Cohen|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Ron! Did you dispell that fireball that went wild?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I mean, I did put a little bit more power behind it than normal, but the forest is damp enough that it should be fine.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: So you didn't dispell it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Nope. Something wrong?[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Sad] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen and Forma exchange some frantic signals.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Once they finish Forma turns and starts sending scent signals to the other ant-girls. Fear is a definite undercurrent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: You started a forest fire. You must of hit a burial ground.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: More likely you hit a dumping ground. Decaying crap goes up like nothing else. I've shoveled enough of it to know.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene: Oh hell, I don't think we are in the condition to out run a forest fire.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You can't out run a forest fire. Ask her if there are any bodies of water nearby maybe?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (All this and we are going to burn to death?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen and Forma exchange some signs, growing more frantic as they continue. [BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: She says there is a place where we can ride out the fire.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Cohen|Neutral] Hopefully.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As we head off into the forest, I can see a growing cloud of smoke on the horizon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ants set a quick pace, I'm able to keep up but the others are having a hard time.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: We come to a small earthen hut built solidly on a rocky outcrop. The walls are reassuringly thick.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Forma takes me by the hand and leads me into the hut.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (I feel a bit like child.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][MOVECHAR|Irene|Middle-HFbody|QUADINOUT][MOVECHAR|Frederick|Middle+HfBody|QUADINOUT][MOVECHAR|Alonso|Middle-Hfbody-Body|QUADINOUT][MOVECHAR|Ron|Middle+Body+HFBody|QUADINOUT][MOVECHAR|Cohen|Middle+Body+body+HFBody-10|QUADINOUT][RemCHAR|Handmaiden] She motions for everyone to stay in the hut. Then she moves quickly to rejoin her sisters.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Nobody has been here for a while. Look at all the moss. I wonder who built it.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  Nice cool moss to relax on. It'll be nice to rest.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Irene: Let's hope it's enough.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Cohen|Neutral] Ants found it probably. They prefer to dig than build standing structures.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Ron: Look at them go.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: We all turn to watch the ant-girls. They are working with a feverish intensity to clear anything burnable from the surrounding area.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They are obviously worried.[BLOCK][CLEAR]") ]])
--NOTE TO SELF: Do I need the above line?
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (After all the fighting this is what has them worried? That's unnerving.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a fearsome crashing sound as they methodically pull down the nearby trees.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone spends the time in the hut resting, and sharing what food Cohen brought. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I do my level best to make my various misadventures sound impressive. [BLOCK][CLEAR]") ]])
   if(iAntIntimacy == 1.0) then
fnInstruction([[ WD_SetProperty("Append", "Narrator: I tactfully neglect to mention my dalliane with Forma. I'm not the type to kiss and tell.[BLOCK][CLEAR]") ]])
end    
  --[EMOTION|Cohen|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I am rewarded by a pat on the shoulder from Cohen.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] Cohen gives a more indepth account of what he did once he reached town. Apparently he strong armed the local general store.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Cohen|Neutral]Ron looks consumed with a question[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Cohen, do you know why they turned us into women?[BLOCK][CLEAR]") ]])
  -- [EMOTION|Cohen|Angry] 
	fnInstruction([[ WD_SetProperty("Append", "Cohen:To make better slaves.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][EMOTION|Frederick|Neutral][EMOTION|Alonso|Neutral][EMOTION|Ron|Neutral] We all quiet down.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Oh hell, there is very little Cohen hates more than slavery. He is remarkably calm for someone who has engaged in combat the majority of his life. But his true anger is reserved to for slave holders.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (To see Cohen truly, brutally angry is a terrifying sight.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: They would of changed the rest of you like our buggy friend. It reduces the odds that humans retaliate. Also makes it harder for them to escape.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Everyone:...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] [EMOTION|Cohen|Neutral] Hey Ron did you prepare your fire resist spell? [BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
  fnInstruction([[ WD_SetProperty("Append", "Ron:  Yes.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Don't you normally prepare it twice? That might help us.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Ron:  Oh, well I cast it this morning and then again shortly after you broke me out of the cocoon.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Sad] Well there goes that idea. Good thinking though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][EMOTION|Frederick|Neutral][EMOTION|Ron|Neutral] We all watch the fire move closer, nothing else to do.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The first thing I notice is that all the colors in the air start to become wavy and less clear. Then I start to feel out of breath.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Sad] It's like after sprinting and you can't get your breath back despite breathing heavily.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: You don't feel well do you?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: No, it's like being half underwater.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: [EMOTION|Cohen|Neutral] Ants do some of the breathing through the skin. The smoke will disagree with you more than us.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Keep your breathing steady, and don't panic.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSad][EMOTION|Alonso|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised] Alonso shifts so I can lean on him.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Even as a girl he has broad shoulders)[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|KHappy] 
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Seems your ant powers have a downside. Good news for us puny humans.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: ...I'm not... going to dignify that... with a response.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|KSad] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso's face goes dark with worry.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|KHappy] 
  fnInstruction([[ WD_SetProperty("Append", "Alonso: You did great today, just rest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Handmaiden|-1|Neutral|Middle+HfBody][MOVECHAR|Frederick|Middle-HfBody-body-body|QUADINOUT] The ants return to the hut, covered in dirt and splinters.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KSpecial]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma lies down on the ground next to me, she holds my hands and then with a sad look kisses me on the cheek.[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 1.0) then
fnInstruction([[ WD_SetProperty("Append", "Narrator: She holds all of her hands in mine and holds me close. Through the smoke I can see that she's wreathed in worried scents.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She's done all that she can, and she is sorry.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My head is pounding and the sky is clouded over with smoke. It's a struggle to keep my eyes open, and I can't see any scents.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSpecial]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Panic raises it's ugly head.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: What will we do if this doesn't work?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Die.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Things go black.[BACKGROUND|Null] [REMCHAR|Ron][REMCHAR|Frederick][REMCHAR|Cohen][REMCHAR|Alonso][REMCHAR|Handmaiden][BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/340 1 escape finish.lua", "Begin Scene")
end


if(iAntRescue == 0.0) then
--if $norescue

--fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen steps through the trees looking pleased with himself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Cohen: Knew I'd find you younglings eventually[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene][REMCHAR|Frederick][REMCHAR|Alonso][REMCHAR|Ron] Everyone collapses in relief. Cohen smiles warmly.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Alonso: How the hell did you get away, Cohen?[BLOCK][CLEAR]") ]])
--[EMOTION|Cohen|Special]
fnInstruction([[ WD_SetProperty("Append", "Cohen:  What you think a few patrols of spiders at once were enough to force me back?[BLOCK][CLEAR]") ]])
--Need to put a beat here
fnInstruction([[ WD_SetProperty("Append", "Everyone:...[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Cohen:  [EMOTION|Cohen|Neutral] Oh fine, they forced me back. So I went back to town and got some supplies.[BLOCK][CLEAR]") ]])
--[EMOTION|Cohen|Happy]
fnInstruction([[ WD_SetProperty("Append", "Cohen: Then I tracked you younglings.  Good job on the fires Ron, they were hard to miss. Good job leading them this far Alonso.[BLOCK][CLEAR]") ]])
 if(sGender == "Boy") then
   fnInstruction([[ WD_SetProperty("Append", "Cohen: I'm guessing your ant companion is Isaac[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Yeah, the spiders did something that makes you a bit changable. I think it's why everyone else is a girl.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Cohen: I'm guessing your ant companion is Irene[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: Yeah, the spiders did something that makes you a bit changable. I think it's why everyone else is a girl.[BLOCK][CLEAR]") ]])
end
  fnInstruction([[ WD_SetProperty("Append", "Ron: I wonder why?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Cohen: Questions for another time younglings. For now eat up, arm up and then form up.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Cohen: Hard part is just starting.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Cohen|REdge-Hfbody|QUADINOUT][ADDCHARPOS|Frederick|-1|Neutral|REdge-Body][ADDCHARPOS|Alonso|-1|Neutral|REdge-Body-Body][ADDCHARPOS|Ron|-1|Neutral|REdge-Body-Body-Body][ADDCHARPOS|Irene|REdge-Body-Body-Body-Body|QUADINOUT][BLOCK][CLEAR] Cohen brought food and some weapons for everyone. The fighting is still going to be hard but it might be winnable now.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Narrator: He also knows the way out, which helps immensely.[BLOCK][CLEAR]") ]])
--[EMOTION|Handmaiden|Fighting][EMOTION|Ron|Fighting][EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Cohen|Fighting][EMOTION|Alonso|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Still despite the obvious difficulties it seems like the spider-girls have a hard on to capture us.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: How spider-girls have a hard on for anything is not going to be addressed here.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|SpiderA|-1|Neutral|LEdge][ADDCHARPOS|SpiderB|-1|Neutral|LEdge+HFbody] They attack often and in such numbers it seems obvious that the greater part of the spider nest has been mobilized against us. [BLOCK][CLEAR]") ]])
  --(if: $youfight is 1)
          --fnInstruction([[ WD_SetProperty("Append", "Narrator: I know punches to the box hurt but I didn't think they hurt that bad.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The fighting is hard and constant. Everyone is pushed to the limit and more than once it seems like we might be overwhelmed.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We flee the fights we can't win and thanks to Frederick and Cohen manage to dodge multiple patrols.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: This is dangerous though, as it lets the spiders control the path. [BLOCK][CLEAR]") ]])
--[EMOTION|Frederick|Fighting][EMOTION|Irene|Fighting][EMOTION|Handmaiden|Fighting]
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [Remchar|SpiderA][Remchar|SpiderB] Still whenever I start to get worried I look at Cohen's audacious smile.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Cohen|Middle+body+Body|QUADINOUT][MOVECHAR|Frederick|Middle+Body|QUADINOUT][MOVECHAR|Alonso|Middle-Body-Body|QUADINOUT][MOVECHAR|Ron|Middle|QUADINOUT][MOVECHAR|Irene|Middle-Body|QUADINOUT] Finally things come to head. Two patrols try to perform a pincer movement on our group.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|SpiderA|-1|Neutral|LEdge][ADDCHARPOS|SpiderB|-1|Neutral|REdge] Ron redies his magic with a manic grin. Everyone else isn't quite as enthusiastic.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone fights their very best. Cohen's sword flashes and I see spider girls flee with their spears cut in half. The long way.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I see spider-girls reading blow darts. They are aiming for Ron![BLOCK][CLEAR]") ]])
  --maybe a reference to a variable set earlier, if you fougth over his body in the first part.
  --(if: $conanhelp is 0)[(You have enough problems without having to carry his ass.)](if: $conanhelp is 1)[The last time you tried to hold off impossible odds above his unconcious body it didn't work out.]
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[RemChar|Ron] I push Ron to the ground and the darts go wide. The fireball Ron was conjuring goes wide and up through the trees above. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Oh shit! What was that for?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Darts Ron! keep an eye out and get back to casting.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron:[ADDCHARPOS|Ron|-1|Neutral|Middle] I'll try. They won't get another shot after that.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [Remchar|SpiderA][Remchar|SpiderB] As the attack falters and is eventually beaten back I see a growing cloud of smoke above the horizon.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We manage to lose the spider-girl patrols for a moment and everyone rests.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen however is busy scanning the horizon. He looks uncertain.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Cohen: Youngling climb a tree and see what's making the smoke. I'd do it, but you seem more suited for the job. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I actually am, I climbed a tree earlier today and let me tell you. These fingers are great for it.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Cohen: Great climbers ants. Now hurry and make sure you don't get stuck in anything up there. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[RemChar|Ron][RemChar|Cohen][RemChar|Frederick][RemChar|Alonso] [MOVECHAR|Irene|Middle|QUADINOUT] I'm pretty tired and close to exhaustion but I make the go of it I can. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Thankfully the tree is easy to climb and the bark isn't too wet. It's also on an outcrop to look out above it's neighbors.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long I'm far up in the trees, a world of green surrounding me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I climb as far as I dare, finally reaching blue skies and a relatively good view of the trees surrounding.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Far in the distance there is a growing tower of smoke and fire. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Surprised]  Fuck.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: They hear my cursing before I reach the ground.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[ADDCHARPOS|Ron|-1|Neutral|Middle-Body][ADDCHARPOS|Alosno|-1|Neutral|Middle-Body-Body][ADDCHARPOS|Frederick|-1|Neutral|Middle+Body][ADDCHARPOS|Cohen|-1|Neutral|Middle+Body+body] There is a gigantic forest fire.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: It wasn't necessarily me this time.[BLOCK][CLEAR]") ]])
  -- everyone turns to look at Ron here.
  fnInstruction([[ WD_SetProperty("Append", "Everyone:...[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Well okay, it might of been me. I got pushed and cast a fireball on accident. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Irene|Offended] I didn't put enough magic behind it to cause a forest fire. So it's not really my fault.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: You get big fires at burial grounds sometimes. Maybe he hit one of those?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk]  I used to shovel shit everyday. Ron didn't hit a burial ground, he hit a dumping ground.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Decaying shit goes up like nothing else.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: If it's spreading we are in trouble. You can't outrun forest fires. I don't mind a funeral pyre but now's not the time.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral]  What should we do Cohen?[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Cohen: Nothing to do but run Youngling. We need either an exposed place far from nearby trees or a body of water to try and ride out the flames.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: I don't know where we could find either. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I have a bad feeling.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Buck up, we made it this far.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We make the best speed we can through the forest. We shove ourselves through the underbrush ignoring brambles and branches.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We try to head down and away from the fire, but everyone was already exhuasted.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Ominously the spiders have stopped chasing us. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: As we sprint down a hill Cohen suddenly has an idea.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Cohen: Youngling ants have a great sense of smell, can you smell water anywhere near by?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] I'm not sure I could Cohen. what if I lead us in the wrong direction?[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] You can do it. Just take a deep breath and give it a try. Can't be worse than what we are doing already.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alright just need to do this right or I'll kill everyone.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Deep breaths, deep breaths.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Surprised] (Hell)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I twitch my antenna and the smells around me come into sharper relief[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The smoke is making them all seem muted and wave in the air.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: It doesn't help that I don't know what to look for.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I have an idea. I bring one of the nearly empty water skins that Cohen brough infront of my antenna. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk]  A light blue tinged with memories of thirst wafts up. I scan desperately for anything similar. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: At first there is nothing. Nothing but smoke and distant death. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a fleeting twinkle of blue. It's like a start on a cloudy night though and gone in an instant.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] That way.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] Maybe.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Neutral] Well no sense in waiting![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Good job.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] It might be the wrong way.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] Good job anyway.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We proceed with the same frantic speed as before, the fire growing larger behind us all the time.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle-Body|QUADINOUT][MOVECHAR|Ron|Middle|QUADINOUT] After a while my breath starts to grow short. I try and keep my breath slow and steady, slowing as much as I dare.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: it doesn't help.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Surprised][MOVECHAR|Irene|Middle-Body-Body|QUADINOUT][MOVECHAR|Alonso|Middle-Body|QUADINOUT] I fall to the back of the group, gasping as I try to keep up. It's like my chest has been filled with cloth. [BLOCK][CLEAR]") ]])
  --go to kneeling.
  fnInstruction([[ WD_SetProperty("Append", "Irene: cough, hkkht[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk]  Keep going I'll be alright. Just need to catch my breath.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Surprised] What's wrong with them Cohen?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: I was worried this might happen. Ants do a lot of breathing through their skin. The smoke gets to them before it gets to us.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Oh no.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: What like frogs?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: They don't need to keep their skin wet Frederick. Alonso carry him. If you get tired we will switch off.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'm fine.[BLOCK][CLEAR]") ]])
  --stand then kneel here.
  fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] Heh, you rescued everybody so now we get to rescue you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Surprised] Alonso heaves me onto his back with little difficulty.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Even as a woman he has broad shoulders.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Good thing you got lighter or this might be difficult.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Heh, that's not funny.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Surprised] Got a heading?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Forward.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The fire draws closer and closer. Thankfully the blue smell grows stronger and stronger.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Eventually we head up a rocky ridge.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick sprints ahead, watching the ground carefully.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: A river! About 30 paces below![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Bah, where has the courage of young men gone? As long as you don't mess up the jump it will be fine.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: And if we do miss we will bash ourselves to pieces on the rocks.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Frederick, unless you suddenly became fireproof that's the only option.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My head hurts and I can barely keep my eyes open.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Anything is better than this.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk]  Someone throw me down the cliff already. I don't have all day to suffocate.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Surprised] I can barely see Frederick eyeing Ron's trembling figure.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: I'll do it. But if I'm dragging them out of the water I'll need Ron's help.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Yep I can do that. I'm not afraid, definitely not terrified. Oh goodness me not afraid at all.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso brings me to the edge of the the cliff.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The drop is down a straight cliff. Despite ths the river at the bottom looks peaceful and deep.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: As calm as a grave.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Let's do this quick. A river that smooth is probably running pretty fast. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Frederick, Ron. As soon as our ant friend hits the water you need to recover them. He probably won't be able to swim as breathless as they are.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Keep their head above water. They might try to pull you down but keep their head above water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We all stand near the edge. Ron is obviously terrified. He keeps looking at the water below and then back at the fire. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally he looks at me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: I can do this.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick takes two deep breaths, tries not to look at the edge and then grabs Ron.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: They step over the edge together. [RemChar|Frederick][Remchar|Ron] Distantly I can hear Frederick yell with excitement as Ron screams.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso gives me a desperate smile. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: I'll be right behind you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [RemChar|Cohen][Remchar|Alonso] I step weakly over the edge plummeting straight down. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Somehow this fall seems faster than the one in the darkness. Perhaps it's the water rushing up to meet me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Frederick|Middle|Linear] In the curiously fast slow time I see Frederick and Ron desperately swimming against the current trying to stay near where I'll land.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|Null] I pierce the water like a rock.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I sink and I sink, the wet void around me a welcome relief from the hell above.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (All this just to drown.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I start to float like a cork to the top.[BLOCK][CLEAR]") ]]) 
  --[BACKGROUND|ForestC]
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestB][ADDCHARPOS|Frederick|-1|Neutral|Middle+Body][ADDCHARPOS|Ron|-1|Neutral|Middle-Body] Frederick is there with Ron working to keep my head above water. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Both have their hair plastered to their smiling faces.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: You mad bastard, you float![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Hahah![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Would you imagine that. I guess ants do float a bit.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Any further conversation is cut off by the impressive splashes made by Cohen and Alonso.[ADDCHARPOS|Alonso|-1|Neutral|Middle+Body+Body][ADDCHARPOS|Cohen|-1|Neutral|Middle-Body-Body][BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I switch to floating on my back. Everyone grabs one of my hands. It's a bit weird to be able to hold so many hands once.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: What now Cohen? We swim out of here?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Swim? Youngling just how hard do you expect an old man like me to work? We will float out of here.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Spiders, if they have sense in their heads will have fled, and I'd say a jump like that would make a fine ending for any tales we tell about this.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: There is little to do but float, it's a nice feeling to finally relax.[BLOCK][CLEAR]") ]])
  --[BACKGROUND|ForestF]
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] Finally we reach a sandy beach with a town nearby.[BLOCK][CLEAR]") ]])
  --Jump to 340 1 escape finish fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Happy] Well I don't think I'm wrong when I say that was a skin of the teeth adventure.[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/340 1 escape finish.lua", "Ending")
  end
  
  end
  