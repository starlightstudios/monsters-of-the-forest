--Short in between passage that link to the inital TF choices, adapts the option 1 and option 2 choice from twine game
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
        WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] With my mind focused I look over my surroundings again.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought: [VOICE|Isaac] (Assuming that I was captured by that unfortunate spider-girl, she was probably taking me back to her nest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Brook flowing downhill will grow larger, assuming that it keeps going it likely joins a river. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Rivers lead to civilization. Civilization means safety.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Looking up I can see the occasional broken branch and a few spots reinforced with spider silk. These spots lead uphill.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Probably as close to a spider trail as it gets.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Assuming the spider-girl was heading home, that should lead to the nest where everybody is being held captive. Although, that's if it's all the same group of spiders.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Plans this desperate rarely have all the parts figured out.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral](Guess I'll have to figure out the rescue when I get there)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The options are simple, I could try a single handed suicidal rescue or flee to safety.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If I had everyone and the right equipment, I'm confident we could pull this off. Instead it's me barehanded against a full nest of spiders. But it might be the only shot I get.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Alternatively I might abandon them. Throwing away my freedom and possibly my life will accomplish nothing.)[BLOCK][CLEAR]") ]]) 
--SD: Remember deeper into the forest is right to left.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Almost on instinct I move up the brook[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk](If I survive this it's going to make one hell of a story to tell around the campfire.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Everybody else would do the same thing.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Well maybe not Frederick. He would gear up first I think. He needs weapons to be effective.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I move slowly but purposefully, trying to maintain awareness of my surroundings and follow the trail.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The hill isn't that steep but I can feel my energy fade quickly. Today just doesn't stop.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended] (Hell I'm hungry. I can't even tell how long ago lunch was.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] A quick scan shows a patch of white mushrooms, and a conveniently low to the ground beehive.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Just about the last thing I want to do right now is run from bees. The mushrooms should be safe.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: This is management please asking readers to not eat mushrooms they are not confident are safe.[BLOCK][CLEAR]") ]])
  
  fnCutsceneBlocker()
  LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/220 Ant 1 TF.lua", "Begin Scene")
  end