--Tf passage, adapting the mushroom or bee hive choice. leads in to the first 1/0 ant tf choice
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    --inital ant tf 
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Just to be safe, is start by smearing the mushroom on my lips. After a few minutes of no reaction, I cautiously sample just a pinch of one.[BLOCK][CLEAR]") ]])	 
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Welp, I can't wait much longer. No telling how much longer the daylight will hold.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (Hell, is it the same day? This forest is so gloomy that I can't see the sun enough to tell time.)[BLOCK][CLEAR]") ]])
   if(sGender == "Girl") then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (I know Ron wouldn't wait.)[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Having paid the minimum courtesy to safety I proceeded to wolf down the rest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Burrrp.[BLOCK][CLEAR]") ]]) 
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Well that wasn't very ladylike.)[BLOCK][CLEAR]") ]])
  if(sGender == "Boy") then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I should be okay, I can keep most things down.)[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Still I feel better.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I get ready to resume my march when my foot starts to tingle.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a practiced ease I slip off my boots. There is a small blackish brown spot on my sole. [BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Morality non-withstanding.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought: [EMOTION|Irene|Angry][VOICE|Isaac] (Hell)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I sit and get ready to face whatever is coming.[BLOCK][CLEAR]") ]])	

--End result:


--kneel here or got to black background? Probably black background
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene] With due caution I gently poke the spot. The texture is far rougher than skin. As if in response to the poke, it quickly begins to grow.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Strangely as the spot grows I feel contented. I should probably be worried but I'm not. On the whole it feels like a ticklish massage. Before long it has spread across my toes and past my ankle. My foot seems smaller for the change.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The hair on my leg drops off as the brown advances. The sensation as it crosses my knee is unbearably itchy. As the tickling spreads up my thigh I feel suddenly very self-conscious.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:The growth finally stops at mid-thigh.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (This new stuff is hard and rough, kind of like a beetle-shell)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I try not to panic as a similar spot develops on the other leg. Before long this performance is repeated on the leg. Leaving me with a pair of petite feet.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A glance at my hand shows that one of my fingers has developed the same black marking.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Fuck, deep breaths. It hasn't hurt me so far.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The brown tide subsumes my fingernails quickly, progressing before my eyes. A few gestures tell me that I still have a sense of touch. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:The chitin continues to roll up my arm, the sensation is again more like a comforting touch than anything truly alien.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As it reaches my elbow, there is a intense tickling sensation and the joint reverses before my eyes. Tapering in as opposed to out. The feeling is rightfully indescribable, but true panic is kept at bay by the same comforting sensation. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It stops short of my shoulder. However with squeezing sensation my shoulder grows more slender.[BLOCK][CLEAR]") ]]) 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As the process begins to happen again on my other arm, I feel a ticklish sensation above my tailbone[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Uh-oh)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: After a bit of contorting I lay eyes on a black developing spot above my tail bone. The spot enlarges slowly, gradually encasing your butt.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With the feeling of being strongly gripped, my butt reduces size slightly and my waist grows thinner.[BLOCK][CLEAR]") ]]) 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: From all of this an insectoid abdomen gently pushes out. A hard layer of chitin covers the top with a more supple layer on the bottom. It's tender.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As the abdomen finally finishes growing, there is a light tingle on my face. A quick glance in the mirror shows that my face has grown a little cuter.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I wait a few more minutes for anything else to change and then stand unsteadily.[BLOCK][CLEAR]") ]])
  --stop kneeling or black screen here
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [ADDCHARPOS|Irene|-1|Neutral|Middle] (Whoa, this new abdomen throws my balance off something wicked. Still my arms and legs feel a bit stronger than before.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A glance at the surrounding trees confirms my worst fears, I pull out my pocket mirror just to be sure.[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The guys are going to give me hell for being short.)[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Aw hell, I'm shorter. I'm gonna have to work extra hard to be intimidating if I'm this short. Especially with this face.)[BLOCK][CLEAR]") ]])
end
      --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Today has just been a real trip. Still could of been a lot worse. I can move and fight.)[BLOCK][CLEAR]") ]])
--remember deeper into the forest is right to left
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I slip back on my now redundant boots and head up the brook.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (My doom won't find itself.)[BLOCK][CLEAR]") ]])
	--fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] After a bit of a struggle I manage to navigate past a gigantic fallen tree damming the brook creating a swamp on the other side. The tree's diameter is easily twice my size.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] As I walk I find my path blocked by a gigantic fallen tree damming the brook. The tree's diameter is easily twice my size. It must of raised an unholy racket when it fell.[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]The only way around involves shredding my pants on the bushes.[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a hint of a breeze at the brook, and it's only going to be muggy and still past the tree.[BLOCK][CLEAR]") ]])
   if(sGender == "Boy") then
     fnInstruction([[ WD_SetProperty("Append", "Narrator: I reach for my cantenn only to realize that it's not there. Better fill up while I can.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Narrator: I take a long greedy drink from the brook. If it's gonna disagree with me later, I'll deal with it then. I'm no good to my friends tired, hungry and dehydrated.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Narrator: The water is blissfully cool and I feel refreshed for a moment. Time to fight my way through the underbrush.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] I take a moment to relax, slipping my feet into the brook. The cool water feels nice on my newly hard toes.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:It's only going to get harder from here. But I can do it. The others need me after all.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator:Then I slip on my boots, ready to fight my way through the underbrush.[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Narrator: My new anatomy provides some protection from the brambles. But my pants are helpless in the face of the onslaught. On the otherside of the tree, there is a small swamp made by the blocked water.[BLOCK][CLEAR]") ]])
   	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (RIP my noble pants, you will be mourned.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Finally the spider's trail takes a sudden turn and arcs off into the forest. The forest is dense compared to the relatively clear area around the brook.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It was easy going this far, but in there it's going to be a real challenge to follow the trail and not run into anything.)[BLOCK][CLEAR]") ]])
    --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Or anyone.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (I'm gonna have to rely on something to get me through this, what's it gonna be?)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Decision")
    --Decisions. 1/0 ant tf choice, anatomy is the +1, skills is the +0
elseif(sTopicString == "Decision") then
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
     fnInstruction([[ WD_SetProperty("Append", "Narrator: Should I rely on my new limbs or my skills as an adventurer?[BLOCK]") ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"My new arms and legs\", " .. sDecisionScript .. ", \"Anatomy\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"My tracking skills\",  " .. sDecisionScript .. ", \"Skills\") ")

	--My tracking skills
	--My new arms and legs

elseif(sTopicString == "Anatomy") then
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Think, what's the best way to do this?)[BLOCK][CLEAR]") ]])
   fnCutsceneBlocker()
	LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/230a ant 1_1.lua", "Begin Scene")
  
  
  elseif(sTopicString == "Skills") then
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Think, what's the best way to do this?)[BLOCK][CLEAR]") ]])
   fnCutsceneBlocker()
	LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/230 ant 1+0.lua", "Begin Scene")
	end
