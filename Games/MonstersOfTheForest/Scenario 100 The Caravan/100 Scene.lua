--[Scene]
--This constitutes the scene for now. The scene splits when speaking to the adventurers, but otherwise loops back to this script.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iAntMeal = VM_GetVar("Root/Variables/Characters/Scenario/iAntMeal", "N")
    local iYouFight = VM_GetVar("Root/Variables/Characters/Scenario/iYouFight", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    
  fnInstruction([[ WD_SetProperty("Append", "Narrator:We run like all hell through the nest. Ron clears a path with fire magic. Forma and I run interference to keep the spider-girls from surrounding us.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-girls bemused surprise quickly changes to furious anger when they realize what we are doing.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: They do everything they can to stop our escape but desperation gives us wings and our team work is solid.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally we breach the nest and reach the clearing surrounding it. That's when things get complicated. [BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Across the clearing I see the lone ant-girl patrol. It's led by the ant-girl wearing the top-knot. They are completely surrounded by the spider-girls.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls are trading steely glares with the spiders as they circle. Still they are in trouble, they won't get out without help.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Across the clearing I sight the junior patrol of ants. They are surrounded and scents of desperation are coming off of them.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Fuck! They must have overcommitted. Still they aren't getting out of that without help.)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Indecision is streaming off of Forma, she wants to get to safety but can't stand the thought of abandoning them.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: We have to get them out of there Alonso.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Deep breath. Okay, we don't have many weapons so you two are the wedge. Ron! Keep fire on the tree line so they don't surround us as well. Frederick! W are going to run interference to keep the gap open.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick looks terrified for a second but nods. Forma gives me a grateful smile, she puts out a dark brown scent. My attempt to understand it's meaning is cut off by Ron starting casting.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron puts out an impressive amount of fireworks. These light up the murky clearing and for a second it looks like a lighting strike is coinciding with our attack. It's an awesome sight.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-girls form a double line shouting battle cries as they do so. No doubt they are contemptuous at being forced back so close to home.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Form and I fight with one mind, the desperation giving us an improved coordination.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: It's slow hard fighting but we finally force our way to the ant-girl patrol.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The patrol realizes what's happening and tries to fight to reach us.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: When we reach them I see that the ant-girl with the top knot has been wounded, but she still fights in the defense.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She looks at me with an expression of annoyance, not relief. I can't help but feel that I have made a serious mistake.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I look back to see what she's looking at.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The scents of fear and panic coming from the patrol are almost blinding, but forma does her best to put out a scent of calm to help me focus through them.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: When we reach them I see that the younger ant-girl has been wounded, her sisters fighting around her desperately to protect her.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She smiles at me, but then her look changes to one of horror. A dark purple cloud of despair starts up around me.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I look back to see what has everyone so spooked.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Coming around the bend of the clearing is a massive charge of spider-girls. The greater part of the nest is about to charge us. I cannot help but feel a bit like a bug about to squished underfoot.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The sitution is bad. We are still bogged down in the fighting. The injuried ant-girl is going to have to be carried.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma looks at me uncertainly, not sure what to do next.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Fuck. Fucking Fuck.[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](Well he's out of ideas.)[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Out of the frying pan and into the fire.)[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The enemy charge rolls forward and we get ready for a real bad time. Then I see scents rushing up from behind me. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I turn and see the rookie patrol attcking and trying to cover to escape.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: Ron buy us time, stop the enemy charge![BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I toss Alonso my entrenching tool and put the wounded ant-girl on my back. Alonso and Forma lead a charge through the retreating spider-girls as I carry the top-knoted ant-girl on my back.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The enemy charge loses heart about halfway through. I only recognize what's happening as I see smug scents streaming through the trees.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The veteran patrol has made it's presence known, obviously happy about ambushing the spiders for once.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I toss Alonso my entrenching tool and put the wounded junior ant-girl on my back. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-girls try to block our escape but Alonso shouts for Ron to shift his fire and they reconsider.[BLOCK][CLEAR]") ]])
    end
    --if antmeal is 1
    if(iAntMeal == 1.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Briefly I consider trying to make a push for the nearby ant-girl outpost. The ant-girls wouldn't make a tunnle that big and that long if it didn't go somewhere important. Still it might lead to more ant-girls and I don't know if that's better.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: What follows is a desperate escape. The spider-girls have taken personal offence to my jail break and they pursue us without rest. [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl patrols had the foresight to bring spare weapons and food.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: we barely have time to stop and distribute weapons and food. Not nearly enough time for Ron to try and heal the ant-girl.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: My friends take the rations gingerly, and then look at me with varying levels of concern.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Not bad stuff at all![BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: Is there no other food?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Not the worst thing we have ever had to eat.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Still for whatever reason it leaves them unchanged. Perhaps the simple fact that stopping here would doom us all prevents the changes.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: However in between the ant-girl reinforcements and our courage this has become as fair as this sort of battle gets.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: My teammates giving good accounts of themselves. Alonso proves a capable leader at coordinating with the ant-girls. Frederick manages to escapes more than a few ambushes with simple cleveriness. Ron doesn't toast anyone.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I spend most of my time helping to transport the wounded ant-girl.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Still it's a constant battle, after a full day of running combat I try and question Forma on the plan.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: As always Forma's answer is silent. She does unleash a modest smile and a dazzling array of scents though.[BLOCK][CLEAR]") ]])
    --if antmeal is 1
    if(iAntMeal == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Something about what she tells me seems familar though.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The spiders for their part have no end of plans to get us with. They try to flank, to distract, to drive us into traps, to simply back off and shoot arrows at us.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Their most successful attempt is when they almost split the two patrols.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Me and my friends hold fast though, stopping the spider-girl's charge in their tracks.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma and I quickly figure out what's going on a rally the ant-girls to stop their charge.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: On the whole I work well with the ant-girls even if I don't have a good handle on what they are saying. They follow a peculiar consistent logic that makes it easy to guess what they are tryign to do.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma however is surprisingly good at intuiting what I'm trying to tell her. If only I could understand her.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: By the end of the second day even the ant-girls are tired. Taking another step is difficult.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: My and my teammates fall to our kness which some scarcly better off ant-girls taking watch. They seem to know how to pace their bodies better than me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Then there is a rustling from up ahead. I try to raise my weapon but stop. The scents seem familar.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen steps through the trees looking a bit surprised.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone, well everyone who knows Cohen is too stunned for words. We all collapse in exhaustion.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Hah! Knew I'd find you younglings eventually.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: We all give him a group hug as the ant-girls look on in bemusement.[BLOCK][CLEAR]") ]])
    
    fnInstruction([[ WD_SetProperty("Append", "Cohen: It's good to see you younglings again. Impressive that you escaped from the spiders by yourself. Is this everyone?[BLOCK][CLEAR]") ]])
    
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Hah I wish we broke out on our own! It was this mad, bad and dangerous to know bastard.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: They got turned into an ant-girl so they could get help. Lucky bastard.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: Then with the ant-girls they busted into the spider's nest. It was very impressive.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen blinks twice and then takes a close look at my clothes.[BLOCK][CLEAR]") ]])
    else
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen has a confused expression, looks to everyone else and then looks at me. Finally he seems to recall something and examines my flower crown.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Now that'll be a tale to tell youngling. Perhaps it'll make the poetic edda.[BLOCK][CLEAR]") ]])
    if(iAntIntimacy == 1.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: He notices that Forma is holding one of my smaller hands and gives me a wink.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Irene: So how did you get away Cohen? That ambush damn near got all of us.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: What you think an ambush of spider-girls could stop me?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone looks at him skeptically.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Oh alright. They forced me back. Still once I'd given them the slip I went back to town for gear and supplies. Then I went looking for you younglings.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: But how did you find us? It's a big forest.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Started out tracking the spiders, that wasn't hard. As I went along I ran into a patrol of bee-girls. They said the spiders were up in arms because of a balls-out proving raid by ants on the spiders nest.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: I figured that if there was such a raid then you younglings would take that as a chance to escape. Then the fires started.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: Some of those I was really proud of.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: So you know the way out of here? The spiders have been bugging us without end.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Alonso![BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Oops, dogging us without end.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: You did well getting this far, and it isn't much further. Let's form up and cut our way through.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma accepts Cohen's leadership only reluctantly. To my personal surprise they can actually communicate with one another. Cohen knows a form of sign language that Forma responds to.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: However after the first few times he leads an ambush on an ambush she accepts him wholeheartedly.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen claims it's been a long time since he led a warparty this large. He still knows what he's doing.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Although with this many people absolute stealth goes out of the window. [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Still with Cohen's guideance we are able to outrun or simply out manuever our pursurers.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: This finally gives us the time for Ron to heal the wounded ant-girl. She seems undeniably grateful.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally the trees start to thin and I can almost see clear skies. Fittingly this is when the final largest attack comes.[BLOCK][CLEAR]") ]])
    if(iYouFight == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-queen leads this final attack. Her eyes narrow when sights me.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-queen leads this final attack.[BLOCK][CLEAR]") ]])
    end
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I'm exhausted. Between days of fighting, carrying the ant-girl and precious little to eat it's a miracle I can take another step.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I'm stronger than I used to be but my strength is at it's limit. Between days of fighting, carrying the wounded and trying to lead the ant-girls; it's a miracle I can take another step.[BLOCK][CLEAR]") ]])
    end    
   
    fnInstruction([[ WD_SetProperty("Append", "Irene: I'm at the end of my strength Alonso.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm no better. Almost out of here. Cohen you got this?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Hah! Just clear me a path to the queen.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I nudge forma. She turns to me with a smile.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: No words can describe this final desperate charge. The fleeting moments of safety and danger, the noises as the spider-girls scream and your teammates scream right back at them, The indescribable scents and feelings as the ant-girls try to keep communication.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a great crunch as the lines meet. Forma and I stick together. Moving like a pair of dancers before a very angry audience.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: (albeit fairly clumsy dancers, who tend to bump into one another.)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: In between blows I catch a glimps of Cohen fighting the queen. Their blades are meeting but Cohen is smiling.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally, the spider-queen lets out a curse so foul it could make water boil. The spider-girls begin to retreat and I can see that spider queen's spear is in two parts. [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Her guard curses and flips off everyone as they retreat. (some people can be so petty.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Distantly I can see the edge of the forest. However every step is an effort. [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](I feel like after harvest time, when I worked from pre-dawn to when it was too dark to see.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Forma seems anxious, she's trying to get the ant-girls to march in order for some reason.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Oh here's cohen.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen slows his pace to walk next to me. He pats me gently on the back.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Cohen: The ant-girl with the flower crown.[BLOCK][CLEAR]") ]])
      if(iAntIntimacy == 0.0) then
        fnInstruction([[ WD_SetProperty("Append", "Cohen: The one that seems nervous as we get out of here.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Cohen: To one who keeps making eyes at you.[BLOCK][CLEAR]") ]])
      end
    else
      fnInstruction([[ WD_SetProperty("Append", "Cohen: The ant-girl that's been your body guard.[BLOCK][CLEAR]") ]])
      if(iAntIntimacy == 0.0) then
        fnInstruction([[ WD_SetProperty("Append", "Cohen: The one that seems nervous as we get out of here.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Cohen: To one who keeps making eyes at you.[BLOCK][CLEAR]") ]])
      end
    end
    fnInstruction([[ WD_SetProperty("Append", "Irene: I've taken to calling her Forma, if that helps.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Forma. She's going to make you an offer. You are young and it's a once in a life time sort of thing. We will all support you no matter what you choose.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( What sort of offer I wonder.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally we reach the edge of the forest. Did the sky always seem so blue. the ant-girls seem unwilling to step out beyond the cover of the trees.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma makes a number of arcane hand symbols to Cohen. He winks at her in response and then she drags me behind a tree.[BLOCK][CLEAR]") ]])
    if(iAntIntimacy == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She starts by kissing me on the forehead.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She starts by kissing me gently.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she stares into my eyes. She drops to her kness and begins to kiss each of my hands while smiling at me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The she points at me and then points deeper into the forest.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Uhhh, what?)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma sighs inaudibly.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: She points at me, makes a blue scent and then points at herself making a brown scent. Then she holds up two fingers, then points into the forest making a blue and a brown scent.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Oh she wants me to go with her!)[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( She wants me to go with her!)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Than's a hell of an offer. What Cohen was talking about now makes perfect sense.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( It'd be a break with everything I've ever known. Completely dependent on Forma amongst a strange people who I can't understand well. )[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( I still can't really talk to ant-girls.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( There's something appealing about the balls out audacity of it. )[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Still it's very easy to leave home until you have to camp in the rain.)[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( It'd also mean leaving everyone one. Will they be alright with out me?)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Also I'd better be comfortable with being a girl and an ant. No gettin back to maleness if I go with her.)[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( It'd also mean leaving everyone one. I wouldn't get to see the world beyond this forest.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Also I'd better be comfortable with being an ant. No getting back to human if I go with her.)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Still Forma has risked her lives the lives of her sisters. She wouldn't do all that if she wasn't planning to help me adjust.)[BLOCK][CLEAR]") ]])
    if(iAntIntimacy == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( It's a bit like marriage I suppose. At the very least the scents suggest that Forma would take it that way.)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](I'll shake or nod my head, hard to be clear otherwise.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
       local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
       fnInstruction(" WD_SetProperty(\"Add Decision\", \"Nod\", " .. sDecisionScript .. ", \"Yes\") ")
  if(iAntMeal == 0.0) then
        fnInstruction(" WD_SetProperty(\"Add Decision\", \"Shake\",  " .. sDecisionScript .. ", \"RegularNo\") ")
  else
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Shake\",  " .. sDecisionScript .. ", \"AntmealNo\") ")
  end
      fnCutsceneBlocker()
  
    --[=[choice node
    if antmeal is 0
      shake my head no-> regualar no
    else
      shake my head no-> antmeal no
    end
    nod
    ]=]
    elseif(sTopicString == "AntmealNo") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iAntMeal = VM_GetVar("Root/Variables/Characters/Scenario/iAntMeal", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I start to shake my head no. On to find myself stopping for reasons I don't quite understand.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Would it be so bad? To be among sisters who understand me better than words. To be with Forma who is dedicated to me.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( To live a life completely different from anything I have ever known.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( It's seeming like a good idea, but it's been a hard day. Deep breath. This is an important decision. Gotta think it through carefully.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Nod\", " .. sDecisionScript .. ", \"Yes\") ")
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Shake\",  " .. sDecisionScript .. ", \"RegularNo\") ")
fnCutsceneBlocker()
--[=[ decision node
nod-> yes
shake my head-> regular no
]=]

elseif(sTopicString == "RegularNo") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iAntMeal = VM_GetVar("Root/Variables/Characters/Scenario/iAntMeal", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
--regular no
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](I've gotten along well with the ants. Going on yet another adventure has it's own appeal.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](Forma would love it for me to do so. The ant-girls would become my sisters.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](But in the end I would miss my brothers-in-arms too much.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I sahake my head, at first hestitantly and then confidently.[BLOCK][CLEAR]") ]])
  if(iAntMeal == 0.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma looks at me hesitantly.[BLOCK][CLEAR]") ]])
  else
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma looks at me surprised.[BLOCK][CLEAR]") ]])
  end
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she smiles sadly and nods. There is a resigned blue scent coming off of her defeatedly. [BLOCK][CLEAR]") ]])
  --if antmeal is 1
  if(iAntMeal == 1.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Beneath it though, there is a hint of admiration.[BLOCK][CLEAR]") ]])
  end
  if(iAntIntimacy == 0.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma kneels and kisses my hands again. I look at her questioningly.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: She then stands and gives me a deep bow. Finally she carefully maneuvers her antenna into mine.[BLOCK][CLEAR]") ]])
  else
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The she surprises me with a kiss. It's deep and appreciative. Perhaps not the most romantic kiss but she seems to appreciate it.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally she breaks it off and looks into my eyes longingly. The she gives me a deep bow.[BLOCK][CLEAR]") ]])
  end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: With this as my parting gift she leads me back to my friends.[BLOCK][CLEAR]") ]])
    if(iAntMeal == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls, silent as always wish everyone a fond farewell. The top-knoted ant-girl nods to me respectfully.[BLOCK][CLEAR]") ]])
    else
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls, silent as always wish everyone a fond farewell. The younger ant-girl in particular kisses my hand and kneels before leaving.[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I start walking with my friends with a spring in my step. I choose to be with them after all.[BLOCK][CLEAR]") ]])
       fnCutsceneBlocker()
          LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/340 1 escape finish.lua", "Ending")
    --this goes to reuni in 340 1 escape finish.lua at the ending. Need to set up a variable to catch the differences there
    
    elseif(sTopicString == "Yes") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    local iAntMeal = VM_GetVar("Root/Variables/Characters/Scenario/iAntMeal", "N")
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](it seems like a mad idea at first. But the more I think on it the more it has an audacious appeal. It's a once in a lifetime adventure, and hell that's what got me here.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( The ant-girls are cuties and they've done so much to help so far. I can think of few groups more willing to draw in my defense.)[BLOCK][CLEAR]") ]])
if(sGender == "Boy") then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( I'm gonna miss the hell out of the others though. I hope they can stay safe without me.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( The ant-girls need help though, the spiders can't be allowed to terrorize them.)[BLOCK][CLEAR]") ]])
else
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( I'm gonna miss the hell out of the others though. We were going to see the world together.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( Still there can be an adventure beyond compare in this forest. I've barely seen any of it. It's wonders or it's horrors.)[BLOCK][CLEAR]") ]])
end
fnInstruction([[ WD_SetProperty("Append", "Narrator: I nod smiling at Forma. [BLOCK][CLEAR]") ]])
 if(iAntIntimacy == 0.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma becomes wildly excited for a moment. Then she hugs and spins me using all four of her arms. She looks embarassed as she sets me down and then regains her composure.[BLOCK][CLEAR]") ]])
  else
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma nods happily and looks for a second fit to kiss you.[BLOCK][CLEAR]") ]])
  end
  fnInstruction([[ WD_SetProperty("Append", "Narrator: With a deep curtsy she kisses your hand, looking up at you. She's smillng like a kid who knows a secret.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Then with a regal bearing she adopts a position behind you, both sets of hands folded over one another.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I try to smile as I go to tell the others of my decision.[BLOCK][CLEAR]") ]])
  
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I fail.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I return to my friends. The ant-girls are politely watching frederick demonstrate his knife throwing skills. He's not bleeding so he's doing pretty good. The ant-girls are unimpressed though.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron and Alonso are comparing notes on bust size and ways to keep their hair out of their face. Alonso's already tied Ron's hair back and Ron is trying to do the same for Alonso. He's tying it like you might the mane of a horse.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen gives me a look. I nod slight for him.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Alright younglings line up. The floor is yours.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: My friend here made me an offer. I'm going to adventure with the ant-girls for a while.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: What! But it won't be the same without you! Will you be alright by yourself?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Are you sure it's your own decision? I can limit my fire honestly.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: I am going to miss the hell out of you. We were going to get up to so much much trouble together.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'm alright honestly, and I'll do whatever you want to prove it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: They subject me to a battery of tests and questions trying to make sure that I am in my right mind. But I am.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: We spend a bit of time talking about the escape, complimenting me on what I did.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally it comes time for farewells.[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  
  elseif(sTopicString == "Farewell") then
--Variables.
local iVarA = VM_GetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N")
local iVarB = VM_GetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N")
local iVarC = VM_GetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N")
local iVarD = VM_GetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally it comes time for farewells.[BLOCK][CLEAR]") ]])
if(iVarA == 1.0 and iVarB == 1.0 and iVarC == 1.0 and iVarD == 1.0) then
    LM_ExecuteScript(LM_GetCallStack(0), "Leaving")
    return
    end


--short get psychied up by talking to friends sequence
  fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  	    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    if(iFrederickAdvice == 0.0) then
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Bid farewell to frederick\", " .. sDecisionScript .. ", \"Frederick\") ")
    end
  if(iAlonsoAdvice == 0.0) then
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"bid farewell to Alonso\",  " .. sDecisionScript .. ", \"Alonso\") ")
  end
  if(iCohenAdvice == 0.0) then
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"bid farewell to Cohen\",  " .. sDecisionScript .. ", \"Cohen\") ")
  end
  if(iRonAdvice == 0.0) then
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"bid farewell to Ron\",  " .. sDecisionScript .. ", \"Ron\") ")
  end
io.write("Dogs are great!")
  
  --frederick farewell:
elseif(sTopicString == "Frederick") then
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick shakes my hand, his hands even smaller now. Then he hugs me. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: You have been a brother/sister and a friend to me. We will meet again, if not in this life then in the next.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: He smiles a bit sheepishly.[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  	    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Tell him to stop being so dramatic\", " .. sDecisionScript .. ", \"Frederick1\") ")
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Agree\", " .. sDecisionScript .. ", \"Frederick2\") ")
  
elseif(sTopicString == "Frederick1") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I push him playfully.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Quit being so histronic. We will meet again, I know it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: When you do you have to tell me how Ant-girls are in the sack. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Really?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Look I know you are going to get a lot in there. Look at how that ant-girl pratically hangs off of you. And if it's not recorded for future generations that is a great disservice.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I try to roll my eyes, and then realize it won't work.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I stick my tongue out at him instead.[BLOCK][CLEAR]") ]])
       fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  
  --Agree
  elseif(sTopicString == "Frederick2") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: In this world or the next Frederick we will meet again.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: I can be happy about that at least. Don't do anything I wouldn't. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Heh.[BLOCK][CLEAR]") ]])
       fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  --Alonso farewell
elseif(sTopicString == "Alonso") then
    fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]( I can tell that he's trying to smile.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: He shakes my hand with both of his, so I return the gesture with all four of mine.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: It's been a pleasure and I'm going to miss the hell out of you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Frederick said as much.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Well I mean it just sincerely. Are you sure you will be safe with the ants?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I can manage Alonso.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  	    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Have Fun\", " .. sDecisionScript .. ", \"Alonso1\") ")
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Be good\", " .. sDecisionScript .. ", \"Alonso2\") ")
  
  --Have fun
  --be good
  
  --be good:
  elseif(sTopicString == "Alonso2") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'll miss the hell out of you too. Be good and watch out for everyone else while I'm gone.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: We won't have you to watch out for us. Or to pull amazing rescues.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: You'll think of something. Just be more careful okay?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Your one to tell me to be careful. But I'll make sure that everyone does alright.[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  --have fun:
  elseif(sTopicString == "Alonso1") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Have some kickass adventures for me while I'm gone.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: They won't be as fun without you around.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Then make sure the adventures are extra amazing to make up for me not being there. Save a dragon for me to fight.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Of course[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  
  --Cohen farewell:
  elseif(sTopicString == "Cohen") then
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen gives me a hearty forearm shake, and smiles at me fondly.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Ah! To be young again. To go off on an adventure simply because it's there. Have a good time and be confident in your decision.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: He pulls me into a bear hug and then whispers to me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: You will always be welcome among us, send word via a barkeeper.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'll remember.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Any advice?\", " .. sDecisionScript .. ", \"Cohen1\") ")
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"I should be fine\", " .. sDecisionScript .. ", \"Cohen2\") ")
  
  
  --any advice
  elseif(sTopicString == "Cohen1") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I look back at the ant-girls. They expect much from me I think.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Any advice?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Hmmm. I have given you a lot of very good advice in our time together. Give it to the ants. They will probably need it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Answer is always simpler than you think.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Thanks Cohen, for everything.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: A pleasure youngling.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  --I should be fine
  elseif(sTopicString == "Cohen2") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I should be fine, after all I'm in good hands[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I raise all four hands.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: I have always told you that overconfidence is a sl-[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: A slow and insidious killer. I know old man.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Good to see that something I told you stuck. In my epxerience ant-girls are reliable fighters. Work with them and you should be safe.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Thanks Cohen, for everything.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: A pleasure youngling.[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  
  --Ron farewell:
  elseif(sTopicString == "Ron") then
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron's on the verge of panic. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: If only I had my spellbook, then I could figure out what's wrong with you. I could fix you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'm fine Ron. This my choice. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I try to send out comforting waves but then realize he won't understand them.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Reassure him\", " .. sDecisionScript .. ", \"Ron1\") ")
  fnInstruction(" WD_SetProperty(\"Add Decision\", \"Tell him to stay safe\", " .. sDecisionScript .. ", \"Ron2\") ")
  
  --Reassure him
  --Tell him to stay safe
  
  --Reassure him:
  elseif(sTopicString == "Ron1") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N", 1.0)
local iAntMeal = VM_GetVar("Root/Variables/Characters/Scenario/iAntMeal", "N")
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Are you sure you haven't been drugged or something?[BLOCK][CLEAR]") ]])
 if(iAntMeal == 1.0) then
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I hestiate slightly[BLOCK][CLEAR]") ]])
  end
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'm in my right mind Ron. Relax, the ant-girls watched out for all of us getting here.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: I just don't want to break up the team.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'll miss you Ron, don't get me wrong. But you guys will be fine without me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Be safe.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: You too Ron.[BLOCK][CLEAR]") ]])
   fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  --Tell him to stay safe
  elseif(sTopicString == "Ron2") then
--Variables.
VM_SetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N", 1.0)
  WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Ron.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Be safe while I'm gone. I know you love fire spells. But be a little bit more careful, you almost started a forest fire back there. Otherwise you should be fine.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: The guys are going to bully me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: We aren't bullying you, we are just teasing you. You are a bit too much fun to tease.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Am not.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Are too.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Am not![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I hold up all four of my hands.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Just take care of yourself and watch where you throw your magic. For me Ron.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: I will. For you.[BLOCK][CLEAR]") ]])
   fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Farewell")
  
  
    elseif(sTopicString == "Leaving") then
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: You might say a farewell forever, that's what meeting someone else is a long farewell. But in time they end.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone whoops, hollars and waves as I head into the forest with the ant-girls. No, my sisters.[BLOCK][CLEAR]") ]])
    --go to sisters ending
           fnCutsceneBlocker()
          LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/400 1s Sisters Endings.lua", "Begin Scene")
        end
      




--[=[

--Summary comment: this is the start of the game! The player sets their gender before this scene. This part functions to introduce the characters, their dynamics and the setting. it adapts up to the passage Whoosh

--[Scene]
--This constitutes the scene for now. The scene splits when speaking to the adventurers, but otherwise loops back to this script.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

-- |[Character Gender Change Function]|
--Changes the displayed main character gender, only needs to be deployed once and will work all times after that. Needs a jump to properly make the switch. Deploy the function before calling variables.
function fnChangePlayerGender(psGender)

    --Arg check.
    if(psGender == nil) then return end

    --If Boy:
    if(psGender == "Boy") then
        WD_SetProperty("Register Voice", "Irene", "MOTF|Voice|IreneM")
        DialogueActor_Push("Irene")
            for i = 1, #gsaEmotions, 1 do
                DialogueActor_SetProperty("Add Emotion", gsaEmotions[i], "Root/Images/MOTF/Characters/Isaac|Neutral", true)
            end
        DL_PopActiveObject()

    --If Girl:
    elseif(psGender == "Girl") then
        WD_SetProperty("Register Voice", "Irene", "MOTF|Voice|IreneF")
        DialogueActor_Push("Irene")
            for i = 1, #gsaEmotions, 1 do
                DialogueActor_SetProperty("Add Emotion", gsaEmotions[i], "Root/Images/MOTF/Characters/Irene|" .. gsaEmotions[i], true)
            end
        DL_PopActiveObject()
    end
end
  --Switch Male to Female 
    --fnInstruction([[ fnChangePlayerGender("Girl") ]])

-- |[Main Scene]|
--Once the game has picked the player's initial gender, we go here.
--this section covers waking up, introduces the player's party and leads up to conversation options.
if(sTopicString == "Begin Scene") then
            local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestA][REMCHAR|Irene]The morning sun begins to peek through the canopy as I sit, waiting, on a rock. The fire is mere embers as the birds begin to sing for the day.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I yawn. The night watch was long. I'll be tired today. My companions lay snoring, except one.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen had woken and left to go find our breakfast. Alonso is stirring. Ron and Frederick still sleep, and could do so through a tornado.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Time to wake them.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I give Alonso a prod, and he groans.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody](Alonso is a childhood friend of mine. He's the closest thing our group has to a lieutenant. Probably a side effect of always trying to emulate Cohen.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Morning. Five more minutes?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: You wish. Go see about breakfast, I'll wake everyone else up.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso yawns and scratches himself a few times. He gathers his things and wanders off to see what the caravan has on offer.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Alonso][REMCHAR|Irene]I rub the sleep out of my eyes and look at the civilians who packing up their camp. We've camped with a caravan we were hired to guard on a grassy, open plain. The copse of trees we're under is the only patch of shade from here until a great forest that stretches across the horizon, far from us.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The trees are tall enough to challenge the mountains behind them. On the other side of the forest is our destination.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I yawn. I get the feeling it's going to be a long day.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Next is Frederick, still sawing logs. His young-looking face betrays a happy dream.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle][ADDCHARPOS|Frederick|-1|Neutral|Middle+Body]I wake him up rudely.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Wake up, Frderick. The cows got loose again.[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Sad]
    fnInstruction([[ WD_SetProperty("Append", "Narrator: He bolts awake frantically, looking in every direction before settling on me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Oh not again...[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: He blinks a few times, then gives me an annoyed look.[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Angry]
    fnInstruction([[ WD_SetProperty("Append", "Frederick: You bastard, I was just about to get laid![BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: In your dreams, bucko. Literally.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (Frederick is another of my childhood friends. He has always been quick with his hands and joined our group as a professional treasure finder. Sometimes you really do find treasure during adventures.)[BLOCK][CLEAR]") ]]) 
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (More often in someone's pockets though.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Wake up Ron for me. I'm going to get something to eat.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] Oh that'll be easy.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Frederick] [ADDCHARPOS|Frederick|-1|Neutral|Middle+Body+Body]Frederick scoots over to where Ron is sleeping and in a loud authoritative voice announces.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Wake up, Ron, or you are going to miss breakfast.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Ron|-1|Neutral|Middle+Body+Body+Body] Ron sits up like a tree falling in reverse.[BLOCK][CLEAR]") ]])
    --[EMOTION|Ron|Sad]
    fnInstruction([[ WD_SetProperty("Append", "Ron: What? Already?[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Happy]
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] Both Frederick and I laugh.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: No, not yet, Ron, but get a move on so you don't really miss it![BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Ron|Neutral] (I don't know Ron as well as the others. He is the mage of our group and has a frightening fascination with fire magic.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (The only thing fiercer than his fire is his appetite. He insisted that we pack extra provisions in the carts.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene][REMCHAR|Frederick][REMCHAR|Ron]With everyone awake, a circle of caravaneers has assembled around a fresh fire. Breakfast is cooked ground oats and berries in a bowl. Not the best but good for a meal on the road.[BLOCK][CLEAR]") ]])

    fnInstruction([[ WD_SetProperty("Append", "Narrator: I strap on my gear and check the heft of my sword before joining in.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: A merchant is happily ladelling breakfast out of a big cast-iron pot. [ADDCHARPOS|Cohen|-1|Neutral|Middle+150][ADDCHARPOS|Alonso|-1|Neutral|Middle+Body+150]Alonso is sitting next to Cohen as they pass the bowls along the line.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-Body+150][ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-Body+150][ADDCHARPOS|Ron|-1|Neutral|Middle-Body-Body-Body+150]Once everyone has a bowl, Cohen digs in. He eats with an appetite to rival Ron despite his thin frame.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] As a veteran, aged adventurer, Cohen is the leader of our group. He is probably the most lethal person I will ever meet. If you believe everything he says he has been fighting since he was a child.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Breakfast is good, and Cohen goes over our plan for guarding the caravan today. The routine is the same as the past few days, but Cohen always stresses preparedness.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Cohen: Be careful today, younglings. Thorbald Hollow is known to have... Word for big scary things you fight.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Dragons?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Wolves?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Monsters?[BLOCK][CLEAR]") ]])  
      fnInstruction([[ WD_SetProperty("Append", "Cohen: Monsters in it. They're rare, but they are not myths.[BLOCK][CLEAR]") ]])
      --[EMOTION|Frederick|Happy]
    fnInstruction([[ WD_SetProperty("Append", "Frederick: What kind of monsters? The growly kind, or the cute monstergirl kind?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I give Alonso a poke.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: He'd like a monstergirl, wouldn't he?[BLOCK][CLEAR]") ]])
    --[EMOTION|Alonso|Happy]
    fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Laugh] Oh they'd eat you alive, buddy.[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Angry]
    fnInstruction([[ WD_SetProperty("Append", "Frederick: You're a laugh riot![BLOCK][CLEAR]") ]])
	--[EMOTION|Cohen|Special]
    fnInstruction([[ WD_SetProperty("Append", "Cohen: There's nothing to laugh about. Female or not, monsters are still monsters. Take them seriously, because they'll take you seriously.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I nod solemnly. I'm hoping for a peaceful trip for once and maybe even a payday. Frederick is evidently hoping for action - violent or otherwise.[BLOCK][CLEAR]") ]])
    --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Ron: Not much has been written about monsters, are they very dangerous?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Yes. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Neutral] Oh.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Cohen][REMCHAR|Ron][REMCHAR|Irene]After breakfast, the merchants assemble the pack animals and we get underway. I'm still tired.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestB]The edge of the forest is demarcated by a scattering of rocks, moved there by some ancient farmer who once tilled the plains. Beyond them, thick brush gives way to imposing, darkened trees. Even from here I can see an oppressive gloom within in the forest.[BLOCK][CLEAR]") ]])
       if(sGender == "Girl") then
fnInstruction([[ WD_SetProperty("Append", "Narrator: There were forests at home, but none so deep or majestic as this. Despite the danger I can't help but feel excited.[BLOCK][CLEAR]") ]])
end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: When we reach the entrance, the merchants spend a few minutes rearranging the caravan into a column to fit the narrowing road.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I have a few minutes to spare while they get organized. What should I do?)[BLOCK]") ]])


   --LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/400 1 Endings.lua", "Begin Scene")
    --Decision sequence. It allows the player to talk to friends or head straight into the forest.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Talk to my friends\", " .. sDecisionScript .. ", \"Friends\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"See to my own gear\",  " .. sDecisionScript .. ", \"Gear\") ")
    fnCutsceneBlocker()
    
--[See To Gear]
elseif(sTopicString == "Gear") then
	WD_SetProperty("Hide")
	--avoid talking to friends option
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody] I do a quick double-check of my gear. My armor is strapped on but not too tight. My sword doesn't catch in its scabbard. My canteen's buckle won't give out.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I go across my mental checklist. Before long, the caravan sends up a shout. We take positions and are underway.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I wonder when I will see the sun again.[BLOCK][CLEAR]") ]])
  LM_ExecuteScript(LM_GetCallStack(0), "After Discussion")
    
--[Talk To Friends]
--talking to friends options, works to introduce characters better, can only talk to one then move on. Adapts "Talk to friends" passage"
elseif(sTopicString == "Friends") then
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My friends and colleagues are preparing for the trip ahead. Who should I speak to?[BLOCK]") ]])

    --The decision script for each character is different.
    local sCohenScript =     "\"" .. gsRoot .. "Scenario 100 The Caravan/110 Cohen.lua"     .. "\""
    local sAlonsoScript =    "\"" .. gsRoot .. "Scenario 100 The Caravan/120 Alonso.lua"    .. "\""
    local sFrederickScript = "\"" .. gsRoot .. "Scenario 100 The Caravan/130 Frederick.lua" .. "\""
    local sRonScript =       "\"" .. gsRoot .. "Scenario 100 The Caravan/140 Ron.lua"       .. "\""

    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Cohen\", "     .. sCohenScript .. ",     \"Begin\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Alonso\", "    .. sAlonsoScript .. ",    \"Begin\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Frederick\", " .. sFrederickScript .. ", \"Begin\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Ron\", "       .. sRonScript .. ",       \"Begin\") ")
    fnCutsceneBlocker()

--[Rejoin]
--After talking to any one person, this is where the scripts rejoin.
elseif(sTopicString == "After Discussion") then

    --Variables.
    --I talked to frederick variable gets called in this section, it establishes the player talked to frederick about robbing the wagons.
    local iTalkedToFrederick = VM_GetVar("Root/Variables/Characters/Scenario/iTalkedToFrederick", "N")
   local sPlayerGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
   --I suspect the above line to be a typo, all other calls use sGender
        local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
--this section is the ambush and attendant choices, adapts up to "Whoosh", it's where I introduce the various character arcs which are resolved during the escape.
    --The previous script should have handled setting up the dialogue.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MUSIC|Intro][REMCHAR|Irene] After much waiting, the caravan gets underway. The forest is the more claustrophobic with the caravan taking all the space on the road.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Cohen|-1|Neutral|Middle+150][ADDCHARPOS|Alonso|-1|Neutral|Middle+Body+150][ADDCHARPOS|Irene|-1|Neutral|Middle+150-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+150-Body-Body][ADDCHARPOS|Frederick|-1|Neutral|Middle+150-Body-Body-Body]Despite being dark under the forest canopy, we keep a good pace.[BLOCK][CLEAR]") ]])
  --note to self: organize the formation
	fnInstruction([[ WD_SetProperty("Append", "Ron: The road here is clearer than the one on the plains.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It's granite and well-laid. You think it's Queen's Road?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: See how the trees are grown up along it, and how the stones exposed are mossy?[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Ron: So older than the Queen's Road, then.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Probably, but why is it so clear?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Because a clear road has no cover. Stay on your guard and watch for an ambush.[BLOCK][CLEAR]") ]])
  
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Cohen][REMCHAR|Ron][MOVECHAR|Irene|Middle+150|QUADINOUT] We begin to march through the woods. Guarding a caravan is boring work. Look fierce, keep an eye on the scenary, try not to go mad.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The plants crowd around the road, competing with one another for the pitful amount of sunlight.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: My mind wanders, picturing various ways the monsters Cohen mentioned might look.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Blush] Would they be beautiful, fearsome, alien?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Cohen|-1|Neutral|Middle+150-Body] I am startled by Cohen softly treading beside me. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: You don't need to work so hard on looking fierce youngling. All it will do is make your face hurt, and then you have to fight with a face that hurts.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] What brings you around Cohen? I figured you would be too busy watching the trees.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Cohen: I'm keeping an eye on them, in case they try anything. Your turn to pass out water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought: [EMOTION|Irene|Offended][VOICE|Irene] (Really? This must be the least lucky day of my life.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I had watch last night you know.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Downside to being the toughest around is everyone asks you to do the hard work. Just think of it as part of your heroic saga.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: 'And then the young adventurer triumphantly carried water!'[BLOCK][CLEAR]") ]])
  if(sGender == "Boy") then
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] I guess it's taking care of the others. Although I never thought I'd leave home to carry water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: The most basic care is often the most important, besides youngling, we will fight better if we aren't thirsty.[BLOCK][CLEAR]") ]])
  else
  fnInstruction([[ WD_SetProperty("Append", "Irene: I left home to see fantastic sights and go on marvelous adventures. Now I get to carry water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: In fantastic lands people still carry water. I've been there and carried water there too. Adventuring can't be all swash and buckle or everyone would do it.[BLOCK][CLEAR]") ]])
  end
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Grumbling I heft up the water skins. They aren't that heavy, at least to someone who grew up doing manual labor. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Frederick|-1|Neutral|LEdge][REMCHAR|Cohen] I wait in place for the progress of the caravan to deliver Frederick to me.[MOVECHAR|Frederick|Middle+150-Body-Body|QUADINOUT] [BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Frederick|Middle+150-Body|QUADINOUT] Frederick is staring off into space as he walks, he looks unusually pensive. [BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Narrator: He turns surprised as I clear my throat.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: And here I thought all the beasts of burden were pulling the carts. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] Don't you start Frederick. I'll have you know it's very important to carry the water. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] What were you thinking about? It's rare to see you looking that grim.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Frederick: ...[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Frederick: I was thinking of ways to make sure I didn't run if a fight started. I figured I could tie myself to Alonso or you. Both of you are foolhardy.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Not the sort of thoughts I'd expect from you Frederick, what's got you worried?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Cohen, if he's on guard we should all be on guard.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Irene|Offended](He has a point there.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Neutral] I've been worried for a while to be honest, what if I'm not as brave as everyone else?[BLOCK][CLEAR]") ]])
  --turn to smile here
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Watching the trees gives a lot of time to ruminate.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Well, Cohen always says there are two kinds of people, those who wet themselves and run and those who wet themselves and fight. He also says you can't tell before hand which kind a person will turn out to be.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Yeah[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'm confident you won't run Frederick. But if there is a fight everyone will be there with you so it won't be so bad.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
    fnInstruction([[ WD_SetProperty("Append", "Irene: If your courage fails you, I'll be right there. So you don't need to worry.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Irene: If it looks like you are going to chicken out in front of everyone, I'll grab you.[BLOCK][CLEAR]") ]])
end  
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Thanks.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: He gives me a smile as I refill his canteen.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I nod in response.[MOVECHAR|Irene|Middle+150-Body|QUADINOUT][BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] [REMCHAR|Frederick][ADDCHARPOS|Alonso|-1|Neutral|Middle+150]Alonso is nearby, he's keeping a patient watch over the forest as he trudges along. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: He looks as bored as I feel. But he cracks a smile as he sees me carrying all the water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Having fun so far?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Could be worse, I'm with everyone and nice to be away from home. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'd be carrying water there too.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Alonso: Heh. that's good to hear. I'd feel bad if I convinced everyone to follow Cohen and then it was a miserable time.[BLOCK][CLEAR]") ]])
 if(sGender == "Boy") then
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Irene|Neutral](It's not so bad with the others around.)[BLOCK][CLEAR]") ]])
else
   fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Irene|Neutral](I'm already enjoying way more freedom than I would at home.)[BLOCK][CLEAR]") ]])
end

 fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] How about you Alonso? Having a good time so far?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: I guess so, I feel a bit useless with Cohen around. Cohen's a real hero and it doesn't leave me much to do.[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Alonso: Even if I am his 'lieutenant'[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Irene|Neutral](Alonso is used to being the leader. If I'm humbled next to Cohen, it must be even worse for him.)[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Append", "Irene: Well he's training us, so he'll let you take over sooner or later. Besides we are all here because of you, so I don't think you need to worry too much.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: I guess, thanks for carrying the water.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] My pleasure. Don't sweat it too much Alonso, your moment will come.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][REMCHAR|Alonso] Alonso turns back to watching the forest, obviously not convinced.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Ron|-1|Neutral|Middle+150+Body+Body] It takes a bit of looking to find Ron. He is brushing and whispering to one of the horses as it duitifully plods along.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: As he picks out the brambles in it's hair he chastizes the driver for the sort of feed they are giving the horses.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: When he finishes I approach.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[MOVECHAR|Irene|Middle+150+Body|QUADINOUT]Something bothering you Ron?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: What makes you say that?[BLOCK][CLEAR]") ]])
  --smile here
 fnInstruction([[ WD_SetProperty("Append", "Irene: You always go back to horses when you are nervous. I'd say you tend more carefully to them than to everyone else. It's not you being worried about monsters or you would be on guard.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Irene|Laugh] Hey! I already apologized for being good at healing. Oh, wait that's not what you mean.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Irene|Smirk] I grew up with horses, comes from being in a merchant family. Great listeners horses, well horses and books. All I know came from horses and books.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: I'd say so. I still remember when you didn't know any of the poetic edda or how to hold a spear.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: It's the fact that I don't know anything that's got me nervous. There isn't much written about monsters and the horses aren't talking. I don't know how you and Frederick are so good at reading people or realizing things.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Realizing things? Frederick's only good at realizing he's acted foolish too late.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Well, like recognizing that something is a trap, or when someone is trying to trick you. Like you guys trick me all the time. What if that's what I need instead of magic to beat these monsters?[BLOCK][CLEAR]") ]])
  --the line above needs revisions
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] We only do that to tease Ron, don't take it too hard. What's the thing you say about letters?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Practice makes perfect?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Exactly, you just learn these things through seeing them enough times. Books can't teach it to you completely. As for the monsters, we know just as little as you do. We can figure it out together.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: You might have a point. Everyone else is good at figuring stuff out, and you guys can barely read. There might be hope for me after all.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Thanks Ron. Now take some water and start actually guarding the caravan. Otherwise Cohen's gonna have some sharp words for you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron gets nervous again at the thought. Still he starts watching the forest. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: After putting the water away I take up my position in the formation. [BLOCK][CLEAR]") ]])

  
  
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene][REMCHAR|Cohen][REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Ron]The caravan continues. We find a clearing where the trees break and expose the sun. We take an early lunch.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-Body+150][ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-Body+150][ADDCHARPOS|Ron|-1|Neutral|Middle-Body-Body-Body+150][ADDCHARPOS|Cohen|-1|Neutral|Middle+150][ADDCHARPOS|Alonso|-1|Neutral|Middle+Body+150]The food is bland, but hunger is the best spice. Most of us are focused on the food, but Alonso keeps flirting with the caravan drivers.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'm surprised you can eat so much.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] It's warm and it beats hiking on an empty stomach.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Plus, I had watch last night. I have to keep my stamina up.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Irene|Neutral] If you say so. Just don't make Ron go hungry.[BLOCK][CLEAR]") ]])
    if(iTalkedToFrederick == 1.0) then
      --[REMCHAR|Cohen][REMCHAR|Alonso][REMCHAR|Ron]
        fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Cohen][REMCHAR|Alonso][REMCHAR|Ron] [REMCHAR|Irene][REMCHAR|Frederick][ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle+HfBody]Frederick gives a quick look around to be sure nobody is looking at us.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Third cart from the front, left side. Gilded lanterns, probably bound for some noble's house.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Irene: And therefore going to be carefully accounted for, Frederick.[BLOCK][CLEAR]") ]])
        --[EMOTION|Frederick|Happy][EMOTION|Irene|Special]
        fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] He grins. I shake my head.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] If you're going to make a move, best to do it on the last stretch. Otherwise, they'll notice for sure.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Narrator: He nods and pretends as if our conversation never happened. I really wish it hadn't.[REMCHAR|Irene][REMCHAR|Frederick][BLOCK][CLEAR]") ]])
    end
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Cohen][REMCHAR|Alonso][REMCHAR|Ron][REMCHAR|Irene][REMCHAR|Frederick] [ADDCHARPOS|Irene|-1|Neutral|Middle-Body+150][ADDCHARPOS|Ron|-1|Neutral|Middle-Body-Body+150][ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-Body-Body+150][ADDCHARPOS|Cohen|-1|Neutral|Middle+Body+150][ADDCHARPOS|Alonso|-1|Neutral|Middle+150] Lunch is short and the march resumes.[BLOCK][CLEAR]") ]])
  --add in everyone in formation here, what's the formation?
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The weather is pleasant and though the forest is menacing, so far it is all bark and no bite.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] ... I suddenly hate myself for that pun.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] The trip, however, is as boring as ever. The forest is monotonous. The trees grow thickly together along this part of the path and it's impossible to see far.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator:Frederick starts to sing snatches of a drink song, some part of the heroic edda. He has a good singing voice.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Special][EMOTION|Ron|Happy][EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] Before long everyone is singing. Everyone except Cohen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (That's one of Frederick's favorite tricks to pass the time. Or he's using it as cover to spy on a wagon. But what's got Cohen so upset?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MUSIC|Null] I turn to Alonso to ask about Cohen's demeanor. I see him collapse.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|KSad]
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]  [EMOTION|Irene|Neutral] (Oh hell!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[REMCHAR|Alonso] [EMOTION|Irene|Surprised] Ambushers! Ambushers! To arms![BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting][EMOTION|Frederick|Fighting][EMOTION|Cohen|Fighting][EMOTION|Ron|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I draw my sword and turn to see if the merchants are safe. I stop when I see a shadow drop from the trees above.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Slender with six long carapaced legs and two arms. Compound eyes and a wild hair color. These must be the monsters Cohen spoke of. Arachnes![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|SpiderA|-1|Neutral|Middle-Body-Body+150] The spider-girl drops onto Ron and bites him in the neck. He pushes her off and clutches the wound.[BLOCK][CLEAR]") ]])
    if(sPlayerGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Ron: Isaac... help![BLOCK][CLEAR]") ]])
    else
        fnInstruction([[ WD_SetProperty("Append", "Ron: Irene... help![BLOCK][CLEAR]") ]])
    end
    --[EMOTION|Frederick|KNeutral][EMOTION|Ron|KSad] 
    	fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|Ron] Frederick is struggling to stand. He yanks a dart from his arm and feels his pocket for a cure-all to halt the spread of the poison.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Away with you, beasts![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] Cohen runs to Ron and swings at the spider-girl. She backs away, but her reinforcements are certainly not far off.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (Who do I help?)[BLOCK]") ]])
        --Decision sequence. Ambush sequence, in the twine game who you helps gets referenced. That has yet to be implemented.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Help Ron and Cohen\", " .. sDecisionScript .. ", \"Ronhen\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Help Frederick\",  " .. sDecisionScript .. ", \"Frederick\") ")


--[Cohen Support Sequence]
elseif(sTopicString == "Ronhen") then
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
        fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (The best chance we have of getting out of this is by sticking to Cohen.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KNeutral][EMOTION|Irene|Fighting][EMOTION|Cohen|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Frederick][MOVECHAR|Irene|Middle-HfBody|QUADINOUT][MOVECHAR|Cohen|Middle+HfBody|QUADINOUT] My sword is in my hand as I sprint to Cohen. Ron is slumped over, unconscious. There must be venom in the spider's bite.[BLOCK][CLEAR]") ]])
  --Remember to put Cohen and Isaac back to back with Ron kneeling in the middle
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|SpiderB|-1|Neutral|Middle+HfBody+Body][MOVECHAR|SpiderA|Middle-Hfbody-Body|QUADINOUT] Cohen and I are soon back to back, fending off the spiders. We're outnumbered, but Cohen is unmatched with a blade.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: After taking several injuries, the spider-girls realized they couldn't overwhelm us. They pull back, regroup, and began trying to wear us down.[BLOCK][CLEAR]") ]])
  --have spiders pull back a space here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: For a time adrenaline gives me a frantic strength.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: But it fades. I was tired already and my swings start to slow.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Noticing my strength fading, Cohen begins doing my fighting for me. He parrys a spear-thrust that would have disarmed me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Run. Now![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[EMOTION|Irene|Sad][VOICE|Irene] (I can't abandon them, can I?)") ]])
    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"It's the only option\", " .. sDecisionScript .. ", \"Run\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Not happening\",  " .. sDecisionScript .. ", \"Stay\") ")

    
elseif(sTopicString == "Run") then
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene](Cohen is rarely wrong about this sort of thing. How did it come to this?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] We need the right moment.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Cohen gives a grim smile.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Quite right Youngling. Remember, we can't let them catch all of us.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting][EMOTION|Cohen|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Offended]The fighting continues. To the surprise of the spiders, Cohen suddenly presses forward. He disarms two of them at once and kicks away their spears.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Now![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle-HfBody-Body-Body|Teleport] I dart forward. I give a brief surprise strike to one of the spiders, and surprise them again by simply running past.[BLOCK][CLEAR]") ]])
    --this might be the only time I use to the teleport move
    --Go to 'CohenEscape' to rejoin the script.
    fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "CohenEscape")
    
elseif(sTopicString == "Stay") then
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought: [EMOTION|Irene|Cry] [VOICE|Irene] (Like hell I will!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Angry] Not happening.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I fight with the rage being forced back and made to consider abandoning my friends brings.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen unleashes a curse so foul, even the spiders pause.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen:[EMOTION|Irene|Angry] You always were the stubborn one. Whatever happens, we must not all be caught.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting][EMOTION|Cohen|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The battle resumes. Eventually, Cohen strikes a lucky blow, knocking one spider into two others. They collapse in a heap.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle-HfBody-Body-Body|Teleport] Cohen grabs me by the scruff of my neck and throws me towards the opening.[BLOCK][CLEAR]") ]])
  --movement here, maybe teleport?
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Run, damn you![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The choice made, I shove aside one of the surprised spiders and bolt.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I need to get far enough to make Cohen's efforts worth it...)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    --Go to 'CohenEscape' to rejoin the script.
    LM_ExecuteScript(LM_GetCallStack(0), "CohenEscape")
    
elseif(sTopicString == "CohenEscape") then
    WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --Dialogue is already set up by the previous section.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Cohen][MOVECHAR|Irene|Middle-Hfbody-Body|QUADINOUT][MOVECHAR|SpiderA|REdge|Teleport][MOVECHAR|SpiderA|REdge-Body|Teleport] I bob and weave through the crowd. They had been focusing on our small group. The caravan has moved down the road and the guards are calling to us.[BLOCK][CLEAR]") ]])
  --movement where it's Isaac a few spots ahead of the spiders?
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Angry] The spider-girls, however, are not done with us. Two of them move to cut off my escape.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I shove myself through the underbrush, hesitating only to shield my eyes.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|SpiderA|Middle+Body|QUADINOUT][MOVECHAR|SpiderB|Middle+Body+Body|QUADINOUT] A crashing behind me suggests I'm not getting away clean. The spiders are following me at a lazy pace.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I can't outrun them, I have to do something!)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I pick a good tree, put my back to it, and raise my sword.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: If you want some, come and get it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|SpiderGirlA|Neutral] The spiders exchange a bemused look.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (What does she know that I don't?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I am answered by a pain in my neck. I clutch at it. A dart. The same kind as the one that hit Frederick.[BLOCK][CLEAR]") ]])
  --Have Irene go to kneel here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Things go black as I hear the spiders laugh.[BLOCK][CLEAR]") ]])
   fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|Null][REMCHAR|SpiderA][REMCHAR|SpiderB][REMCHAR|Irene] The world is made of darkness, a sloshing sound and omnipresent wetness.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    --Blackout.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/200 Blackout.lua", "Begin Scene")

--[Help Frederick Sequence]
elseif(sTopicString == "Frederick") then
	WD_SetProperty("Hide")
    
    --Variables.
    local sPlayerGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --[EMOTION|Frederick|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene]  (Frederick is a sitting duck by himself!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][REMCHAR|Cohen][REMCHAR|SpiderA][REMCHAR|SpiderB][MOVECHAR|Irene|Middle+Body|QUADINOUT][MOVECHAR|Frederick|Middle|QUADINOUT]  I run to Frederick, who has taken a position over the fallen Alonso. Alonso has fallen insensate.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:A quick glance indicates that the dart knocked Alonso unconscious, rather than killing him.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] I hope you're not giving up already, Frederick![BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] I have not yet begun to fight![BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He grits his teeth and leans against me. I hope his cure worked, because my odds do not look good otherwise.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|SpiderB|-1|Neutral|Middle+Body+Body][ADDCHARPOS|SpiderA|-1|Neutral|Middle-Body] The spider-girls fan out and form a circle around us. The caravan disappears down the trail.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With the element of surprise gone we seem to make a reasonable prize. They care only to capture us.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick's hit arm is useless but he fights fiercely with his remaining arm.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: They begin to press forward, wary of our weapons. I keep them at bay with a frantic effort.[BLOCK][CLEAR]") ]])
    if(sPlayerGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Hey, Isaac?[BLOCK][CLEAR]") ]])
    else
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Hey, Irene?[BLOCK][CLEAR]") ]])
        end
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] I'm a little busy![BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: I don't think we can hold them...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Even if you're right, what of it?[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  He flashes me a serious look.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He never looks serious.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: You need to go.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: And just abandon you?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: They can't capture us both.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [EMOTION|Irene|Sad] (The worst part is, he's right. Someone needs to escape.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Smirk]
	fnInstruction([[ WD_SetProperty("Append", "Irene:  Maybe you're right. I need a distraction.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KFighting]
    fnInstruction([[ WD_SetProperty("Append", "Frederick:  But I can barely stand...[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] I don't need you to stand![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With all of my strength, I heave Frederick forward. [MOVECHAR|Frederick|Middle-Body|Teleport] He flies, surprised, into the spiders. They're just as confused as he is.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|SpiderB|Middle|QUADINOUT][MOVECHAR|Irene|Middle-body-body|Teleport] When they try to back up, they bump into each other. Everyone collapses into a heap.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KNeutral]
    fnInstruction([[ WD_SetProperty("Append", "Frederick: N-not cool![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Thanks Frederick![BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral][REMCHAR|Frederick][REMCHAR|SpiderA][REMCHAR|SpiderB]I run, and the spiders struggle to stand. I get away by running at full speed blindly into the forest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] [MOVECHAR|Irene|Middle|QUADINOUT](Lose them, double-back, get even. No problem.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The brush whips at my face as I run by. I cover my head with my arms.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I fall flat on my face. I look down to see a milky-white web binding my legs.[BLOCK][CLEAR]") ]])
  --go to kneel [EMOTION|Irene|KAngry]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Damn it![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I look up. A spider-girl is suspended from a tree branch, snickering.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Double damn it![BLOCK][CLEAR]") ]])
  --[EMOTION|SpiderGirl|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I try to cut at the webs, but the spider is on top of me. The spider looks hilariously smug as things go dark.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|Null][REMCHAR|SpiderA][REMCHAR|SpiderB][REMCHAR|Irene] The world is made of darkness, a sloshing sound and omnipresent wetness.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: ...[BLOCK][CLEAR]") ]])
  --hypothetical step for test skip
  --fnInstruction([[ WD_SetProperty("Append", "Narrator:[BACKGROUND|ForestB] ...[ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody] [BLOCK][CLEAR]") ]])
fnCutsceneBlocker()
    --Blackout.
   LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/200 Blackout.lua", "Begin Scene")
    --test skip
      --LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/240 1 trapped ant.lua", "Begin Scene")
  end
    
]=]