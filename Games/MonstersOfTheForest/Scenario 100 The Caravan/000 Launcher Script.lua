--[Launcher Script]
--This gets called in order to load the images needed for the scenario. It will only load if it gets
-- booted from the main menu, otherwise this should already be loaded in Adventure Mode.
gsRoot = fnResolvePath()
io.write("Root is " .. gsRoot .. "\n")

--[ ====================================== Graphics Loading ===================================== ]
--[User Interface]
--User interface parts, such as combat and level-up. Also contains a lot of portrait pieces that are used in
-- the UI, such as the turn-order portraits.
    
--Post-exec script is nil.
gsPostExec = nil

--Open.
SLF_Open(gsRoot .. "Data/UI.slf")

--[Extraction Function]
local fnExtract = function(psaNames, psPrefix, psDLPath)
    
    --Arg check.
    if(psaNames == nil) then return end
    if(psPrefix == nil) then return end
    if(psDLPath == nil) then return end
    
    --Path.
    DL_AddPath(psDLPath)
    
    --Extraction loop.
    local i = 1
    while(psaNames[i] ~= nil) do
        DL_ExtractBitmap(psPrefix .. psaNames[i], psDLPath .. psaNames[i])
        i = i + 1
    end
end

--[Lookup Tables]
local zaLookups = {}
zaLookups[ 1] = {"AdvDialogue|", "Root/Images/AdventureUI/Dialogue/", {"BorderCard", "CommandList", "ExpandArrow", "NameBox", "NamelessBox", "NamePanel", "TextInput"}}

--[Execution]
for i = 1, #zaLookups, 1 do
    fnExtract(zaLookups[i][3], zaLookups[i][1], zaLookups[i][2])
end

--Stamina Bar
DL_AddPath("Root/Images/AdventureUI/Stamina/")
DL_ExtractBitmap("ACW|StaminaBarOver",   "Root/Images/AdventureUI/Stamina/BarOver")
DL_ExtractBitmap("ACW|StaminaBarMiddle", "Root/Images/AdventureUI/Stamina/BarMiddle")
DL_ExtractBitmap("ACW|StaminaBarUnder",  "Root/Images/AdventureUI/Stamina/BarUnder")

--Utility
DL_AddPath("Root/Images/AdventureUI/Utility/")
DL_ExtractBitmap("WhitePixel", "Root/Images/AdventureUI/Utility/WhitePixel")

--[Scenario]
--Graphics unique to Monsters of the Forest.
SLF_Open(gsRoot .. "Data/MOTF.slf")

--Backgrounds
DL_AddPath("Root/Images/MOTF/Backgrounds/")
DL_ExtractBitmap("BG|ForestA", "Root/Images/MOTF/Backgrounds/ForestA")
DL_ExtractBitmap("BG|ForestB", "Root/Images/MOTF/Backgrounds/ForestB")
DL_ExtractBitmap("BG|ForestC", "Root/Images/MOTF/Backgrounds/ForestC")
DL_ExtractBitmap("BG|ForestD", "Root/Images/MOTF/Backgrounds/ForestD")
DL_ExtractBitmap("BG|ForestE", "Root/Images/MOTF/Backgrounds/ForestE")
DL_ExtractBitmap("BG|ForestF", "Root/Images/MOTF/Backgrounds/ForestF")
DL_ExtractBitmap("BG|ForestG", "Root/Images/MOTF/Backgrounds/ForestG")

--Characters with only neutrals.
DL_AddPath("Root/Images/MOTF/Characters/")
local saCharacters = {"Alonso", "Cohen", "Frederick", "Guard", "Isaac", "Ron", "Antgirl", "Spidergirl"}
local i = 1
while (saCharacters[i] ~= nil) do
    DL_ExtractBitmap("Characters|" .. saCharacters[i] .. "|Neutral", "Root/Images/MOTF/Characters/".. saCharacters[i] .. "|Neutral")
    i = i + 1
end

--Characters with full emotion sets.
gsaEmotions = {"Neutral", "Happy", "Blush", "Smirk", "Sad", "Surprised", "Offended", "Cry", "Laugh", "Angry"}
saCharacters = {"Irene"}
i = 1
while (saCharacters[i] ~= nil) do
    
    local p = 1
    while(gsaEmotions[p] ~= nil) do
        DL_ExtractBitmap("Characters|" .. saCharacters[i] .. "|" .. gsaEmotions[p], "Root/Images/MOTF/Characters/".. saCharacters[i] .. "|" .. gsaEmotions[p])
        p = p + 1
    end
    
    i = i + 1
end

--Clean
SLF_Close()

--[ ======================================== Audio Setup ======================================== ]
--Loads audio for MotF
LM_ExecuteScript(gsRoot .. "Audio/ZRouting.lua")

--[ ======================================== Other Setup ======================================== ]
--Set the root path.
gzMotFVar = {}
gzMotFVar.sRootPath = fnResolvePath()

--Order classes which rely on these images to construct themselves.
WD_SetProperty("Construct")

--Execute the rest of the construction sequence.
LM_ExecuteScript(gzMotFVar.sRootPath .. "001 System Variables.lua")
LM_ExecuteScript(gzMotFVar.sRootPath .. "002 Scenario Variables.lua")
LM_ExecuteScript(gzMotFVar.sRootPath .. "003 Dialogue Actors.lua")
Debug_DropEvents()

--Boot the new game.
LM_ExecuteScript(gzMotFVar.sRootPath .. "100 New Game.lua", "New Game")