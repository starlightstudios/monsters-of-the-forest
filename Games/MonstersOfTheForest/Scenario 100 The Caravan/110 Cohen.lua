--[Talk to Cohen]
--Subscript to handle talking to Cohen.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Begin]
--Start of the conversation.
if(sTopicString == "Begin") then
	WD_SetProperty("Hide")
	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen is sharpening his ancient sword. Taking special care so that he doesn't lose a finger to the supremely sharp blade.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody][ADDCHARPOS|Cohen|-1|Neutral|Middle+HfBody]I approach. He gives me a stoic nod.[BLOCK][CLEAR]") ]])
--NOTE TO SELF: Remember to put in female version, her reasons are slightly different.
    if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I first met Cohen a few months ago when he was hired to protect my village from bandits. After slaying a good number of them, he put out a call for apprentices. As a third son I didn't have many prospects, so it wasn't much of a choice.)[BLOCK][CLEAR]") ]])
    elseif(sGender == "Girl") then
        fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I first met Cohen a few months ago when he was hired to protect my village from bandits. After slaying a good number of them, he put out a call for apprentices. Settling down for eventual marriage isn't my style. Why let the boys have all the adventures?)[BLOCK][CLEAR]") ]])
    end
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (He's the sort of adventurer you hear of in tales. A Barbarian from the north. Him taking care of a bunch of novice adventurers is probably a form of retirement for him.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Any advice for today, old man?[BLOCK][CLEAR]") ]])
    --[EMOTION|Cohen|Special]
    fnInstruction([[ WD_SetProperty("Append", "Cohen: When we go in there keep your wits about you. Watch the others and make sure they are on guard. The monsters I have told you of exist in more than tales.[BLOCK]") ]])
    
    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Will do\", " .. sDecisionScript .. ", \"Yes\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"You are worrying too much\",  " .. sDecisionScript .. ", \"Worry\") ")
    fnCutsceneBlocker()

--Affirmative.
elseif(sTopicString == "Yes") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Solid advice as always, I'll keep up my guard.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cohen nods, the slightest hint of pride in his gesture.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Good to see some of what I'm teaching is wearing off youngling. Stick together and listen to what I say, you might just live as long as I have.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy]I smile and go back to my preparations.[REMCHAR|Irene][REMCHAR|Cohen][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")

--Worry-wart.
elseif(sTopicString == "Worry") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: You are worrying too much. So long as you are there we will be fine.[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Cohen stops working on his sword and gives me a long stare.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Overconfidence is a slow and insidious killer. I'm fond of you younglings and I hope to see you all live long full lives, but nothing I can do will save you from carelessness.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I'll keep watch, don't worry.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He seems less upset, but I still retreat back to my own preparations.[REMCHAR|Irene][REMCHAR|Cohen][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")
    
end
