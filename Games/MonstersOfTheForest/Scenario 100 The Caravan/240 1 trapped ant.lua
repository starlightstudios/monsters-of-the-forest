--trapped ant scene, leads to do not rescue, rescue, and not yet added ant 1+1+1 TF scene
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (That was fun but not particularly helpful)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral](Well no now I know what the path looks like, so I know what to look for.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It'll take a while but I should be able to find the nest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My train of thought is disrupted when I glance up towards the tree I just climbed down.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I can distantly see a quiet cloud of prideful purple. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Huh? Perhaps I have finally lost it.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (That or I'm on quite the trip.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Moving to the next tree I suspect to be on the spider's path I can see the same cloud in it's branches.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] Frustratingly this revelation doesn't help any of the other colors around me resolve into meaning.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] In frustration I try to stop seeing the colors. It feels a bit like closing my eyes but I am finally able to mostly dim most of them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] Then with a feeling like narrowing my eyes I am able to focus on the purple colors.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: With an ability like that it goes easier, all I have to do is pick a likely tree and check for the colors. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: This is a roaring success, helping me make much better time. Until of course I almost stumble into a huge web strung between two trees.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Thankfully I stop at the last possible moment.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Surprised] (Whew! That was close.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry] (Damn spiders probably put the webs to catch monsters doing what I'm doing.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I get caught in that I'm dead meat. I don't have a knife large enough to cut myself out with.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (What makes this one different?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying very carefully not to accidentally trip into it, I sniff the web.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It smells like absolutely nothing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (So the other one smelled so I guess the colors are scents?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I wonder what the others are going to smell like?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I wonder what I smell like?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Probably bad. I was sweaty and dusty before all this started. Whatever the spiders bathed me in probably didn't help.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] It takes a while but I know I'm on the right track as the rate of webs increases[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Eventually, I can hear a slight rustle. I can see a gap in the trees suggesting a clearing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My pocket knife is in my hand instantly, for all the good it would do.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (I don't have much of a chance of fighting whatever is ahead.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Wait a minute! I have Scent-o-vision!)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] With a twitch of a muscle I didn't have yesterday, colors suddenly flood my vision.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Most of these are still meaningless, but a few are coming from the clearing. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a subtle scent. So subtle as to be almost undetectable. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's like when a large drum is played in the distance and you can feel the vibration more than hear it.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Afraid of losing the trail, or getting ensnared I creep forward cautiously. Trying to peak through the foliage, into the small clearing beyond.[BLOCK][CLEAR]") ]])
  --move forward a few spaces here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I can't see anything.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying to cover the tremor in my legs, I enter the small clearing my pocket knife in hand. The clearing is no larger than a wagon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The clearing is dominated by a spider-web larger than I am strung between two trees. And there is ant-girl trapped there. She must of thrashed something terrible, as she is cocooned in the web.[BLOCK][CLEAR]") ]])
  --Should I have the ant-girl show up in this part?
  --I might need a webbed sprite, I want to introduce what the handmaiden looks like as soon as she's introduced.
  
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Handmaiden|-1|Neutral|Middle+HfBody] [EMOTION|Irene|Neutral] Freeing her would be hard, she is far up there and I'm shorter than I used to be. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Seemingly to pass the time she is swinging in the cocoon upon the web. One hand has snaked though the web to make the rustle I heard.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She glances out of the cocoon, her large black opaque eyes looking at me hopefully, almost expectant. Her expression grows confused and then cautious as her antenna twitch. It's like she doesn't know make of me. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Doesn't look like she's bait for some spider-girl trap.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I could probably rescue her, assuming I want to put in the effort. I'm here for my friends, not to rescue a monster-girl.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Even if I strongly resemble her at this point.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (However I'm only free myself due to a lucky turn, she needs helps as much as I did. But if something goes wrong and rescuing her tips off the spiders then I'm putting my friends in peril.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (I'm sure Cohen would say do it. He was always pro-rescue the damsel in distress. I'm not sure she counts as a damsel though.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "RescueDecision") 

--Choice:
--Rescue her, leave her

elseif(sTopicString == "RescueDecision") then
 	WD_SetProperty("Hide")
	--rescue choices
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Rescue her\", " .. sDecisionScript .. ", \"Rescue\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Leave her\",  " .. sDecisionScript .. ", \"Leave\") ")


--Leave her:
elseif(sTopicString == "Leave") then
	WD_SetProperty("Hide")
	--confirm not rescue choice
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (The odds are bad and I don't need to make them worse. This is a suicide mission. It would be even more foolish to stop and rescue the ant-girl.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It might get me caught by the spiders.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It would take time I don't have.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I can make up any number of reasons. The simple fact is that I choose not to.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: ...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Sad] I sigh and give a mournful bow to the ant ant-girl. She looks at me sadly. Perhaps you imagined it but you think the ant-girl gave a short sad nod. Monsters must know the rules of the forest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As I turn my heart practically wrenches in half. Trying to walk away is like considering tearing off one of my arms. [BLOCK][CLEAR]") ]])
  --have Isaac turn back at this point.
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm on a mission of mercy already, what would the harm be in helping her?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Can I really call myself a hero if I live someone trapped like that?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (How can I justify not helping a fellow sis- traveler?)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "RescueDecision1") 
  
elseif(sTopicString == "RescueDecision1") then
   	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (How can I justify not helping a fellow sis- traveler?)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Reconsider\", " .. sDecisionScript .. ", \"Rescue\") ")
   fnInstruction(" WD_SetProperty(\"Add Decision\", \"Leave her\", " .. sDecisionScript .. ", \"Harden\") ")


--Choice:
--Reconsider, harden your heart
elseif(sTopicString == "Harden") then
	--Harden your heart
  --maybe go so far as to revert to outline at this juncture
  VM_SetVar("Root/Variables/Characters/Scenario/iAntRescue", "N", 0.0)
  	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --didn't rescue, leads to antrescue is 0 sequence
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
 	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's hard to consider, but I take a deep breath and harden my heart[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I know Cohen wouldn't approve.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Cry] It shouldn't be as hard as it is. It's like tearing off an arm.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: But I do it anyway.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As I leave the clearing I take one look back, the ant-girl has closed her eyes in resignation[REMCHAR|Handmaiden][BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Exunt\")"
    fnInstruction(sString)





elseif(sTopicString == "Rescue") then
--Rescue her:
 VM_SetVar("Root/Variables/Characters/Scenario/iAntRescue", "N", 1.0)
	WD_SetProperty("Hide")
	--rescue choice, sets iAntRescue to 1, may lead into ant 1+1 sex scene
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Well I can't make things worse.[BLOCK][CLEAR]") ]])
  --[EMOTION|Isaac|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene:  It's great that I'm in a place where things can't get worse.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I had better shut up before I really jinx myself.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Poor dear, did herself more harm than good in trying to break free. She has worked herself further up in the web that I am tall.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Shortened as I am, that's less than you would think.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm not going to be able to destroy the web with this pocket knife. Be hard pressed if I had a machete.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmm, I don't need the web destroyed so much as I need the cocoon within reach. So she just needs to fall down here.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Good thing I misspent so much of my childhood. Spiderweb doesn't burn, it's what is in the spider-web that burns. The web itself just kind of melts when near fire.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (These webs are fresh enough to be safe if I use fire to rescue the ant-girl. Last thing I want to do is cook her.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I cast about for the driest wood I can find. The ever-present gloom makes this hard. Wet wood will make smoke, smoke will make trouble.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally in desperation I show what tinder I have been able to gather to the ant-girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a great deal of effort the ant-girl contorts her free arm to point at at the web.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I pause trying to figure out what she means, on a whim I sniff.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The woods around me come alive in a multitude of colors. The ant-girl is putting out a dark woody scent. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: That's not particularly helpful, half the forest gives off colors like that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Another sniff and I realize the scent has an element of size, it suggests something small.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (What? She's pointing at the web and trying to signal about small woody things.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Oh, Oh.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (The driest stuff in a forest is the trees. She's pointing at the leaves and branches caught in the web nearest the trees.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It takes a bit of doing but I am able to break off the wood that anchors the web to the nearby trees. The web is so huge that it barely makes a difference.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Before long I make a small smokeless fire, Then I quickly ignite a long wooden branch that would have been difficult to lift without my spare arms providing leverage.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Cautiously and standing on now very durable tip-toes I bring the flame to the web around the cocoon. The web shrinks away from the flame, and the cocoon quickly proves too heavy to remain up there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The cocoon falls with an undignified thump.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a bit of a struggle, I cut open a slit in the cocoon. (Okay now that has to be a double entandredre.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girls smiles at me as I work. Once the cocoon has been split, I get to work clearing the spider webs off the ant-girl.[BLOCK][CLEAR]") ]])
  --maybe have the Handmaiden show up here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] It's just about the least sexy white sticky stuff you can imagine.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] She uses that as an opportunity to pull me down with all four of her arms.[BLOCK][CLEAR]") ]])
  --shift to kneel here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: For a second I just lie next to her in the soft remains of the cocoon. She is wreathed in a halo of warm happy scents.[BLOCK][CLEAR]") ]])
  --go to black screen or have Isaac actor disappear
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene] [REMCHAR|Handmaiden] Then she begins to enthusiastically kiss me. Her hands roam over my unfamiliar features freely, Two attacking the girls, while her smaller hands feel the underside of my insectoid abdomen.[BLOCK][CLEAR]") ]])
--NOTE TO SELF: add female case here, also is this rhythm okay?
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Wow! Not what I was expecting. Maybe not the right time either.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I am sort of on a quest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Although it wouldn't be a bad reward for rescuing a sister.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()

   LM_ExecuteScript(LM_GetCallStack(0), "AntTF2+1") 
  --no you can't smooch the ant-girl yet, as that leads in to ant 3 sequence
  elseif(sTopicString == "AntTF2+1") then
 WD_SetProperty("Hide")
 	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"No\", " .. sDecisionScript .. ", \"AntTF2+0\") ")
  
--Enjoy your reward?
--Choice: Yes, no
--rejoin the ant 1+0 plus antintimacy scene here
elseif(sTopicString == "AntTF2+0") then

 --Variables. Need to start calling iAntIntimacy
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --ant intimacy 1 is from 240 trapped ant, otherwise same
    if(iAntIntimacy == 0.0) then
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Listening to sense is rarely fun, but this really isn't the time or place.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I gently but firmly push the ant-girl away. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her black opaque eyes form puppy eyes, but I stay firm. I sit up, [ADDCHARPOS|Irene|-1|Neutral|Middle+HfBody] trying to make sure I remember which direction I entered from.[BLOCK][CLEAR]") ]])
  --have kneel here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The Ant-girl emits a deep blue scent, and then shrugs. [ADDCHARPOS|Handmaiden|-1|Neutral|Middle-HfBody] She stands and curtsies deeply.[BLOCK][CLEAR]") ]])
  --Definitely have Handmaiden show up here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The air around her is filled with a multicolored cloud with too many emotions to pick out.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Worried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] She looks at me expectantly, which grows worried. She points to herself and emits an authoritative umbral scent. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (What?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Oh.)[BLOCK][CLEAR]") ]])
--NOTE TO SELF: need female case
	fnInstruction([[ WD_SetProperty("Append", "Irene: I'm Isaac[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She has no response. I try to imitate her her, and emit a scent that identifies me. [BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Neutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The resulting scent defies description. The ant-girl just looks confused. She examines me throughly, darting around too and fro.[BLOCK][CLEAR]") ]])
  --have Handmaiden move around Isaac actor here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The sudden efforts proves a bit too much for her and she wavers a little. Finally she sighs, and puts up her hands.[BLOCK][CLEAR]") ]])
else 
fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] I stand up trying to remember which direction I entered the clearing from.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl seems surprised by this. She pulls on my hand. Then with a smile she points to herself.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] Uhh, she wants something?[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Narrator: She rolls her eyes, then points at me.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] No wait, she's introducing herself. But she's not saying anything.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Oh right.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The air around her is filled with a multicolored cloud with too many emotions to pick out.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Worried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] She looks at me expectantly. She points to herself and emits an authoritative umbral scent. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Offended] Yeah I don't understand her.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I point to myself.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I'm Isaac[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I'm Irene[BLOCK][CLEAR]") ]])
end
 fnInstruction([[ WD_SetProperty("Append", "Narrator: Trying to follow her example I make a scent which identifies me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] She merely tilts her slightly. She smiles at me and then holds up her hands in a wait position.[BLOCK][CLEAR]") ]])
end
  --[EMOTION|Handmaiden|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  She roots around the clearing and returns with a few oddly colored leaves. She hands them to me with a smile.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Smirk] I smile politely.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] The ant-girl's eyes are opaque and featureless but I get a strong feeling that she is rolling them. With exaggerated care she chews and swallows one. Then she offers them again.[BLOCK][CLEAR]") ]])
   if(iAntIntimacy == 0.0) then
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Sad] It's hard to say no to that. I eat one, it tastes terrible. But not poison terrible. It also helps take the edge off my fatigue.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Sad] It'd be a bit strange to not trust her now.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] The least I can do is humor her. I eat one, it tastes terrible. But not poison terrible. It also helps take the edge off my fatigue.[BLOCK][CLEAR]") ]])
  end
  --[EMOTION|Handmaiden|KSpecial][EMOTION|Isaac|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]  The ant-girl sits me down across from her and then looks at me expectantly.[BLOCK][CLEAR]") ]])
  --rather than using kneel here it might be better to just have them stand
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Man can she even understand me? I hardly understand the scents I'm putting out, much less what she is trying to say.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Still won't hurt to give it the old adventurer try.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I AM HERE BECAUSE SPIDERS[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I splay eight fingers outwards.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: TOOK[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I make a grabbing motion[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: MY FRIENDS[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Here I point to myself and then hold up four fingers. I try to punctuate the point by pointing at myself and making my scent.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KWorried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  The ant-girl takes on an urgent expression. She holds up four fingers, and then exudes a familar umber scent. The scent is like when she pointed at herself.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I shake my head. That's not right. I repeat the four fingers and my scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl looks confused, She points at me and replicates the scent I just made.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I nod.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She holds up four fingers, and then makes a dark brown familar scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I shake my head.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Handmaiden|Neutral] The ant-girl seems to realize something. She points up and makes a scent that reminds me of blue skies.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I can't help but feel like there is some subtle meaning I'm missing. I shrug and nod.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  For a second the ant-girl looks supremely pleased with herself.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  The ant-girl nods, pauses for a second and then hold ups four fingers making the scent I made. Then she lies down suddenly. She does all this while releasing an dark depressing decaying scent.[BLOCK][CLEAR]") ]])
  --kneel here
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (What?)[BLOCK][CLEAR]") ]])
  -- [EMOTION|Handmaiden|KSad]
  --[EMOTION|Handmaiden|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She opens her eyes to look at me, and seems annoyed. She mimes the motion again.[BLOCK][CLEAR]") ]])
  --Handmaiden stops kneeling
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry] (Oh! She thinks they are dead or something similar.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Fuck that, I'm not giving up on them until I see their cold corpses.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I put up four fingers, make the grabbing motion again, put up eight fingers and then point down the trail.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KWorried] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The Ant-girl seems genuinely distressed by my declaration. She gives me a pleading look and tugs on my arm, pointing away from the spider's nest. I stay strong even if she doesn't think much of my chances.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  The ant-girl looks depressed as she starts to draw a circle in the dirt between me and her. She draws an arrow from me to the circle. Then makes a dot in the center of the circle and the splays out 8 fingers.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As she does this makes a prideful purple scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Okay map of the spider's nest.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She points at the part of the circle furthest from me. Then she points to herself, holds up multiple fingers, and uses both pairs of arms to refer to the spaces on both sides of her. She emits a happy brown scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She repeats the motion until I signal I get it.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KWorried]
  --* Put AI here, the player will pick up on more ants faster due to spending time with her
     if(iAntIntimacy == 0.0) then
       	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmmm. A lot of her, a nest maybe? Not sure what else would have more of her.)[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Several of her. A nest or outpost. That brings it's own problems, and opportunities.)[BLOCK][CLEAR]") ]])
  end
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I give her a thumbs up. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] That does not satisfy the ant-girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She points at me, makes my scent, points at the spot on the map where the ant nest is.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KNeutral] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She points at herself and then makes a scent like coming up for air after being underwater for far too long. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (How can I get that from a scent?)[BLOCK][CLEAR]") ]])
--Note to self: The original line is you of stepping on land after being on a boat far too long. This is a reference to the epic odyssey simile. I like that better but it might strain credulity.
  if(iAntIntimacy == 0.0) then
    	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I'm not quite sure what to do under her piercing silent stare. So I nod.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (She wants nothing more than to go there. She must of been stuck in that web for a while.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I can understand that, I'm tired and I miss the others.)[BLOCK][CLEAR]") ]])
  end
  --[EMOTION|Handmaiden|KWorried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl sighs, and puts two hands to her forehead. She repeats the gesture, stressing that one spot again and again. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[EMOTION|Irene|Smirk][VOICE|Isaac] (Her expression reads dumb ass.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[EMOTION|Irene|Neutral][VOICE|Isaac] (She wants me to go there, she must think it's for the best.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Isaac|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (I'm less certain. She's cute but I have no way to know if the other ant-girls are as friendly. Or if I'd be safe in associating with them.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (She won't give up until I agree though.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Isaac|KNeutral] [EMOTION|Handmaiden|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I give her a quick certain nod. This seems to put her mind at ease.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She turns her attention back to the map. She points at the left most part of the circle. She draws an X[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then she uses all four hands to make a fearful face, and emits a dangerous scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm pretty sure that's ant-girl for don't go there.)[BLOCK][CLEAR]") ]])
  -- [EMOTION|Isaac|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] ([EMOTION|Irene|Smirk] Shame we can't understand each other, she's helping me and I can't even thank her.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|KSad] [EMOTION|Isaac|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]I signal that I understand. She smiles and nods sadly, then she destroys the map with all four of her hands.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl stretches all four of her arms and then tries to stand up. She's a little unsteady though.[BLOCK][CLEAR]") ]])
  --[EMOTION|Handmaiden|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I stand up, and give her a hand up. She seems slightly embarrassed but tries not to show it.[BLOCK][CLEAR]") ]])
  --have moving around here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Handmaiden|Neutral] I move to hide the evidence of the fire I made. No need to give the spiders any more hints. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl helps and before long besides from the destroyed web, there is no evidence I was ever here.[BLOCK][CLEAR]") ]])
  --both move here
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmmm, how to tell her goodbye?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] ( [EMOTION|Irene|Happy] I give her a deep bow and smile at her.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Goodbye, best of luck[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Isaac|Neutral] The ant-girl seems unmoved by my display. She tilts her head slightly.[BLOCK][CLEAR]") ]])
--SD: Have two portraits move together, both going right to left.
--[EMOTION|Handmaiden|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I turn to leave, double checking that I'm going the right way.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ant-girl follows.[BLOCK][CLEAR]") ]])
--SD: IA portrait turns here, do I need above line?
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Handmaiden|Neutral] (Huh.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Well I guess we are going the same direction.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (The ant-girl gazes at me with a hint of maternal watchfulness.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I pause. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Would it be alright if I called you something?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her dark opaque eyes betray no hint of understanding.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I'm just going to call you Forma.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I set off into the woods, making sure to hold back any branches across the path so they don't hit Forma.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()
local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Exunt\")"
    fnInstruction(sString)
  
elseif(sTopicString == "Exunt") then
LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/300 1 approach and plan.lua", "Begin Scene")
  end
	