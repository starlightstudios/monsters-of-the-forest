--[Talk to Alonso]
--Subscript to handle talking to Alonso.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Begin]
--Start of the conversation.
if(sTopicString == "Begin") then
	WD_SetProperty("Hide")
	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Flag.
    VM_SetVar("Root/Variables/Characters/Scenario/iTalkedToFrederick", "N", 1.0)
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle+HfBody]Frederick has already finished tying on his armor and is casually inspecting a cart. He straightens up suddenly as I approach.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Oh boy.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Frederick is the thief of the group, he has been trouble maker since we were both kids. Still he is one of the most nimble people I have ever known. He's also slick enough to get out of almost all of the trouble he gets himself into.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (His family raised livestock, which Frederick has always said gave him such quick fingers. It also made him eager to see the world and experience all he can of life. Perhaps a side effect of years of staring at grazing animals?)[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Hello, Isaac.[BLOCK][CLEAR]") ]])
    else
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Hello, Irene.[BLOCK][CLEAR]") ]])
    end
	fnInstruction([[ WD_SetProperty("Append", "Isaac: [EMOTION|Irene|Angry] Frederick, have your hands been wandering again?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Frederick tries to look innocent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He fails.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Of course not. But... why shouldn't they? I can't be held responsible for what they do...[BLOCK][CLEAR]") ]])
    
    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Don't get us in trouble again\", " .. sDecisionScript .. ", \"Trouble\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Just don't get caught\",  " .. sDecisionScript .. ", \"Caught\") ")
    fnCutsceneBlocker()

--Not a good idea.
elseif(sTopicString == "Trouble") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Frederick always had a hard time not doing the thing he shouldn't...)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Do you recall what happened the last time your hands 'took a walk'?[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: But that's just it! I've learned from my mistakes, and won't get caught again.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] I give him a pointed glare.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Really![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: If they catch you, they'll probably refuse to pay us.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  He pouts, but he knows I'm right. I leave him to it as I return to my preparations.[REMCHAR|Irene][REMCHAR|Frederick][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")

--Stealing is fun.
elseif(sTopicString == "Caught") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Though:[VOICE|Isaac] (Despite the lack of formal training and thieves' tools, Frederick is actually pretty good at not getting caught.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: [EMOTION|Irene|Neutral] Just don't get caught.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It was one time, and I would have gotten away with it if that guard hadn't ducked in with a girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Isaac: It doesn't matter why, it matters that it happened. We really can't lose this job.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He gives me a mock salute.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  I'll be as careful as always![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (We are so screwed.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  [EMOTION|Irene|Neutral] I give him a nod and return to my preparations. He resumes eying the carts and discreetly cataloging their contents.[REMCHAR|Irene][REMCHAR|Frederick][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")
    
end
