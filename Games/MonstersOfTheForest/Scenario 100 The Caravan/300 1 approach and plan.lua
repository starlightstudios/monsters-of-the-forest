--approach to the spider nest, has the pre attack choices, will eventually lead to leeroy jenkins and sisters branches
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
   --rescue variation
   if(iAntRescue == 1.0) then
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I continue onward, the increasing density of webs suggesting I'm nearing my destination.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma treads behind me quietly. [BLOCK][CLEAR]") ]])
	--[EMOTION|Handmaiden|Worried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She marks every web we pass with a blaring red scent.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Sad] I join in the effort just to help us keep moving. It's awkward trying to make the right scents, much less put them on something. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: My companion does her best to teach me, 'sounding' out the scent note by note to make sure I make it right.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I finally manage it.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Handmaiden|Happy]
  --AI: She maybe kisses you on thecheek for making an effort?
     if(iAntIntimacy == 0.0) then
       fnInstruction([[ WD_SetProperty("Append", "Narrator:  She pats my head in thanks.[BLOCK][CLEAR]") ]])	
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Blush] She kisses my cheek in thanks.[BLOCK][CLEAR]") ]])	
  end
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Handmaiden|Neutral][EMOTION|Irene|Neutral] By the time we reach our destination, the forest is damn near more web than trees.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally we reach a raised circular hillock, (really closer to a knoll).[BLOCK][CLEAR]") ]])	
end
if(iAntRescue == 0.0) then
  --no rescue variation
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] The number of scentless webs only increases as I continue. Despite the danger it's somewhat reassuring. The more webs the closer to the nest I must be.[BLOCK][CLEAR]") ]])	
 fnInstruction([[ WD_SetProperty("Append", "Narrator: I tread carefully, trying not to think about how I might be trapped the same as the ant-girl behind me.[BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally I reach a raised circular hillock, (really closer to a knoll).[BLOCK][CLEAR]") ]])	
  end
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Huge trees are covered in yellowed spider-webs. Above the webbed walls there are a number of prominent look outs. This must be the place.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (They must keep the hills cleared to help spot intruders.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (After all that I made it, now for the hard part. Actually getting to my friends.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (First step is to do recon, and make a plan after that.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (I can save the panicking for later on.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Offended] (Now I gotta be super careful, the others would never let me live it down if I got caught now.)[BLOCK][CLEAR]") ]])
  --if: $antrescue
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I keep the clearing to my left as I try to sneak around.[BLOCK][CLEAR]") ]])	
   if(iAntRescue == 1.0) then
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma is wavering more than a little, but it's obvious she understands how important not being seen is.[BLOCK][CLEAR]") ]])
  end
	-- end: $antrescue
	fnInstruction([[ WD_SetProperty("Append", "Narrator: After a while I smell something strange.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (Smells sweet, like rotting fruit.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Strange that I can smell it all with how stagnant the air is.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral] (Still whatever it is won't get me in the nest.)[BLOCK][CLEAR]") ]])
  --if $antrescue
   if(iAntRescue == 1.0) then
     --[EMOTION|Handmaiden|Worried] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma pulls my sleeve and then points away from the nest. She compliments this by making a bright yellow scent that reminds me of flowers.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Handmaiden|Neutral] [EMOTION|Irene|Neutral] I smile politely, and she gives a defeated shrug.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As we approach the furtherest point from where I first sighted the spider's nest, Forma touches my shoulder.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She's smiling wide, relieved scents radiating off of her.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She points away from the nest.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The ground is a bit more densely packed, and even from here I can see a few umber scents. [BLOCK][CLEAR]") ]])	
  --[EMOTION|Handmaiden|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Her hand finds mine and she tries to pull me toward what is likely her nest.[BLOCK][CLEAR]") ]])	
  --*AI while it'd be nice to meet a whole bunch of ants as nice as her, I have no idea if the other ants are as nice as she is. If nothing else I need to survey the nest first.
  --*AI Shame I can't really conbey that
  --*AI alternatibely make this little episode a bit more dramatic, Forma won't try to make a crude appeal but she will try her hard to spell out that it's in the player character's best interest
  --*AI how did I do it in the twine game?
  --[EMOTION|Handmaiden|Worried]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her face falls as I don't move. She looks puzzled, and then releases a dazzling array of scents trying to convince me.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: When that doesn't work, she mimes eating and drinking.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I am pretty damn hungry and thirsty.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (But it's risky.)[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma gives me a desperate look obviously concerned.[BLOCK][CLEAR]") ]])	
      fnInstruction([[ WD_SetProperty("Append", "Narrator: In despiration her hands sweep over her body as if offering it.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It's a nice offer and I do need rest. But still.)[BLOCK][CLEAR]") ]])
  end
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (At the very least I need to scope out all of the nest.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Shame I can't really communicate that.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I smile at her politely, shaking my head.[BLOCK][CLEAR]") ]])	
  --[EMOTION|Handmaiden|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Forma looks depressed, and releases a bittersweet scent. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She tries to pull me one last time, and then gives up with a sigh.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She makes a point of standing next to the next and pointing at the ground to drive the point home.[BLOCK][CLEAR]") ]])--[EMOTION|Handmaiden|Special]
   if(iAntIntimacy == 0.0) then
     	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Smirk] Finally she stands on tip toe to kiss me on the forehead, and then she heads towards the spot she pointed at.[BLOCK][CLEAR]") ]])	
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Blush] She takes my small hands in hers and then gently kisses me.[BLOCK][CLEAR]") ]])	
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I kiss back a little, the least you can do for a farewell kiss.[BLOCK][CLEAR]") ]])	
  end
	  --remove Handmaiden actor here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] [REMCHAR|Handmaiden][MOVECHAR|Irene|Middle|QUADINOUT] She suddenly disappears from view. I turn my attention back to the nest.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As I reach a point equidistant from the ant-nest and the place I first spotted the spider's nest, the webs become noticeably more fortified. [BLOCK][CLEAR]") ]])	
  end
  --end $antrescue
  -- $norescue:
   if(iAntRescue == 0.0) then
  fnInstruction([[ WD_SetProperty("Append", "Narrator: At the furtherest point from where I approached the spider nest there is very little to see. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: For a second I catch a hint of a brown, familar scent and then it's gone.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:The dirt seems a bit more packed than normal but it's probably my imagination.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: I continue circumnavigating the spider nest, mindful of the danger all around me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: As I continue around the nest the webs become considerably more fortified.  [BLOCK][CLEAR]") ]])
  end
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A silver haired spider-girl looks at me and eyes meet. Her opaque eyes terrifyingly featureless. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: For a moment my heart stops and then she looks away. [BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I head deeper into the forest, here the trees are stripped and the ground has claw marks.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Hmm, if the nest is fortified to keep whatever lives here out, then maybe I can use them against the spiders?)[BLOCK][CLEAR]") ]])
   if(iAntRescue == 1.0) then
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Forma seemed to think that whatever was here was dangerous. Too dangerous for me, even when she knew I was trying to rescue my friends. It's probably a bad idea.)[BLOCK][CLEAR]") ]])
end
fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Choice1")
	--(END IF)

elseif(sTopicString == "Choice1") then
 	WD_SetProperty("Hide")
   fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
     fnInstruction(" WD_SetProperty(\"Add Decision\", \"Investigate\", " .. sDecisionScript .. ", \"Investigate\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Keep moving\",  " .. sDecisionScript .. ", \"Circle\") ")


--Choice: investigate, keep moving
elseif(sTopicString == "Investigate") then
	WD_SetProperty("Hide")
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	--Investigate: 
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If I move this way I'll run into whatever is causing all this damage.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad](Wait a minute, what's the plan here? )[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I get a whole bunch of very sharp, very dangerous things chasing me and lead them into the spiders?)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry](That's stupid for a lot of reasons.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Choice2")

--Choice: try anyway, maybe something else
elseif(sTopicString == "Choice2") then
WD_SetProperty("Hide")
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry](Maybe I should rethink.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
     fnInstruction(" WD_SetProperty(\"Add Decision\", \"Try anyway\", " .. sDecisionScript .. ", \"Death\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Maybe Something else\",  " .. sDecisionScript .. ", \"Circle\") ")
 
	--Try anyway
  --remove Irene actor here
elseif(sTopicString == "Death") then
  	WD_SetProperty("Hide")
     fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
	 fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene] As it turns out this is a nest of the same things that killed the spider-girl holding that was carrying me.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Unsurprisingly using myself as bait didn't work out, and my tale ends with me being eaten.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I do remember that these things are called gorn though.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: YOU ARE DEAD[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle+HfBody] Now let's try something different.[BLOCK][CLEAR]") ]])	
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Choice1")
	
--Keep moving, maybe something else
elseif(sTopicString == "Circle") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")

	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I keep skirting the edge of the spider's nest until I finally reach the area where I first sighted it.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Special] It takes a bit of scrambling but I manage to put my back to a tree while not crushing my new abdomens.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I take a deep breath.[BLOCK][CLEAR]") ]])	
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Still not time to panic, focus.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Okay, the right side is the least defended, and my end goal is to free my friends.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Assuming they transported them the same as me, then they should be in cocoons. Wish I had my sword.)[BLOCK][CLEAR]") ]]) 
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll need to deal with the spiderwebs, spiders and the cocoons my friends will probably be in.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk] (It's gonna be a tall order, but I have surprise, and that with a good enough plan might be enough to turn the tide. )[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (What should I use?)[BLOCK][CLEAR]") ]])

--Choice: My new body, My experience, My sist- the ant-girls

--My experience
--Note to self: this might be hokey and better delivered though monologue
--this is eventually lead to the sisters or leeroy jenkins choice
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Smirk](I'm an adventurer first and foremost. Current appearances non-withstanding, that's my greatest strength.)[BLOCK][CLEAR]") ]])
     if(iAntRescue == 1.0) then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm sure Forma is interested in helping but I can't risk it.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (What if she wants to delay? What if I become so anty I don't think to rescue the others?)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Better to use what I can rely on.)[BLOCK][CLEAR]") ]])
end
  --*AI: I'm sure forma would be interested in helping but I can't risk it. What if she wants to delay? What if I become so anty I don't think to rescue the others?
  
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If the others were here I'd brainstorm with them, but it's just me. So I'll imagine what they would say.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral](And try not to think about what that says about me.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Advice")
  

--choice: Alonso's advice, Frederick's advice, Ron's advice, Cohen's advice, it comes together. 

elseif(sTopicString == "Advice") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
  --if talked to everybody can put together the plan to attack the nest
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought: [MOVECHAR|Irene|Middle+HfBody|Linear][VOICE|Isaac] (Who should I ask?)[BLOCK][CLEAR]") ]])
local iVarA = VM_GetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N")
local iVarB = VM_GetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N")
local iVarC = VM_GetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N")
local iVarD = VM_GetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N")
if(iVarA == 1.0 and iVarB == 1.0 and iVarC == 1.0 and iVarD == 1.0) then
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/310 1 the attack.lua", "Begin Scene")
    return
    end


--short get psychied up by talking to friends sequence
  fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  	    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Alonso\", " .. sDecisionScript .. ", \"Alonso\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Frederick\",  " .. sDecisionScript .. ", \"Frederick\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Ron\",  " .. sDecisionScript .. ", \"Ron\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Cohen\",  " .. sDecisionScript .. ", \"Cohen\") ")


--Alonso's advice:
elseif(sTopicString == "Alonso") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I imagine Alonso standing in front of me. It's a little indistinct and I can't quite recall his eyes though.)[BLOCK][CLEAR]") ]])
  --[ADDCHARPOS|Alonso|-1|Happy|Middle-HfBody]
  	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Alonso|-1|Neutral|Middle-Body-HfBody] He's smiling at me though.)[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 1.0) then
       fnInstruction([[ WD_SetProperty("Append", "Alonso: Nailed down a little something on the way huh?[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Irene: Stop looking so smug.[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Alonso: I won't be smug.[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Irene: No you will be smug.[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Narrator: I give him the Quadruple Deuce.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: If we could stay on subject please.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'd say you got yourself into a mess[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Sad] I'd say we are all in trouble, my heroics haven't changed that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Hey you made it by yourself, alive and unharmed. That's worth an epic.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:I always said that no adventure was worth our lives. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Any advice on doing this and staying alive?[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Together we can't be stopped. But on your own you don't stand much of chance.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Any plan that could work involves no fighting until everyone is rescued. So restrain yourself.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Hmm, No chance of getting away without everyone either.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] I'll keep that in mind, see you soon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Alonso|Neutral] One way or another [REMCHAR|Alonso] [BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()

  --Flag.
  VM_SetVar("Root/Variables/Characters/Scenario/iAlonsoAdvice", "N", 1.0)
  LM_ExecuteScript(LM_GetCallStack(0), "Advice")

--Frederick's advice:
elseif(sTopicString == "Frederick") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I visualize Frederick sitting in front of me, complete with a roguish smile. Can't quite remember what his hair looked like though.)[BLOCK][CLEAR]") ]])
  if(iAntIntimacy == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-HfBody] Frederick is looking incredibly self-satisfied.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: Got something to say Fredrick?.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] Well, you can't criticize me for poor impulse control now. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Angry] Oh?[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] From now on whenever you tell me not to steal something I'm going to remind you about how in the middle of a death march to rescue your friends you stopped to get with a girl.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] Go to hell Frederick. Now if I could get some help?[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] At least you have a lot of hands to work with. [BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|Frederick|-1|Neutral|Middle-Body-HfBody]  Nice look, very fitting for the forest. The extra hands seems useful.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: I wonder if I would be a better thief with another set of hands?[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] The hands are alright.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I'm not sure that they would help you that much. You are already pretty good.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: I'd be a triple threat, two pairs of hands and a mug like this.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: When we meet again I'll have to teach you a few tricks. Now let's get to the greatest heist someone like you is going to do.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Sad] I'm not sure anything will make a difference, I don't know how the hell I'm gonna get in there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] You are thinking too small. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Then what do you recommend?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: If I was here, the way things are now I couldn't get in there. Way too many places to be seen from.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: When things are like this we make a distraction.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Remember when I borrowed some things from that warehouse? I made sure the guards were wondering who broke the window across the street.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] I'll think on it Frederick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I look forward to my rescue no doubt.[REMCHAR|Frederick][BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()

  --Flag
  VM_SetVar("Root/Variables/Characters/Scenario/iFrederickAdvice", "N", 1.0)
  LM_ExecuteScript(LM_GetCallStack(0), "Advice")

--Ron's advice:
elseif(sTopicString == "Ron") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --[ADDCHARPOS|Ron|-1|Special|Middle-HfBody] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Ron|-1|Neutral|Middle-Body-HfBody] I imagine Ron looking nervous. This isn't hard, what's hard is remembering exactly how tall he was.)[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 0.0) then
       	fnInstruction([[ WD_SetProperty("Append", "Ron: Magic is always surprising, even to me. I wonder why the spider-girls transform people?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Probably to make something they couldn't use into something they could.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: No way to tell really, still what I'd give to know the magic involved[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] If we could focus on the problem at hand Ron.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Ron: I...[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Something bothering you Ron?.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: No![BLOCK][CLEAR]") ]])
    	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] Of course not. If we could focus on the problem at hand?[BLOCK][CLEAR]") ]])
end

  --[EMOTION|Ron|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]  Ron straightens a little, looking far more confident.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: You know what I'm going to say.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Well I definitely have an idea. But I'd like to hear you say it.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Ron|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: You know me as a wizard, which means that I either have a very specific spell to get what I want.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Ron:  Or I do the fun thing and just apply enough fire spells to get the job done.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: That approach hasn't failed me yet.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] You are captured.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Neutral] I'm confident that will be explained as not enough time to apply enough fire spells[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Fighting] Anyway, get creative, get destructive. The forest is too damp to really catch fire. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Come on, you know you want to.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Thank you Ron.[REMCHAR|Ron][BLOCK][CLEAR]") ]])
--SD: have ron vanish here
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] Friggin pyro.[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()

    
  --Flag
  VM_SetVar("Root/Variables/Characters/Scenario/iRonAdvice", "N", 1.0)
LM_ExecuteScript(LM_GetCallStack(0), "Advice")

--Cohen's advice:
elseif(sTopicString == "Cohen") then
--Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Cohen|-1|Neutral|Middle-Body-HfBody] I visualize Cohen standing in front of me, looking as audacious as ever. Was his beard always like that though?)[BLOCK][CLEAR]") ]])
  --[EMOTION|Cohen|Happy]
       if(iAntIntimacy == 0.0) then
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  He softens to a reassuring grin.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Don't worry too much about your changes youngling. I told you many stories of how I got hit with various polymorph spells.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Remember my story about getting turned into a little girl wearing a floofy dress? These things happen and I came out okay.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] I appreciate that. Any ideas on how to make the rescue work?[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Narrator:  He softens to a knowing smile.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Cohen: Ah I can not judge you for seeking comfort. Those with death in their path often find a another in their arms. Still it had it's effect on you.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Thought:(Can't deny that.)[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Just like you to lecture when I ask for help.[BLOCK][CLEAR]") ]])
fnInstruction([[ WD_SetProperty("Append", "Cohen: Then listen youngling.[BLOCK][CLEAR]") ]])
  end
  --[EMOTION|Cohen|Special]
	fnInstruction([[ WD_SetProperty("Append", "Cohen:  Don't give up. There is definitely a way, even if it seems unlikely.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Whatever you do, once it's begun move fast. Do your plan so fast that the spiders don't have a chance to realize it won't work.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Cohen: Take too long and you will be surrounded and caught.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Thanks Cohen, see you soon I hope.[REMCHAR|Cohen][BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  VM_SetVar("Root/Variables/Characters/Scenario/iCohenAdvice", "N", 1.0)
LM_ExecuteScript(LM_GetCallStack(0), "Advice")

--LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/310 1 the attack.lua", "Begin Scene")

  end