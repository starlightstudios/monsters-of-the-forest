--the attack sequence, featuring the cocoon and who gets to fight the queen choices
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
   --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
--It comes together:
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alright, any workable plan will have the following.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Me moving fast.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (No fighting until I have rescued at least Ron.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll need an excellent distraction.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (And finally a lot of shit getting burninated.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Oh, I just need to start a large fire as a distraction and move fast to spring the others. Still no over-thinking for a plan like this. No stopping me now that I got a plan.)[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Let's hope that convinces the spider girls.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] It takes a bit of doing but from across the clearing I spot a tree that's so dry rotted that it's being held up by the web around it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then I get to work making a torch.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I tear long rags from what remains of my clothes. To be fair at this point my modesty is a non-issue.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Still old habits die hard and I make sure I have at least some shorts left over. [EMOTION|Irene|Happy] Frederick probably will still get distracted. I also leave enough that I can be identified.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I soak these rags in sap from a freshly fallen tree.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It makes my hands unbearably sticky but I tie the rags around around a green hardwood stick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: It's pretty rough looking torch, but I know from experience it will burn bright and long.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I pause.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Once I light the torch the attack has begun.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (This is stupidly dangerous, I might be throwing away my life.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (But I don't plan to live forever.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm doing it for my friends and I'll take strength from that. )[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Now if my legs could just stop trembling, I'd be totally set.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alonso wouldn't be afraid, he'd give me an audacious look and then lead the charge.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] My legs stop trembling a little so I make a small smokeless fire and light the torch.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I sprint across the clearing faster than I thought I was capable of. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Fear and my new stronger legs let me move like the wind.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As I sprint an orange haired spider-girl flits into view above me. Her head starts to turn towards me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The terror is nameless, and I barely reach a tree to throw myself behind in time. [BLOCK][CLEAR]") ]])
  --need motion here
  --[EMOTION|Irene|Kneutral]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  I try to wipe my brow only to find that only half of my arms will move.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (Oh shit.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I've barely begun and I've already caught myself on a web.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  (Good thing I brought an emancipator)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Ksmirk] 
  fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Maybe Ron was right about fire having a hundred and one uses.)[BLOCK][CLEAR]") ]])
   if(iAntRescue == 1.0) then
     fnInstruction([[ WD_SetProperty("Append", "Narrator: Same as it did for Forma, the torch melts the web quickly.[BLOCK][CLEAR]") ]])
     end
--IF: $antrescue is 1(I use the same technique as to rescue forma, the web melts quickly)
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The torch singes my carapace a little but in no time at all I'm free.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Resolving not to let that happen again, I make my way carefully to the tree I spotted.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I'm the very imitation of Frederick as I sneak around.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The tree is massive, reaching far up into the canopy. It's also so old and rotten it crumbles when I pick at it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The yellowed webs around it are the only thing holding it up. It's going to burn like a match when set on fire.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy](No matter what happens this is going to be quite the show.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (As Ron loved to remind us the first rule of adventuring is that it's never wrong to set things on fire.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Angry](We disagreed often about if that was always true.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] The tree goes up like sawdust when I bring the torch to it's trunk. It's a sight for sore eyes. I almost giggle triumphantly.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral]I begin to carefully sneak my way deeper into the nest. It's arranged in a series of concentric rings.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Captives, human captives especially are most likely held in the deepest part of the nest. Standard rules for stashing treasure.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: At least if you believe Cohen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I am almost spotted once or twice, but most of the spiders are too busy looking worried to pay me much attention.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Finally I reach the inner sanctum, the trees are spare and cover is thin.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I can spot a trio of cocoons that look like the one I popped out of. One is notably thicker and covered in burn marks.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Well that's Ron's. He's consistent if nothing else.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Heck, I hope he didn't burn up all the air in there.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (He probably learned his lesson after the first time he knocked himself out like that.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (I have to rescue him first. Fire magic will make the distance and time I need to get the others.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I glance back. The burning tree is casting a flickering orange light throughout the nest.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The fire has reached the canopy, and looks set to spread to other trees. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I smile.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] Then abandoning caution I sprint the final distance to the cocoons.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KFighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Furrowing my brow I bring the torch close to Ron's cocoon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I sure hope I'm not cooking you alive in there Ron.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Irene:  Although that would probably count as being ironic. [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KFighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I look around, the spiders are in disarray and I haven't been spotted yet. It's a matter of time though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With all my arms I drive my small knife into the softest spot of the cocoon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I sure hope I didn't just skewer Ron)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm pretty sure that wouldn't be ironic.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KFighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a solid effort I manage to open a small gap. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I grasp the sides of the gap with all of my hands and then take a deep breath.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Irene|KAngry]
	fnInstruction([[ WD_SetProperty("Append", "Irene: Open![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: With a mighty renching noise I manage to force open the gap a bit more.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Out of ideas I try emitting a warm happy scent.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: That ought to get him out of there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Oh right.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Ron, wake up you are missing dinner again.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a muffled thud and I hear a quiet 'Ow'.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: To emphasize the point, I shake the cocoon and there is a slooshing noise. [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Ron:  I'm awake, wait what?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Ron get the hell out of there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Wait who are you?[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KNeutral]
	fnInstruction([[ WD_SetProperty("Append", "Irene: The person rescuing you, now get out of there. I can't break the cocoon open from the outside.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: On it. Don't stand too close.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I throw myself away in time to save my eyebrows. The heat from the gout of flame is uncomfortably warm.[BLOCK][CLEAR]") ]])
  --move away here
  --[ADDCHARPOS|Ron|0|KNeutral|Middle-HfBody][EMOTION|Irene|KHappy] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle+HfBody+Body|QUADINOUT][ADDCHARPOS|Ron|-1|Neutral|Middle-HfBody][EMOTION|Irene|Happy] Ron sits up out of the charred remains of the cocoon. He's been turned into a girl as well. [BLOCK][CLEAR]") ]])
  --add Ron here
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He takes a slow look around, blinking like he can't believe his eyes. Finally he speaks.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I'm sorry I don't think we have met?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] I'm rescuing you, now get moving. We need a distraction.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  A look of slow realization dawns on Ron's face as he examines my clothes.[BLOCK][CLEAR]") ]])
  	fnInstruction([[ WD_SetProperty("Append", "Ron: Spiders do that?[BLOCK][CLEAR]") ]])
    --[EMOTION|Irene|KAngry]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] And they turned you into a girl. Focus! We are gonna get captured again if you don't start making a distraction.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: A distraction? [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised] I feel terror rise in the back of my mind as I see Ron's smile.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|KFighting] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: That's something I can understand.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He gets up and starts lobbing fire into the nearest trees while cackling madly.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Offended](Damnit Ron.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Neutral] (Alright two cocoons, which to open first?)[BLOCK][CLEAR]") ]])
      --Left/right cocoon decision choice. It only gets called like twice later in this passage.
    --the biggest reason to have it is to increase the amount of content and keep the player engaged.
    --*AI: THere are variations for AI in each cocoon rescue path, these can probably be lifted fairly cleanly from twine.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Left Cocoon\", " .. sDecisionScript .. ", \"LeftCocoon\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Right Cocoon\",  " .. sDecisionScript .. ", \"RightCocoon\") ")

--right cocoon sequence
elseif(sTopicString == "RightCocoon") then
	WD_SetProperty("Hide")
	--Right cocoon sequence:
	--Reboot the dialogue.
  VM_SetVar("Root/Variables/Characters/Scenario/iOpenedLeftCocoon", "N", 0.0)
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Ron|Middle+HfBody+Body|QUADINOUT][MOVECHAR|Irene|Middle-HfBody|QUADINOUT] I scramble over to to the right cocoon. It's thankfully a lot thinner than Ron's[BLOCK][CLEAR]") ]])
  --need motion here, maybe have Ron emoting and turning the background
  --[EMOTION|Irene|KAngry] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] After affixing all four hands to the outside, I give a mighty grunt and tear a sizable hole.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Immediately a pair of hands reach out and try to pull me into the cocoon.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I put up my remaining hands in a calming gesture.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Calm down, this is a rescue spaz.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The hands relax and I see Frederick's face peak out of the cocoon cautiously.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He pops out of the cocoon a smile on his face. [BLOCK][CLEAR]") ]])
  --Add Ron here
  --[ADDCHARPOS|Frederick|-1|KSpecial|Middle-HfBody] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|Frederick|-1|Neutral|Middle+HfBody] Rescued by a mysterious ant-girl? Allow me to introduce you to the strange emotion called love.[BLOCK][CLEAR]") ]])
    --[EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Laugh] Seriously Frederick that's the line you lead with?[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KAngry] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It's a classic! Don't worry I got another good one.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Stop flirting Frederick. We don't need things to get worse. Now get out of there and help me open the last cocoon.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KHappy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Can I just say it's damn good to see you again?[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] It's good to see you too Frederick.[BLOCK][CLEAR]") ]])
--SD: have spider queen portrait show up
--[EMOTION|Frederick|KSad] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|SpiderC|-1|Neutral|LEdge+50] Oh hell, friend of yours?[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSpecial]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Surprised] Can't say I know her.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: A regal spider girl wearing an impressive crown and hefting a fearsome looking spear approaches at a relaxed pace.[BLOCK][CLEAR]") ]])
  --add spider queen actor here, if we have it.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her icy glare suggests she's ready to murder things until the problem stops.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac]  [EMOTION|Irene|Neutral] (Someone has to buy time, it's either gonna be me or Ron.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Frederick's not that good in a straight fight and is probably still disoriented.)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "FightDecision")
  
  
    
--LEFT COCOON SEQUENCE
elseif(sTopicString == "LeftCocoon") then
  VM_SetVar("Root/Variables/Characters/Scenario/iOpenedLeftCocoon", "N", 1.0)
	WD_SetProperty("Hide")
	--Right cocoon:
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Ron|Middle+HfBody+Body|QUADINOUT][MOVECHAR|Irene|Middle-HfBody|QUADINOUT] I scramble over to to the left cocoon. It's thankfully a lot thinner than Ron's[BLOCK][CLEAR]") ]])
  --need motion here, maybe have Ron emoting and turning the background
  --[EMOTION|Irene|KAngry] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] After affixing all four hands to the outside, I give a mighty grunt and tear a sizable hole.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: An eye immediately appears at the hole and then pulls back cautiously.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KHappy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I put up my remaining hands in a calming gesture.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Hello Alonso, this is a rescue.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: About time! Here help me tear open this damn thing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Together we force the cocoon open. Alonso's strong arms making quick work of the silk.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso sits up out of the cocoon watching me closely. [BLOCK][CLEAR]") ]])
  --Add Alonso here [ADDCHARPOS|Alonso|-1|KSad|Middle-HfBody]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody] I'm sorry have we met?[BLOCK][CLEAR]") ]])
    	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I'm the person who pulled you out of the half inch of water you were drowning in.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: OH! Let's not talk about that. Still good to see you.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: What happened? You look... Um different.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] So do you, so don't get smug Alonso. [BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Smirk] Not bad I hope! still not as different as you. You, Ron, where's frederick?[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Other cocoon let's go![BLOCK][CLEAR]") ]])
  --SD: have spider queen portrait show up
  fnInstruction([[ WD_SetProperty("Append", "Alonso: [ADDCHARPOS|SpiderC|-1|Neutral|LEdge+50][EMOTION|Irene|Surprised] I see you have made quite the impression on the locals.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene: Hey they started it.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: We can't leave Frederick behind, we'd never find him again.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: A regal spider girl wearing an impressive crown and hefting a fearsome looking spear approaches at a relaxed pace.[BLOCK][CLEAR]") ]])
  --add spider queen actor here, if we have it.
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her icy glare suggests she's ready to murder things until the problem stops.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Neutral](Someone has to buy time, it's either gonna be me or Ron.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Alonso has already found a rock but I don't give him good odds fresh out of the cocoon.)[BLOCK][CLEAR]") ]])
   fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "FightDecision")




  elseif(sTopicString == "FightDecision") then
	WD_SetProperty("Hide")
  fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Who should I send?)[BLOCK][CLEAR]") ]])
 fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
  local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Me\", " .. sDecisionScript .. ", \"YouFight\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Ron\", " .. sDecisionScript .. ", \"Ronfight\") ")




elseif(sTopicString == "Ronfight") then
local iOpenedLeftCocoon = VM_GetVar("Root/Variables/Characters/Scenario/iOpenedLeftCocoon", "N")
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  
  
  --who to send choice: remember an if statement for left/right cocoon.
--sent Ron to fight sequence:
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Fuck it, Ron can do it. I have no weapons and no armor besides from the stuff I've grown. A second pair of arms is handy but not going to cut it.)[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Ron may not be a great fighter but he's good at keeping enemies at bay.)[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Besides I think I've more than run up the score at this point.)[BLOCK][CLEAR]") ]])
     
     --left cocoon variation
     
     if(iOpenedLeftCocoon == 0.0) then
      fnInstruction([[ WD_SetProperty("Append", "Irene: Ron quit laughing and cover us! we have to get Alonso![BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Ron: Hehehe! [BLOCK][CLEAR]") ]])
      --note to self: focus camera on duo and maybe move ron and spiderC to the left.
      fnInstruction([[ WD_SetProperty("Append", "Frederick:[REMCHAR|Ron][REMCHAR|SpiderC] Good to see Ron's enjoying himself. Even if it scares me a little.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: If Ron's performance unnerves the spiderqueen she doesn't show it.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle+HfBody|QUADINOUT][MOVECHAR|Frederick|Middle-HfBody-Body|QUADINOUT] Frederick and I move as fast as we can to remaining cocoon. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: With a satisfied smile Ron produces a hunting knife. I couldn't guess where the hell he was hiding that.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The remaining cocoon isn't hard to open but it takes a minute to hack away at the resilent silk. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: I want to ask what the heck happened to you but at the same time I want to hear the story told right. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: I'm not gonna lie I was pretty bad ass.[BLOCK][CLEAR]") ]])
         if(iAntIntimacy == 1.0) then
fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll save the juicy part until I have more time. Also have a better excuse.)[BLOCK][CLEAR]") ]])
end
      fnInstruction([[ WD_SetProperty("Append", "Frederick: I don't doubt it, you look pretty ravishing as an ant-girl by the way.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: Wait really?[BLOCK][CLEAR]") ]])
              if(iAntRescue == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Frederick: You got a really cute face.[BLOCK][CLEAR]") ]])
       fnInstruction([[ WD_SetProperty("Append", "Irene: Just wait until you meet a real ant-girl.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Wait you met one? Are they super cute?[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Irene: Hello Alonso.[BLOCK][CLEAR]") ]])          
else
      fnInstruction([[ WD_SetProperty("Append", "Frederick: You got a really cute face. Oh hello Alonso.[BLOCK][CLEAR]") ]])
end
      fnInstruction([[ WD_SetProperty("Append", "Alonso: [ADDCHARPOS|Alonso|-1|Neutral|Middle-HfBody] The hell. Okay deep breaths[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm being rescued by Frederick and an Ant-girl[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: It's me doofus.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm sorry I'm not familar.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: I was there when you got your head stuck between the legs of a horse.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: I remember that.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: Wow you look different. Anyway what's the plan?[BLOCK][CLEAR]") ]])
      end
      --end left cocoon variation
      
      --right cocoon variation
       if(iOpenedLeftCocoon == 1.0) then
      fnInstruction([[ WD_SetProperty("Append", "Irene: Ron quit laughing and cover us! We have to get Frederick![BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Ron: Hehehe! [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: [REMCHAR|Ron][REMCHAR|SpiderC] Good thing he is on our side. Be bloody terrifying if he wasn't.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: If Ron's performance unnerves the spiderqueen she doesn't show it.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|Middle+HfBody|QUADINOUT][MOVECHAR|Alonso|Middle-HfBody-Body|QUADINOUT] Alonso and I move as fast as we can to remaining cocoon. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: We hammer my knife into the cocoon with Alonso's rock. Thankfully my new chitin keeps it from smarting too much.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Once the knife is in there, we begin to tear at the cocoon. Even with two people it's slow going.[BLOCK][CLEAR]") ]])
         if(sGender == "Boy") then
            fnInstruction([[ WD_SetProperty("Append", "Alonso: I gotta ask. So the spiders turned us into women, but how are you an ant-girl?[BLOCK][CLEAR]") ]])
else
   fnInstruction([[ WD_SetProperty("Append", "Alonso: I gotta ask. So the spiders turned Ron and I into women, but how are you an ant-girl?[BLOCK][CLEAR]") ]])
end
              if(iAntIntimacy == 0.0) then
              fnInstruction([[ WD_SetProperty("Append", "Irene: You gotta watch what you eat, it seems some stuff has transformative effects.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: Huh, that gives a lot of things to try actually. Still it's good to know you are alright.[BLOCK][CLEAR]") ]])
else
     fnInstruction([[ WD_SetProperty("Append", "Irene: You gotta watch what you eat, it seems some stuff has transformative effects.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Blush] Also don't smooch any ant-girls. Or more intimate versions of smooching.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso loses track of what he's doing. Sitting back as he starts to howl with laughter.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Alonso: heheh hhahah! I'm sorry. I can't.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Alonso: Excuse me.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Alonso: You what? I'm sorry. I'm glad you are safe.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Narrator: He giggles a bit more.[BLOCK][CLEAR]") ]])
     fnInstruction([[ WD_SetProperty("Append", "Alonso: You just never seemed the type. I always figured that would be Frederick. Okay, let's get him and lets get out of here.[BLOCK][CLEAR]") ]])
end
      fnInstruction([[ WD_SetProperty("Append", "Alonso: You look good as an ant-girl.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Blush] Ah, umm. You look fine as a girl.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: I have a great figure, don't know where that came form. Oh hello frederick.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: [ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody][EMOTION|Irene|Smirk] Being rescued by a blonde beauty and an ant-girl. This dream is shaping up to be pretty good already.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: Heh, if you hadn't become a chick yourself Frederick.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Neutral] Oh, well I'm open to trying new things with friends. So what's the plan?[BLOCK][CLEAR]") ]])
      end
      --end right cocoon variation
      
      
      
      fnInstruction([[ WD_SetProperty("Append", "Irene: Plan?[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: Wait you hear that?[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: ..... [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: Oh crap, Ron's not laughing.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|SpiderC|-1|Neutral|LEdge+body] Everyone turns at once. The spider queen is standing over Ron who's sprawled out on the ground.[MOVECHAR|Irene|REdge-body-body|Teleport][MOVECHAR|Alonso|REdge-body|Teleport][MOVECHAR|Frederick|REdge-HfBody|Teleport][BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Ron's not invincible, if you can dodge his fires and close the distance you really just need to bop him pretty hard to take him out of the fight.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Everyone moves at once.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|LEdge+body+Hfbody|QUADINOUT][MOVECHAR|Alonso|LEdge+body+body|QUADINOUT][MOVECHAR|Frederick|LEdge+body+Body+Hfbody|QUADINOUT] Alonso and I rush forward taking the spider-queen surprise. We tackle the spiderqueen's forward legs trying to force her back.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Frederick|LEdge+body+Body+Hfbody+body+Body|QUADINOUT] Frederick drags Ron, back and gets him back to his feet as fast as possible.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Ron|-1|Neutral|LEdge+body+Body+Hfbody+Body] Ron's a bit worse for wear, but he knows the danger. He starts to chant magic as soon as he can.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Hearing that Alonso and I rush back to Ron and Frederick.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: The spiderqueen quickly realizes she's back in range of Ron's fire.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|SpiderC|LEdge+Hfbody|QUADINOUT] She takes in her burning nest and then shoots me a hate filled look. She heads into her nest barking orders.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|SpiderC] Despite everything Ron manages to look smug.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Ron: I'm alright, just a little winded. So what's the plan now? [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: I was just asking our friend that.[BLOCK][CLEAR]") ]])
      fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Ending")
      --jump to: Uhhh. You see the plan only really extended to rescuing you guys. I didn't think much further than that
     
      
--sent player to fight sequence:
elseif(sTopicString == "YouFight") then
local iOpenedLeftCocoon = VM_GetVar("Root/Variables/Characters/Scenario/iOpenedLeftCocoon", "N")
VM_SetVar("Root/Variables/Characters/Scenario/iYouFight", "N", 1.0)
WD_SetProperty("Hide")
	--Right cocoon sequence:
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (It should probably be me. I started things after all, and I'm the most familiar with my current form.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Just another great story to tell around the campfire I guess)[BLOCK][CLEAR]") ]])
  -- [EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Assuming I live that long.)[BLOCK][CLEAR]") ]])
  --left cocoon (frederick)
  if(iOpenedLeftCocoon == 0.0) then
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Frederick open the last cocoon. I'll distract our new friend over there.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  Won't take a moment! And no showing off, you are making everyone look bad as it is.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Happy] I can't help but smile, which is nice because the situation isn't good.[REMCHAR|Frederick][REMCHAR|Ron][BLOCK][CLEAR][REMCHAR|Alonso]") ]])
  end
  --end left cocoon variation
  if(iOpenedLeftCocoon == 1.0) then
  fnInstruction([[ WD_SetProperty("Append", "Irene: Alonso, open the last cocoon. I'll buy time![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Be careful! And give her one for me![BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk] I smile, this is the opposite of careful and it doesn't look good. [REMCHAR|Alonso][REMCHAR|Frederick][REMCHAR|Ron][BLOCK][CLEAR]") ]])
  end
    --have the camera "move" here to focus on the spider queen, have Isaac [EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [MOVECHAR|Irene|REdge-body|QUADINOUT][MOVECHAR|SpiderC|LEdge+Body|QUADINOUT] [EMOTION|Irene|Neutral] I have a small knife, and the strength in my new arms and legs.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She is a spider queen holding a spear as large as I am. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: These are bad odds.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I either need to disarm her and hope she backs off or delay her for a minute or two.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Anything except for getting a spear in my gut really.)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|REdge-body-body|QUADINOUT][MOVECHAR|SpiderC|LEdge+body+body|QUADINOUT] She closes the distance in no time at all. Then she quickly sweeps her spear.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KFighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Offended] I duck under this and keep moving as fast as I can towards her[BLOCK][CLEAR]") ]])
  --maybe swap to kneel, move forward swap to standing fighting? Might be better to black screen this part.
  --[EMOTION|Irene|Fighting] 
	fnInstruction([[ WD_SetProperty("Append", "Narrator: If I can just get close I might get inside the range of her spear. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: As she stabs, I dash to her right, getting under her. [MOVECHAR|Irene|Middle|QUADINOUT][MOVECHAR|SpiderC|Middle|QUADINOUT] [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Against something this much bigger than me, the only safe place is where she can't reach or step on me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I spend a few seconds under her, her shiny under carriage turning this way and that as she tries to see me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle+Hfbody|QUADINOUT][MOVECHAR|SpiderC|Middle-HfBody|QUADINOUT]  The illusion of safety is shattered as she jumps back, catching me with a solid kick as she does.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The world spins as I try to keep my eyes on her. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle+body|TELEPORT] She tries to sweep my legs with her spear, I jump back just in time.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Now she has the distance to try and stab me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She stabs and I recognize it's a feint fast enough to grab the spear.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] I hold on with all four hands and dig in my heels.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle|QUADINOUT][MOVECHAR|SpiderC|Middle-Body|QUADINOUT] The spider queen pulls back and for a moment a desperate tug of war ensues. She comes close to just lifting me into the air.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: An ant-girl is simply smaller than a spider.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Fuck I'm loosing. Gotta think.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (AH!)[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Fighting]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Smirk]  I let go of the spear with one arm, and then reach out and punch her in the box.[BLOCK][CLEAR]") ]])
	--fnInstruction([[ WD_SetProperty("Append", "Spiderqueen: ...nh[BLOCK][CLEAR]") ]])
  --change this to a spiderqueen line later.
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider queen lets out a pained noise.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|Irene|Middle+HfBody|QUADINOUT][MOVECHAR|SpiderC|Middle-HFBody|QUADINOUT] She loosens her grip long enough for me to wrench it away from her. Desperate I throw it away.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-queen looks at me with an expression of surprise.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Surprised] The spider-queen speaks with a voice that sounds like my imminent death. 'Well done little queenling'[BLOCK][CLEAR]") ]])
	--fnInstruction([[ WD_SetProperty("Append", "Spiderqueen: Well done little queenling.[BLOCK][CLEAR]") ]])
	--fnInstruction([[ WD_SetProperty("Append", "Narrator:  Her voice sounds like my imminent death.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[MOVECHAR|SpiderC|Middle|QUADINOUT] My confusion is punctuated by her punching me in the face and then strangling me with both hands.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Her grip is like a vice and it hurts oh gods it hurts. I am dimly aware of her lifting me into the air, my feet dangling helplessly.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Angry] I struggle with all four hands to loosen her grip, and try to swing my legs to hit her face. [BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  It's hopeless.[BLOCK][CLEAR]") ]])
  --go to black screen here [EMOTION|Irene|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Surprised] The chitin on my neck starts to crack and my vision begins to go red around the edges. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Then a gout of flame roars in front of me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: The spider-queen drops me and I land on my abdomen, sprawling out like a sack of potatoes.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|KSad]
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [REMCHAR|Irene] Air has rarely tasted so good.[BLOCK][CLEAR]") ]])
  --move spider queen back here
	fnInstruction([[ WD_SetProperty("Append", "Narrator:[REMCHAR|SpiderC] The spider-queen retreats rather than get charred.[BLOCK][CLEAR]") ]])
  --if opened frederick's cocoon (left cocoon)
  if(iOpenedLeftCocoon == 0.0) then
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I look up to see Alonso offering me a hand.[BLOCK][CLEAR]") ]])
  --add party members here
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Alonso|-1|Neutral|Middle-HfBody][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle+Body+HfBody] What have we told you about trying to take on the big ones by yourself? Not even you are that tough. You just can't help yourself can you?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Cough, hccck. I had her.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You were about to pass out.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] All part of the plan.[BLOCK][CLEAR]") ]])
  end
  --end left cocoon variation
  --right cocoon (alonso)
  if(iOpenedLeftCocoon == 1.0) then
  fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Irene|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody+Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody] I look up to see Frederick offering me a hand.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: Ha! What have I told you about trying to show off? Always trying to make us look bad.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:  [EMOTION|Irene|Happy] I think you managed that on your own Frederick.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: Cut it out you two! We don't have much time.[BLOCK][CLEAR]") ]])
 end
 --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Neutral] Frederick, Ron group up. Our newly anted companion is about to lead us out of here.[BLOCK][CLEAR]") ]])
  --Alonso turns back here
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Alonso|Neutral] You do know the way out of here right?[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Ending")

  
  
  elseif(sTopicString == "Ending") then
	WD_SetProperty("Hide")
	--Right cocoon sequence:
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
  fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Sad] Uhhh. You see the plan only really extended to rescuing you guys. I didn't think much further than that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] So you sprung us without a plan for escaping the forest? Generally prison breaks are at least two step processes.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] I sprung you didn't I? Hell I didn't think I'd get this far.[BLOCK][CLEAR]") ]])
--SD: Everyone should be staring at MC at this point.
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] Nothing is good enough for you guys.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [MOVECHAR|Irene|Middle-body-Hfbody|QUADINOUT][MOVECHAR|Alonso|Middle+Hfbody+body|QUADINOUT][MOVECHAR|Frederick|Middle+Hfbody|QUADINOUT][MOVECHAR|Ron|Middle-Hfbody|QUADINOUT] [EMOTION|Irene|Neutral]  Well follow me then, any direction has to be better than here.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy]
  fnInstruction([[ WD_SetProperty("Append", "Ron: I'm having a good time. Thanks for the rescue.[BLOCK][CLEAR]") ]])
--SD: if I get the pained face put it here
--[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] No problem Ron.[BLOCK][CLEAR]") ]])

fnCutsceneBlocker()
LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/320 1 escape part 1.lua", "Begin Scene")
  end
