--[Talk to Alonso]
--Subscript to handle talking to Alonso.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Begin]
--Start of the conversation.
if(sTopicString == "Begin") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [ADDCHARPOS|Irene|-1|Neutral|Middle-Body][ADDCHARPOS|Alonso|-1|Neutral|Middle+Body][ADDCHARPOS|Guard|-1|Neutral|Middle+Body+Body]Alonso is trying and failing to impress one of the caravan guards with tales of his exploits.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: She is humoring him although he does get a few laughs from all the times things went horribly wrong.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I supress the urge to mention the time he nearly drowned in three inches of water as I approach.[MOVECHAR|Alonso|Middle|QUADINOUT][REMCHAR|Guard] He turns to me with a smile.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Alonso|Happy] Ah, my friend. What's up?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[EMOTION|Irene|Smirk][VOICE|Irene] (Alonso has been a natural leader even since you knew him as a kid. He always tries to be diplomatic, and isn't half bad at it. He's helped by being good in a fight for when the other side is hard to convince.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (He's the second son of a blacksmith and also in charge of making sure everyone's weapon is kept properly.  [EMOTION|Irene|Happy] Rust and dullness are the real villains.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:  [EMOTION|Irene|Neutral] Not much. Just checking in.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: He gets a self-depreciating smile and takes me aside.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Laugh] I'm not having much luck here I'll tell you. I wanted to talk to you about something.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Smirk] Once this caravan gig is over, we should do something else. Let's try our luck in a dungeon, an abandoned keep or something. Go on a real adventure like Cohen is always talking about.[BLOCK]") ]])
    
    --Decision sequence.
    fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"I don't know...\", " .. sDecisionScript .. ", \"Nope\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Sounds fun!\",  " .. sDecisionScript .. ", \"Worry\") ")
    fnCutsceneBlocker()

--Not a good idea.
elseif(sTopicString == "Nope") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (I don't know, some of the horrors you hear about in dungeons are enough to make your blood run cold.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Sad] Maybe Alonso, I don't love the guard life, but you hear tales of slime monsters and men bewitched by foul creatures in dungeons.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Alonso is unfazed by my hesitation. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm sure we could take them together. Just think of the riches and the tales to tell![BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We will talk it out with the rest of the group.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: And here I thought I could get your vote now.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Quite the politician aren't you?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Alonso|Neutral] Alonso grows serious.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Do be careful, we will fall apart with you to do the fighting.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Likewise Alonso, we aren't much without as leader.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: I give him a nod and go back to my preparations.[REMCHAR|Irene][REMCHAR|Alonso][BLOCK][CLEAR]") ]])
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")

--Worry-wart.
elseif(sTopicString == "Worry") then
	WD_SetProperty("Hide")
    
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Irene] (High risk, high reward? I didn't become an adventurer for the nonexistent pay and rare warm meal.)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] I'm down, sounds like a nice change of pace.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Narrator:  Alonso is noticeably buoyed by my agreement.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Alright, I'll count on your vote when we discuss what to do once this gig is over.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: So long as we stick together we can't be stopped.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Alonso|Neutral] Alonso grows serious.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Do be careful, we will fall apart with you to do the fighting.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Smirk] Likewise Alonso, we aren't much without as leader.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: We part with a fist bump as I resume my preparations.[REMCHAR|Irene][REMCHAR|Alonso][BLOCK][CLEAR]") ]])

--Both: 
    
    --Rejoin the primary script.
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/100 Scene.lua", "After Discussion")
    
end
