--endings, mild variation based on antintimacy and player gender
--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)

--[Main Scene]
--Previous scripts rejoin here.

if(sTopicString == "Begin Scene") then
  	
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    
    --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.

    --Decision sequence.
     WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] Still probably best to make up my mind now.[REMCHAR|Cohen][BLOCK][CLEAR]") ]])
       if(sGender == "Boy") then
           fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Better to remain an ant-girl\", " .. sDecisionScript .. ", \"Antgirl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Being a girl wouldn't be so bad\",  " .. sDecisionScript .. ", \"Woman\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"To return to the way I was\",  " .. sDecisionScript .. ", \"Man\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Joke ending!\",  " .. sDecisionScript .. ", \"Joke\") ")
else
      fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Better to remain an ant-girl\", " .. sDecisionScript .. ", \"Antgirl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Being a man wouldn't be so bad\",  " .. sDecisionScript .. ", \"Woman\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"To return to the way I was\",  " .. sDecisionScript .. ", \"Man\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Joke ending!\",  " .. sDecisionScript .. ", \"Joke\") ")
end

    --[=[fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Better to remain an ant-girl\", " .. sDecisionScript .. ", \"Antgirl\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"Being a girl wouldn't be so bad\",  " .. sDecisionScript .. ", \"Woman\") ")
    fnInstruction(" WD_SetProperty(\"Add Decision\", \"To return to the way I was\",  " .. sDecisionScript .. ", \"Man\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Joke ending!\",  " .. sDecisionScript .. ", \"Joke\") ")
]=]
--swap these based on player's original gender (I am not looking forward to that.)


elseif(sTopicString == "Antgirl") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
   fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])

--To stay an ant-girl would be best
--To aim for womanhood
--To get back to the state I worked so hard for
--Alternative ending

--Note to self, these are done in roundtable style so almost no (NA) Or (TH)

--Ant-girl ending, dance party optional:
	fnInstruction([[ WD_SetProperty("Append", "Irene: [BACKGROUND|Null] [MOVECHAR|Irene|Middle-HfBody|Teleport][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I decided privately then and there I would stay as I was. I think anxiety kept me from saying it out loud. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Cohen said that he had an old adventuring friend who could help us. And that she was 'A bit of a trip away'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: He mentioned that she was a witch. Which I was excited for.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Frederick|Angry] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: What he didn't mention was that 'a bit of a trip' meant the other side of the continent. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] And through the thickest swamp we had ever seen.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: This lucky bugger's chitin kept them safe from bugs. I swear they were as large as kittens.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special]
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] They just aimed for where the chitin wasn't.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We rolled into a small town on the edge of the truly wild swamp. They told us of an evil witch living deep in the woods.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] Needless to say that was who were here to see. 'Evil' witches tend to have the best magic in books.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: The swamp was suck on earth.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] Of course you would be excited, Ron. Deep within the swamp we found a crude hut at the edge of a large shallow lake.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: As we approached the hut a woman stepped out. She looked like a witch from tales, complete with a black large brimmed hat.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: She was also the most beautiful woman any of us had ever seen.[BLOCK][CLEAR]") ]])
   --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Her hair[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Ron:  Her eyes[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Her um... personality.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: In that moment it was obvious that to gain her favor I'd have to kill the others.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It was regrettable but the only way. It was the sort of feeling you can't explain afterwards. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Frederick|Neutral][EMOTION|Alonso|Neutral][EMOTION|Irene|Neutral] Thankfully Cohen called out, he said 'Myrtle turn down your glamour. They are about to knife eachother to worship the ground you are on.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Ron|Neutral] When her glamour dropped, she was still attractive but no longer a goddess on earth. Magic like that is powerful stuff.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I remember you had a look of awe on your face Ron.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: It was a piece of magic far beyond anything I can do.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: She asked if we were Cohen's apprentices.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: He smiled and said we had a bit of a 'misadventure' and needed help getting untransformed.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy] I remember she said that any adventure with Cohen was a misadventure.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Frederick|Neutral][EMOTION|Irene|Neutral] The hut was worse than the house I grew up in. The witch was barely above destitution.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: There were three kids who stared at us wide eyed. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You in particular Isaac. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We greeted the kids, the oldest one was sucking his thumb. He stopped sucking his thumb long enough to say 'Hello I'm five'. Then he went back to sucking his thumb.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso:  So I said 'Well hello five, I'm Alonso.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The boy didn't know what to make of that. He looked childishly frustrated and said 'Alonso is a boy's name.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: So I said 'It sure is' Then I started bouncing the youngest one on my knee.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: They were good kids.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We played with the other two kids as Cohen and Myrtle talked.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special] 
	fnInstruction([[ WD_SetProperty("Append", "Irene: I remember feeling intensely curious to see children. It tugged at some part of my brain. I wondered if I could ever have children. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I've poked enough ant nests to know only the queen has children. Sadly I couldn't ask an ant-girl even if I had one handy.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Irene|Neutral] Cohen looked distantly sad as he talked to Myrtle. 'Your grandchildren myrtle?'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Myrtle sat down with some difficulty. 'My daughter died, and my son-in-law ran away. I'm only getting older.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: She didn't look old enough to be a grandmother. Cohen nodded sadly. 'I'm so sorry Myrtle'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Myrtle nodded sadly. 'Only thing to do is live, but you know that better than anyone. Come close little ant, I need to see if you can be turned back.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I moved close and she began to poke and prod my antenna, I turned to see what she was doing and was taken aback.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Surprised] She looked at least 80. Obviously quite the beauty in her youth, but now it was an illusion to conceal her age. Also turns out antenna hurt when pulled on.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] Cackling like a witch she told me that 'Age catches up with all of us.' She released me and appeared young again. 'I'll need some special ingredients to brew the potion.'[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Ron: It was the most complicated potion I had ever heard of. The ingredients were all esoteric and needed to be harvested in just the right way.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: The process of gathering the ingredients took weeks. Still it was fun questing. [BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy] 
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We had free time while trying to find the ingredients. Which I didn't mind that much. I spent most of it taking care of the kids.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso:  I spent part of the time helping with the kids, the rest was spent building furniture and improving the hut.  [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You were better with the kids than I was.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Special]
	fnInstruction([[ WD_SetProperty("Append", "Alonso:  Just something I had practice with Frederick. My brother needed a lot of care growing up.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special] 
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I spent most of the time building a garden and tending to the fields.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I dug some drainage trenches for the field, cleared the nearby trees so the crops get sunlight, planted several kinds of crops and taught the children how to tend to the plants properly.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: [EMOTION|Frederick|Neutral] You really went hard on those fields.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Feeling an urge to return to agriculture? I never thought you the type.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] As if. But I was especially handy with four arms and soil was soft.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] I remember you spent quite a lot of time with Myrtle, Ron.[BLOCK][CLEAR]") ]])
    --[EMOTION|Ron|Special]
    fnInstruction([[ WD_SetProperty("Append", "Ron: She was teaching me magic. She was a great witch, possibly only second to the grand witch.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: I always thought it was weird she agreed with you whenever you said that. You spent most of your time together making potions as I recall.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: I didn't think I'd like it but yes potions can be interesting. Not as useful as fire though.[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Happy] 
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Are you sure there wasn't another reason?[BLOCK][CLEAR]") ]])
    --[EMOTION|Ron|Angry]
    fnInstruction([[ WD_SetProperty("Append", "Ron:  No... just the learning.[BLOCK][CLEAR]") ]])
    --[EMOTION|Alonso|Special]
    fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Ron|Neutral] We spent the time not building up the homestead trying to find the ingredients.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Alonso|Neutral] To make the potion work Myrtle needed ingredients harvested in a certain way.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I remember how you stole a pinon feather from a sleeping roc. I'm still amazed you didn't wet yourself. I would of.[BLOCK][CLEAR]") ]])
    --[EMOTION|Frederick|Special]
    fnInstruction([[ WD_SetProperty("Append", "Frederick:  I am much better at thieving than fighting. I am an ice cold thief.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: The last ingredient was probably the easiest.[BLOCK][CLEAR]") ]])
    --[EMOTION|Irene|Special]
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Frederick|Neutral] Says you.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Myrtle's blackened caludron was warming when she said all that was left was a live catfish. Problem was the only catfish left was in her words 'A big ugly fucker that has learned not to bite at hooks.'[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Frederick: Which really left only one option.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] I assumed the only option was some combination of nets, magic, and spear fishing.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: The only solution was to have you noodle for it. Use your hand as bait and then just grab it by the jaw when it bit you.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Happy]
    fnInstruction([[ WD_SetProperty("Append", "Alonso:  I remember how you insisted so adamantly that there was absolutely no way you were going to go noodling for a giant catfish.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Happy]
    fnInstruction([[ WD_SetProperty("Append", "Ron:  I remember how about 15 minutes after that they were standing nude in the water up to the waist. [BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick:  Cursing at us constantly.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Look I love you all like family. But why is it always me? 'Hey Isaac, check if this potion is poison. Hey Isaac, check if this armor is thick enough. Hey Isaac, has the meat gone sour?'[BLOCK][CLEAR]") ]])
else
        fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Angry] Look I love you all like family. But why is it always me? 'Hey Irene, check if this potion is poison. Hey Irene, check if this armor is thick enough. Hey Irene, has the meat gone sour?'[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Because you can take it better than we can.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I guess.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You are the toughest of us, it's just a fact.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Ron: Way tougher than me. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Offended] Ugh, if that's how I'm helping.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The fucker's hole was beneath a sunken log. The water wasn't that bad, thigh deep and not very cold. Thankfully my chitin meant I was mostly safe from leeches.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: After putting my hand in the water I wiggled my fingers enticingly.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I felt like a fool but before long I was rewarded with a stabbing pain.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: It was the biggest catfish I'd ever seen, possibly a dire catfish. Although that's pretty terrifying to contemplate.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Frederick: It fought Isaac all the way to the shore. Ron and I tried to help but the catfish was quite slick and before long we were all muddy and soaked.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Frederick: It fought Irene all the way to the shore. Ron and I tried to help but the catfish was quite slick and before long we were all muddy and soaked.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Irene: I remember you were in charge of bringing up the tub. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Hey I got soaked when you guys hefted it in there. Besides we had to keep it wet remember?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The fucker's fangs pierced my chitin.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Myrtle chanted as she cut off the tail and then pitched the catfish into the bubbling cauldron. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We had fried fish while the potion boiled down. Good stuff. Myrtle's also healed our fisherman's hand.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: The potion was a dark evil substance that smelled like death. I tasted a bit that landed on knuckles though, it was delicious.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] Myrtle gave me a knowing look and then asked me 'So are you going to drink the potion dearie?'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] She caught me off guard. So I sputtered before telling the truth, I wanted to try out being like this for a while.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You didn't need to be afraid. [BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Special] 
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Frederick: I smiled and said 'Well, no problem! I'll hold off on my potion for a while. Girls gotta stick together. Let's walk on the wild side!'[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Frederick: I smiled and said 'Well, no problem! I'll hold off on my potion for a while. Girls gotta stick together. Your friend wants to stand out. The least we can do is give them a lot to look at'[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I wasn't that surprised, I never saw so natural a transformation. Not that I've seen that many transformations, but you definitely took to being an ant.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Still I wanted to be sure. 'Are you sure you want to try this?'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I nodded more than little put on the spot. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You seemed so certain 'Well then I'll try it with you.'[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Ron: I said 'It seems important to you, so I'll wait. But I'm not going to be a girl permanently.'[BLOCK][CLEAR]") ]])
--NOTE TO SELF: Probably another trans moment I'm not nailing
--[EMOTION|Ron|Special]
	fnInstruction([[ WD_SetProperty("Append", "Ron: The potions themselves were masterpieces.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Yes because that's what's important in this part of the story Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] [EMOTION|Frederick|Neutral] We tarried a while with Myrtle. I really enjoyed being with those kids.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: However, the call of adventure is strong. We were all getting restless, especially Cohen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: He was pacing furrows into the floor.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Finally it came time to leave. Cohen presented Myrtle with an overflowing bag of coins. 'This should cover the cost of the potion.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] Myrtle shook her head, trying to refuse. 'You have sent far too much money as is'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Cohen closed Myrtle's hands around the bag. 'It's nothing. We have to take care of eachother.'[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso: I hope that when I'm as old as they were I have friends that good.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] Myrtle waved us goodbye. 'Good bye boys. The potion won't go bad. Little Queenling, if you do start a hive send me some of that white waxy stuff, it's good for potions. And waterproof containers.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: That was a surprise to me. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] As we exited the swamp Cohen turned to us and said 'Remember always younglings, there is no old-age pension for adventurers.'[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We all started to sock away more treasure after that.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Frederick|Neutral][EMOTION|Ron|Neutral][EMOTION|Alonso|Neutral][EMOTION|Irene|Neutral] I found I enjoyed being a woman. I would never fit in with respectable women, but I'm an adventurer so being unrespectable is in the job description.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Frederick|Neutral][EMOTION|Ron|Neutral][EMOTION|Alonso|Neutral][EMOTION|Irene|Neutral] I found I enjoyed being an ant-girl. I would never fit in with respectable ladies, but I'm an adventurer so being unrespectable is in the job description.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Sad] It had been obvious earlier as we traveled to see Myrtle but an ant-girl always stands out amongst humans. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I could I suppose have attempted to blend in. All it would of taken was a hat for the antenna, tinted glasses for my eyes. Layers for my arms and legs, and probably a hoop skirt for my abdomen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Happy]But as I enjoyed being an ant-girl I didn't bother. My friends deflected attention whenever the stares got too burdensome.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] My chitin was the lightest and strongest armor I could hope for. Stronger arms and legs are always useful.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Even if I am a little shorter than when I started, I think it's been well proved that I'm as fearsome as ever.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Happy] It took a while but I finally managed to use the scents to guess at emotions reliably. This helped my situation awareness in combat immensely.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] Also cheat at cards. To Frederick's eternal consternation.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You bastard.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] [EMOTION|Frederick|Neutral] In time I drank the potion and returned to being a dude.[BLOCK][CLEAR]") ]])
  --Change Ron's sprite here
  --[EMOTION|Frederick|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: But you looked so fetching in your witch outfit.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Ron: STOP TALKING. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Ron|Neutral] I didn't bother drinking the potion. In the end male or female was just a way of viewing the world to me. More important were the adventures to have.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I didn't drink the potion either. These two need someone to show them how to really work being a girl. Besides I enjoyed myself as a girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We eventually became an established group of adventurers.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: At least we didn't nearly die or almost get captured so often.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Sad][EMOTION|Frederick|Sad][EMOTION|Irene|Sad][EMOTION|Alonso|Sad]
	fnInstruction([[ WD_SetProperty("Append", "Ron:  Cohen took us aside and said that he was proud of us, and that it was time for him to move on.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It was his way, gods we missed him though...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We didn't worry too much about Cohen's safety though. Remember when he killed six zombies in a minute?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Hell, I still see it when I sleep sometimes.[BLOCK][CLEAR]") ]])
  -- [EMOTION|Ron|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Ron: He said he was proud of us and I always carried that with me.[BLOCK][CLEAR]") ]])
  --[EMOTION|Frederick|Happy][EMOTION|Alonso|Happy]
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Happy] In time we became famous throughout the land. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I personally often got credited as a bad influence on girls everywhere. I took every opportunity I could find to encourage them to go on adventures.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: After all if something as humble as an ant-girl can be adventurer, why not a human-girl?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: My single handed rescue in the face of impossible odds became part of the poetic edda. I eventually became a mythologized hero, someone who gave up their humanity for their friends.[BLOCK][CLEAR]") ]])    
      if(iAntIntimacy == 1.0) then
       	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Blush] The story of my dalliance with Forma made it in. Although to my annoyance it was always presented as more a temptation than anything else.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] In time the story got a lot more hardcore than it ever was.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'm almost certain you didn't fight a battalion of spiders with every step.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] Burst my bubble why don't you. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] The exaggeration keeps the bards fed so we don't do too much to stop them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Our finest moment comes when we robbed the tombs of Aten.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: The tombs were huge, ancient and mostly empty.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: They had been used to hold some ancient evil, which had long since been banished.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It's only real guardians were it's depths, traps and occasional fits of madness.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: To these I found myself well adapted. I could leave scents behind as marks and see well in the dark. Also no tolerance for nonsense.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We came out rich! Well reasonably rich, there wasn't that much treasure down there.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Mostly famous for returning from where no man had gone before.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Being a man, I had to wait outside.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: In time I started an order of female knights. Men have no monopoly on honor or bravery and we like to remind them of that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It's rewarding work, questing to right wrongs and keep the land safe.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I became a master thief. Sadly not famous because famous thieves don't last long. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Still I made a habit of attending royal parties as a lady of distinction and leaving richer than I arrived.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Despite my humble origins I became an archmage. I figured the best thing to do was to spread the knowledge of magic through out the world.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: That way others could have the same chance I did.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Yeah we made a habit of looking through there to make sure that nothing too dangerous got in.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Despite our success our group is always ready for the next adventure. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I look forward to whatever comes next, confident we can handle it together.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Perhaps in time I will see if what Myrtle said was true, and if I am a queen ant.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Narrator: The end (Excellence in a new form)[BLOCK][CLEAR]") ]])
  fnCutsceneBlocker()
  fnInstruction([[ MapM_BackToTitle() ]])
    --LM_ExecuteScript(LM_GetCallStack(0), "Debug")





elseif(sTopicString == "Man") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
--To become a boy again:
      if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Irene:[BACKGROUND|Null][MOVECHAR|Irene|Middle-HfBody|Teleport][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I resolved to return to being to a man. [BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Irene: I could tell the others mostly felt the same way.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Irene: [BACKGROUND|Null][MOVECHAR|Irene|Middle-HfBody|Teleport] [ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody] [ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body] [ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I resolved to try and return to being a woman. It's not perfect but little in life is.[BLOCK][CLEAR]") ]])
    	fnInstruction([[ WD_SetProperty("Append", "Irene: I could tell the others were looking to reclaim their manhood.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I wasn't totally on board so I didn't speak up.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: We began questing for a way to get back to normal.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Laugh] Also money because it turns out edge of the teeth escapes leave you broke.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] We gained some fame as a band of female adventurers.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'll admit we played into the gimmick a little bit, but hey it got us jobs.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: This lead us to getting a job foiling an attack on a monastery full of nuns.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You would think bandits would attack a female monastery for a singular reason, but they did actually have a lot of religious valuables.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Also an impressive distillery and medicine making operation.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We succeeded and were richly rewarded by the nuns. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Warm meals and a soft beds. Better reward than most of the time.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We took it as time to rest and relax. Also to get used to our new forms.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] I remember Ron convinced the nuns to make him a lot of witch outfits to play dress up with.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: IT WASN'T DRESS UP. Sorry, it wasn't dress up.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Neutral] We weren't judging Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] As I recall didn't you get pretty close with some of the nuns.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Anything you would like to share Frederick?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I wouldn't like to cast aspersions on a nun.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Those two had fun, I learned a bit about medicine. Turns out setting bones isn't that far from blacksmithing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: They also had some wonderful handbooks on swordplay. Good thing Ron was on hand to help with the letters.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Books are wonderful, I wish you would treat them with a bit more respect.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Also they made me a female knight outfit and then insisted I wear it. It was embarrassing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: You looked quite striking as I recall.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] They were pretty hands off with me. I don't think they knew what to make of an ant-girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Offended] I don't think they didn't you like you. They just hadn't seen anything like you before.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Smirk] The fact that you kept doing that ant stare didn't help.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: It was a nice place to be. I spent most of my time adjusting my equipment, and getting a feel for my new reach. Also worked in the fields.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Four arms made me damn handy at gardening. Felt familiar to work the dirt for some reason.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: After about a week though, an order of female knights showed up.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Happy] They had heard of us which was always cool. A band of girls under the tutelage of Cohen.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Then they insisted that they join us as sisters in arms, which was troublesome.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] They were virgins to a woman. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'm not one to talk but they were embarrassingly horny.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We all love a bit of the good life. You don't go out and risk your life everyday without being a bit of bon vivant.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: But godsdamn.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: They seemed stuck in a teenage mindset, always talking about sex and speculating on dick sizes.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I think I let slip I'd done it and they hounded me about how it felt constantly. Thank goodness Frederick kept quiet.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Can't do what I do, if you don't know when to shut up. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Now if you would just be so wise more often.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Their leader had a complex about her age and spent pretty much every spare second with the pommel of her sword.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Not subtle about how she spent her time either.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: What made it worse was that they were actually very competent knights. Just so incredibly awkward they would never actually work up the courage to talk to a dude.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Making that worse was for some reason they assumed we'd all been girls from the start.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: By the time we cottoned on to that, it was too late to correct them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: The fact that we had been having a pastoral time with the nuns didn't help.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Still they were good people and having company on the road was always nice. Fearsome fighters too.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Neutral] This culminated in us driving slimes away from a remote mountain shrine.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: The shrine's lake was reputed to have purifying properties.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We were grimy and tired after the battle, so we didn't think too much of it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The knights invited us to bathe, and we joined them to not blow our cover.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We didn't recognize our mistake until we felt tingly all over. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I didn't know what we should do, still I figured if we kept distance we could maybe wait out the knights and make a dignified retreat.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: That plan failed pretty quick as the magics of the pool caused a great gout of steam to be raised around us.[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
         fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Man 2")
      return
    end
        if(sGender == "Girl") then
           fnInstruction([[ WD_SetProperty("Append", "Irene: I lost my balance a couple of times due to the loss of my second pair of arms and my abodomen.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] Thankfully the boys had all grown lower counterweights to help us keep our balance.[BLOCK][CLEAR]") ]])
         fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Man 3")
    end
      
      
    elseif(sTopicString == "Man 2") then
      
          --Change displayed gender to man
        fnInstruction([[ fnChangePlayerGender("Boy") ]])
    --Variables.
      
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
      
            
      	fnInstruction([[ WD_SetProperty("Append", "Irene: I almost fell over from how much height I gained, then I felt something poking me in the leg and I knew I was back to normal.[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Frederick: Unfortunately the knights all noticed that we had things poking us in the leg again.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Man 3")


 elseif(sTopicString == "Man 3") then
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
    local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
    WD_SetProperty("Hide")
	
	--Reboot the dialogue.
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
 
    fnInstruction([[ WD_SetProperty("Append", "Frederick: The knights all faked a scream of outrage and then began to eye us in a very particular way.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: I felt like how a deer being eyed by a wolf feels.[BLOCK][CLEAR]") ]])
      if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: When it was over we were all exhausted, triumphant, and very very sore. [BLOCK][CLEAR]") ]])
      else
      fnInstruction([[ WD_SetProperty("Append", "Alonso: When it was over we were all exhausted, triumphant, and very very sore. [BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Blush] I ended up spending my time making out with the girls who found they didn't like the male counterweight very much. [BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Smirk] We bid farewell to the knights after that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: There was a distinct feeling that they didn't really know how to handle relationships. That and they were heavily armed.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: [EMOTION|Irene|Neutral] A shame we had to leave them, they were great knights. I never felt so safe in combat.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It would of ended in tears Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Someone who spends so much time building up what their first relationship will be very disappointed when it doesn't turn out that way.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: [EMOTION|Irene|Smirk] Also a cornucopia of sharp things, and we were newly vulnerable.[BLOCK][CLEAR]") ]])
      if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: From there we resumed adventuring as dudes. [BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Irene: From there we resumed adventuring as we started out. [BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We eventually became an established group of adventurers.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: At least we didn't nearly die or almost get captured so often.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Sad] Cohen took us aside and said that he was proud of us, and that it was time for him to move on.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It was his way, gods we missed him though..[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] We didn't worry too much about him. When death comes for him it will have to bring war and maybe pestilence as backup.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Only chance is if they get him sleeping.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: He said he was proud of us and I always carried that with me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: In time we became famous throughout the land.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: My single handed rescue in the face of impossible odds became part of the poetic edda.[BLOCK][CLEAR]") ]])
     if(iAntIntimacy == 0.0) then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: Even if I did actually have a bit of help.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Blush] A surprising amount of sex made it in. Although it was presented as a doomed starcrossed love.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] Our finest moment was when we slew that marauding dragon. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I still don't know why I was chosen for the virgin sacrifice, I was as much a virgin sacrifice as Frederick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: As I recall you practically volunteered. Said something about wanting to finally slay a dragon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I may of volunteered.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I remember how you let it get close and then killed it with a single swipe to the neck. Everyone just stared at you.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It was quiet except for you hooting and cheering yourself.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Laugh] I'll admit that happened. Also that I looked totally badass.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] And we got to fight it's guards. Shame they refused surrender.[BLOCK][CLEAR]") ]])
      if(sGender == "Girl") then
        fnInstruction([[ WD_SetProperty("Append", "Irene: I became renowed throughout the land, and reviled for being a bad influence on girls hoping to become adventurers.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: In time I became a leader of men. I ended up leading an order of knights.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: The same knights from earlier I might add.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Every time after you joined them you looked very drained and the knights looked very satisfied.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I'd rather not talk about it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I became a master thief. I'd forewarn the targets just to keep the challenge high.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: And then rope us into your plans. I can't believe you have managed to keep your identity hidden.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You guys are just plain old indispensable.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Despite my humble origins I became an archmage. I figured the best thing to do was to spread the knowledge of magic through out the world.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: That way others could have the same chance I did[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Yeah we made a habit of looking through there to make sure that nothing too dangerous got in.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Despite our success our group is always ready for the next adventure. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I look forward to whatever comes next, confident we can handle it together.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
         fnInstruction([[ WD_SetProperty("Append", "Narrator: The End (A man among men)[BLOCK][CLEAR]") ]])
      else
         fnInstruction([[ WD_SetProperty("Append", "Narrator: The End (A woman among women)[BLOCK][CLEAR]") ]])
      end

    fnCutsceneBlocker()
    fnInstruction([[ MapM_BackToTitle() ]])
     --LM_ExecuteScript(LM_GetCallStack(0), "Debug")

	






--To aim for womanhood:
elseif(sTopicString == "Woman") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
        if(sGender == "Boy") then
          	fnInstruction([[ WD_SetProperty("Append", "Irene:[BACKGROUND|Null] [MOVECHAR|Irene|Middle-HfBody|Teleport][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I privately resolved to aim to be turned back into a human-girl or woman. I kept this to myself though.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Irene:[BACKGROUND|Null] [MOVECHAR|Irene|Middle-HfBody|Teleport][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I privately resolved to aim to be turned into a human-man or man. I kept this to myself though.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] I might of been a little afraid of losing my nerve.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] We would of been supportive no matter what[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Turning everybody back was way beyond my magic so we asked Cohen for a method to turn us back.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: He got that ambitious smile on his face.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We set off on a grand quest to find a mythical way to get back to normal.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'll be honest I don't remember what the cure was.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Surprised] Ummm, hell. Me neither.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: It was a wishing well I think?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] That sounds right. Hardly matters now.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] Most of what I remember about it was that it was very far away. So we had many days of riding.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Alonso: I was a bit envious of you actually, Isaac. Your chitin seemed to make you less saddle sore than us fleshy butts.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Alonso: I was a bit envious of you actually, Irene. Your chitin seemed to make you less saddle sore than us fleshy butts.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Irene: Not much padding back there, also I could feel my organs move in my abdomen. That's less fun than you might think.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Smirk] Good camping, whole places I'd never seen before.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Some beautiful vistas.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] Then one morning, the sun didn't rise. It got lighter sure, but we couldn't see the sun.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I still remember Cohen's expression. Scared me to my core.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: He took a deep breath and then told us that bad times were coming. He had heard of such things from his people and a sign like this always preceded a time of harrowing.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: What he said precisely was that the sun not rising meant that the ice giants were fighting the gods again. A thing which heralded the world possibly ending.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] I wonder if that's what it really was.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: No telling, too big. Besides I doubt we would do well against ice giants.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] I'd be interested in seeing if they melt.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] He introduced us to the mayor of a town called Ris. We became guards and troubleshooters.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I'll admit I didn't become an adventurer for steady work. But when we made it to the town people were growing skittish.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Offended] We are all farmer's children, we could tell the crops weren't growing well.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I didn't think much of it at the time but Cohen picked the town well.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: It had high thick walls and a large river ran through it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I never thought that the river would freeze[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Or that the walls would be overtaken.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: At the start things were calm. Everyone was nervous, waiting to see if the sky would finally clear.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We spent most of the time gathering food, buying it at a good price from farmers, and hauling it back to the city.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I never felt more like deserting. Glad I didn't though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: It's when the crops failed properly that things got dicey. People who lived where there hadn't been much food began to move.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: People tried to move south to where it was warmer, but then the fish dried up on the southern coast.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Then people swept the country side hunting for food. Before long there was a refugee camp outside the city walls.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Desperate hungry people, with nothing to do. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: The mayor kept sending us out there to make sure people were getting fed.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We spent most of the time trying to keep the peace. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: that's when we started to get popular I think. Which I sure didn't mind.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] It's because you two were so good at finding people who were stealing food.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: It was mostly Frederick, takes a thief to catch a thief.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Frederick: Nah, it was mostly Isaac, you were just really good at fingering the right person.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Frederick: Nah, it was mostly Irene, you were just really good at fingering the right person.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Irene: The nose knew, that and everyone seemed to be a bit afraid of me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Neutral] I like to think we made a difference.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We did the best we could, there is only so much acts of heroism can do when the land can't support human life.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Offended] As fall passed it got colder. Earliest snows I ever saw.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: By the dead of winter there were days when you couldn't tell the sun came up at all.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I remember the day the river froze over.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I remember how you guys made me axe through the ice to get water.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen kept us busy, shoveling snow gathering firewood and the like.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Ron finally proved his worth. When the rations ran short, he could make us hot water to drink.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Smirk] I did get an awful lot of offers for ummm, kisses on the cheek. I'm still not sure why.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Because you are warm to the touch Ron. And that will get you into a lot of beds at times.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Offended] I remember you were pretty miserable through all of that Isaac.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Chitin is good armor, and damn handy to have. Does not keep you very warm. I was shivering constantly. Luckily I avoided frost bite.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: For some reason I kept thinking that I needed to find a mass of people when I was cold.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: When spring came we still couldn't see the sun. By then people were marauding looking for food and warmth.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: This wasn't helped by the lords deciding to go raiding.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The queen's legion was busy trying to keep the capital secure and deliver food. So it was a golden opportunity for them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Thankfully raids are long way from a real war. We got called out many times to scare them off.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: It turns out that all you really have to do to scare off marauders is show that you are ready for a fight.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Your tendency towards fire works helped too Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Offended] Finally one of the lords decided to lay seige to the town. Where they got the food to do that I'll never know. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: We were all terrified. But where could we run to?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen wasn't afraid, but he knew the score. If they took the town they'd sack it and take all the food. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] We'd starve to death in the ruins.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Our only hope was to arm everyone we trusted, and a lot we didn't.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] For some reason I was in charge of training them. We didn't even draw straws, everyone just picked me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] Because you are the only with the charisma to pull it off.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Also the only one with the patience for it.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Also the only one patient enough. Besides they wouldn't listen to me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Finally the enemy army approached. They were as rough as we were. The campfires ringing the city were pretty though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick:[EMOTION|Irene|Offended] Only Ron would pick that.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] Cohen was charged with the defense of the city. We got placed where the fighting was going to be the worst. The main gate.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: My legs never shook so much, and I was never more worried of letting you guys down.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] You did a fine job Frederick. We never doubted you for a second.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Neutral] The fighting there was intense, if the gate fell the enemy could stream into the city. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Thankfully the enemy was just as tired as we were, but still it was close.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Sad] We were killing our countrymen, men and women not really different than us. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: Swept to war by hunger and those promising food. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: No glory in a fight like that. Just survival.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: More than once I remember you smelling in the enemy coming up the walls before we could see them.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Neutral] The antenna helped.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We fought and we fought and we fought. I didn't feel good about killing my countrymen but that's what it was.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I remember late one night, we were all back lit by Ron's fires. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: You looked like a terror with four arms. A whirling dervish of death and blood.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: The enemy looked up the ladders at all of us.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Then they decided that whatever was beyond the walls was not worth dying on top of them. And ran.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: In some places the walls were overrun and the enemy entered.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: But despite that, thanks to us the city held.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: To say that made us popular is an understatement.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I never got laid so much so often.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I know the rest of you did too.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: [EMOTION|Irene|Blush] No comment.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I would never...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Maybe...[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Neutral] Still once the siege lifted we had more problems. The refugees wanted in, the natives didn't want them in.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: We were called on constantly to mediate.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Food began to run out.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] Then finally one morning we could see the sun again.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I remember how the city sang and people danced.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: We were celebrated throughout the town, everyone called us the ant squad.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I thought you would make a better mascot Frederick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It'd be nice if all our problems were solved immediately but far from it. Makes stories sound better I suppose if they end all at once.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: There wouldn't be food until the crops were harvested.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Offended] That and for whatever reason some of the locals started agitating for the refugees to return immediately.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Just the way people are sometimes, Ron. They were afraid the refugees would take what they had.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: But we had all fought together.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: Sometimes you can't trust the people you fight with Ron.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Neutral] Too complicated for me. Magic is much simpler.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: We did our best to keep things from boiling over. Not quite as dangerous as holding the wall, but close.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We were convalescing from stopping more than a few fights, but we patrolled the refugee camp regardless.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Things were desperate there, but the people smiled in the sunlight.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: A little girl pulled my hand and I led everyone in following her. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: She led us to an old heavily tattooed man. He seemed like a normal old man to me.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: He claimed to be a healer of faith. Said he wanted to reward us for saving the town.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] His claim was that if we truly believed we would become our old selves again.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: I went first, making up my mind to only half believe and thereby become a girl.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: He laid his hands on me, and then suddenly I was taller and down a pair of arms.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: You were too busy blinking and trying to flex muscles you didn't have anymore to notice, but when he put his hands on Frederick nothing happened.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Frederick: I was unconvinced. Besides I look great like this.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Woman 3")
end
  --this jump allows the displayed gender swap
if(sGender == "Girl") then
   fnInstruction([[ WD_SetProperty("Append", "Irene: I went first, I made myself truly believe that I had been a man.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Woman 2")
  return
  end
  
  elseif(sTopicString == "Woman 2") then
  	
    

    --Change displayed gender to man
        fnInstruction([[ fnChangePlayerGender("Boy") ]])
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
        local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
     WD_SetProperty("Hide")
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
  
      fnInstruction([[ WD_SetProperty("Append", "Irene: He laid his hands on me, and then suddenly I was taller and down a pair of arms.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Alonso: You were too busy blinking and flexing your new muscles to notice, but when he put his hands on Frederick nothing happened.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Frederick: I was unconvinced. Besides I look great like this. I might of been inspired by Isaac.[BLOCK][CLEAR]") ]])
         fnCutsceneBlocker()
  LM_ExecuteScript(LM_GetCallStack(0), "Woman 3")






elseif(sTopicString == "Woman 3") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I guess you do Frederick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: In time the crops were planted, and we could resume adventuring.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We always kept the city in our heart though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Free drinks when we rolled in certainly helped.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I think they made us a statue eventually. I never felt so proud to have been an adventurer. To know I helped save the town fills me with pride to this day.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: They were good people.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: I never quite managed a respectable girl, but adventurers have very little use for respectable girls.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Besides there wasn't much chance of you winding up respectable with me around.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Laugh] True Frederick.[BLOCK][CLEAR]") ]])
else
  	fnInstruction([[ WD_SetProperty("Append", "Irene: After being an ant-girl being a man was just another change. I figured it out eventually, for a while I wasn't sure how they got anything done.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Yeah we had to help you through it a bit. Thankfully you had the two shining examples of manhood in Alonso and Ron around.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Heh, sure Frederick.[BLOCK][CLEAR]") ]])
end
		fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] We eventually became an established group of adventurers[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: At least we didn't nearly die or almost get captured so often [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Sad] Cohen took us aside and said that he was proud of us, and that it was time for him to move on.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: It was his way, lord knows we missed them though.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: We didn't worry too much about him though. Remember when he killed two men with one arrow?[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Yep, punched it through into the eye of the next guy.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: He said he was proud of us and I always carried that with me.[BLOCK][CLEAR]") ]])
  fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] In time we became famous throughout the land.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: My single handed rescue in the face of impossible odds became part of the poetic edda.[BLOCK][CLEAR]") ]])
      if(iAntIntimacy == 0.0) then
       	fnInstruction([[ WD_SetProperty("Append", "Irene: Even if I did actually have a bit of help.[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Blush] A surprising amount of sex made it in. Although it was presented as a doomed starcrossed love.[BLOCK][CLEAR]") ]])
      end
	fnInstruction([[ WD_SetProperty("Append", "Ron:[EMOTION|Irene|Neutral] Our finest moment was when we slew that marauding dragon. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: I still don't know why I was chosen for the virgin sacrifice, I was as much a virgin sacrifice as Frederick.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: As I recall you practically volunteered. Said something about wanting to finally slay a dragon.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Smirk] I may of volunteered.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: I remember how you let it get close and then killed it with a single swipe to the neck. Everyone just stared at you.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: It was quiet except for hooting cheering yourself.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Happy] I'll admit that happened. Also that I looked totally badass.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: And we got to fight it's guards. They were partisans from the empire to a man. Dragons like that always claim to be exiled, but it's a lie.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso:[EMOTION|Irene|Smirk] In time I became a leader of men. I ended up leading an order of knights.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Mostly because you can't say no to a pretty face.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I became a master thief. Sadly not famous because famous thieves don't last long. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Still I made a habit of attending royal parties as a lady of distinction and leaving richer than I arrived.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: Despite my humble origins I became an archmage. I figured the best thing to do was to spread the knowledge of magic through out the world.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Ron: That way others could have the same chance I did[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Alonso: Yeah we made a habit of looking through there to make sure that nothing too dangerous got in.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Despite our success our group is always ready for the next adventure. [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I look forward to whatever comes next, confident we can handle it together.[BLOCK][CLEAR]") ]])
      if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Narrator: A female touch need not be weak (The end)[BLOCK][CLEAR]") ]])
      else
        fnInstruction([[ WD_SetProperty("Append", "Narrator: A masculine touch need not be clumsy (The end)[BLOCK][CLEAR]") ]])
      end
    fnCutsceneBlocker()
    fnInstruction([[ MapM_BackToTitle() ]])
     --LM_ExecuteScript(LM_GetCallStack(0), "Debug")
	
	
	
	
	
-- |[=======Joke Ending======]|
--Alternative ending:
elseif(sTopicString == "Joke") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    local iAntRescue = VM_GetVar("Root/Variables/Characters/Scenario/iAntRescue", "N")
     local iAntIntimacy = VM_GetVar("Root/Variables/Characters/Scenario/iAntIntimacy", "N")
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnInstruction([[ WD_SetProperty("Show") ]])
	fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
  fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
     fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[BACKGROUND|Null] [MOVECHAR|Irene|Middle-HfBody|Teleport][ADDCHARPOS|Alonso|-1|Neutral|Middle+HfBody][ADDCHARPOS|Frederick|-1|Neutral|Middle-HfBody-Body][ADDCHARPOS|Ron|-1|Neutral|Middle+HfBody+body] I ultimately resolved to return to my original form. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Personally I thought this was a mistake, but hey it's your body not mine.[BLOCK][CLEAR]") ]])
     if(sGender == "Boy") then
       fnInstruction([[ WD_SetProperty("Append", "Ron: I was supportive, I don't mind being a girl but I prefer being a boy.[BLOCK][CLEAR]") ]])
else
  fnInstruction([[ WD_SetProperty("Append", "Ron: I was supportive, if that's how you were comfortable that's how you should be.[BLOCK][CLEAR]") ]])
end
	fnInstruction([[ WD_SetProperty("Append", "Alonso: I was whatever, but we asked Cohen if he knew a way back to normal. [BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: Cohen got that heroic look in his eyes and said he knew of a way to get back to normal. It involved a massive temple complex deep in the desert.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene: A grand temple to Echnida mother of monsters.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Ron: He told a lot of stories about getting back to normal but never about this temple so I was naturally curious.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Frederick: I heard temple in the desert and thought treasure myself.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Alonso: What exactly happened at that temple is a story for another time.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: TO BE CONTINUED IN:[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: Monsters of the desert 2: (Electric boogaloo)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: THE END (for now)[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Irene:[EMOTION|Irene|Angry] Wait are we actually doing this?[BLOCK][CLEAR]") ]])
  -- [EMOTION|Frederick|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Frederick: Fuck if I know.[BLOCK][CLEAR]") ]])
  --[EMOTION|Ron|Sad] 
	fnInstruction([[ WD_SetProperty("Append", "Ron: Given the current rate of writing I sincerely doubt it.[BLOCK][CLEAR]") ]])
  --[EMOTION|Alonso|Angry]
	fnInstruction([[ WD_SetProperty("Append", "Alonso:  Lazy author.[BLOCK][CLEAR]") ]])
	fnInstruction([[ WD_SetProperty("Append", "Narrator: To Be Continued (the end?)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    fnInstruction([[ MapM_BackToTitle() ]])
end