--[Blackout] Adapts whoosh to ant 0 tf, includes a short looping decision squence
--After the player blacks out, this script resumes.

--[Arguments]
--Make sure an argument was received.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Store.
local sTopicString = LM_GetScriptArgument(0)
--[Main Scene]
--Previous scripts rejoin here.
if(sTopicString == "Begin Scene") then
	--Establishes tranformation if male, how screwed the player is regardless
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
        --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    --NOTE TO SELF: after getting captured, neutral is the predominant emotion with almost no time spent idling in happy.
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Man, what did I do last night?)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Oh right, spider attack. Where am I?)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Everything swings forward as I grope blindly.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (If everything could stop moving, that would be great.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a grisly thump and the feeling of falling.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (What just happened?)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I hear a crashing sound and the falling feeling stops. I begin to roll.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Yet somehow, there is no pain. Instead there is only dizziness.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Ugh... It'll be a miracle if I keep my lunch down.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Then, the rolling stops.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Everything is quiet.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Where the actual hell am I? It's smooth and wet, but I can't feel anything like a door knob. It's tight but I can push against it.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I spend a few seconds pushing into the wall. I hear a cracking noise from outside. Now I really start beating on the wall.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The wall cracks and gives way. Twilight of a setting sun floods in.[BLOCK][CLEAR]") ]])
    --[BACKGROUND|ForestC]
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [BACKGROUND|ForestB] I sit up. A milky cocoon was my prison, now broken in half. The cocoon is leaking a fluid that looks like moonlight in gooey form.[BLOCK][CLEAR]") ]])
    --Should probably have just outline until they get up the hill, Hmm probably not for female start. I'll ask salty about it
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (When Cohen said adventurers wake up in strange places, I had no idea this is what he meant.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I stand. My legs are weak and I'm still a little dizzy, but I am alive.[BLOCK][CLEAR]") ]])
      if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Narrator: And yet, I still feel off. Uneasy in ways my addled brain hasn't caught up to yet.[BLOCK][CLEAR]") ]])
      end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I begin to pick my way up the hill I rolled down moments ago.[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] Why don't my clothes fit? My hands seem smaller, what's going on?[BLOCK][CLEAR]") ]])
        fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a clearing at the top of the hill with a small brook running through it.[BLOCK][CLEAR]") ]])
     --Irene is centerstage here on the Addchar
else
        fnInstruction([[ WD_SetProperty("Append", "Narrator:[ADDCHARPOS|Irene|-1|Neutral|Middle] There is a clearing at the top of the hill with a small brook running through it.[BLOCK][CLEAR]") ]])
end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The mangled corpse of a spider-girl sits here. It has been torn to pieces, limbs and blood scattered about.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Poor dear...)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: The sheer carnage that took place here is almost enough to turn my stomach. This is a brutality not seen in a fight. This was a predator tearing apart a prey.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I close the spider-girl's eyes. She may have been my enemy, but she deserves what little dignity I can give her.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: That done, I do a search of the clearing. I'm desperate, and the blood and viscera won't stop me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My equipment is gone, and there's nothing from this spider-girl. Crud.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (All I found was a pocket knife, flint, and a hand mirror.)[BLOCK][CLEAR]") ]])
    if(sGender == "Boy") then
      --make jump here so I can use gender swap code
      fnCutsceneBlocker()
      LM_ExecuteScript(LM_GetCallStack(0), "Female reveal")
        return
  end
   if(sGender == "Girl") then
       fnInstruction([[ WD_SetProperty("Append", "Narrator: I pull out my mirror and look at myself. I look much the same, although my clothes are totally soaked in the moonlight fluid.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: It gives my clothes a shiny, shimmery appearance. I can't say why they put it in the cocoon. They must of had a reason.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Further searches of the clearing don't produce anything useful. I sit at the edge of the brook and think for a moment.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I am hopelessly lost, tired and hungry.[BLOCK][CLEAR]") ]])
       end
    fnCutsceneBlocker()
    LM_ExecuteScript(LM_GetCallStack(0), "Decisions")
    --Decisions.
    
    
    elseif(sTopicString == "Female reveal") then
	--short sequence to facilitate the use of the gender swap function. 
  --Gender swap code which changes portrait and voice tick
      fnInstruction([[ fnChangePlayerGender("Girl") ]])
    --Variables.
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
        --Note: The "Confirm" dialogue option will have already constructed the dialogue for us.
    --NOTE TO SELF: after getting captured, neutral is the predominant emotion with almost no time spent idling in happy.
     WD_SetProperty("Hide")
      fnInstruction([[ WD_SetProperty("Show") ]])
      fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
      fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My clothes feel too tight in some spots and too loose in others. I had better check myself.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Smirk] (Before I wreck myself.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] You knew what kind of jokes you were getting into before you started playing this game, right? They don't get better.[BLOCK][CLEAR]") ]])
        
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][ADDCHARPOS|Irene|-1|Neutral|Middle] (My chest is swollen and my hips feel larger.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (...)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Uh oh.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: A quick private check shows that I am down a private.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: I pull out my mirror and look at myself. Somehow, I doubt what I see in the mirror. I run to the brook and check my reflection in it.[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [ADDCHARPOS|Irene|-1|Neutral|Middle](I don't look like myself... but at least I'm good looking.)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Why did they turn me into a girl? Was it on purpose?)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (...)[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac][EMOTION|Irene|Blush] (Maybe they're really kinky...)[BLOCK][CLEAR]") ]])
        --[EMOTION|Irene|Special]
      fnInstruction([[ WD_SetProperty("Append", "Narrator:[EMOTION|Irene|Neutral] Now I am certain I am lost, tired, hungry, and turned into a girl.[BLOCK]") ]])
      fnCutsceneBlocker()
      LM_ExecuteScript(LM_GetCallStack(0), "Decisions")
    
    
    
    -- |[======= Decision tree=====]|
    --coping with being screwed decisions
elseif(sTopicString == "Decisions") then
      fnInstruction([[ WD_SetProperty("Show") ]])
      fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
      fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
      fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
      fnInstruction([[ WD_SetProperty("Append", "Narrator: Now what?[BLOCK]") ]])
      fnInstruction([[ WD_SetProperty("Activate Decisions") ]])
      local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Masturbate\", " .. sDecisionScript .. ", \"Aaaac\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"This must be a bad dream...\",  " .. sDecisionScript .. ", \"Dream\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Survey the surroundings\",  " .. sDecisionScript .. ", \"Survey\") ")
      fnInstruction(" WD_SetProperty(\"Add Decision\", \"Get my mind right\",  " .. sDecisionScript .. ", \"Mind\") ")
      fnCutsceneBlocker()
    
--Masturbate.
elseif(sTopicString == "Aaaac") then
      local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
      WD_SetProperty("Hide")
	
    --Variables.
    --Reboot the dialogue.
      fnInstruction([[ WD_SetProperty("Show") ]])
      fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
      fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
      fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    if(sGender == "Boy") then
        fnInstruction([[ WD_SetProperty("Append", "Narrator:I glance down, still a bit surprised by what I look like.[BLOCK][CLEAR]") ]])
    end
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I guess I could, but this doesn't seem like the time. I'm lost, isolated, and only going to get weaker without food, water and shelter. )[BLOCK][CLEAR]") ]])
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Yeah, this is probably a bad idea. Let's get focused)[BLOCK][CLEAR]") ]])
      fnCutsceneBlocker()
    
    local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Decisions\")"
    fnInstruction(sString)
        

--This is all just a bad dream right?
elseif(sTopicString == "Dream") then
    local sGender = VM_GetVar("Root/Variables/Characters/Player/sGender", "S")
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Smirk] I try to smile to myself.[BLOCK][CLEAR]") ]])
  if(sGender == "Boy") then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My friends routed in skirmish, Cohen forced back? Being turned into a girl? This must all be a bad dream.)[BLOCK][CLEAR]") ]])
  else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (My friends routed in skirmish, Cohen forced back? Being abducted? This must all be a bad dream.)[BLOCK][CLEAR]") ]])    
  end
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'll just pinch myself and wake up safe and sound)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: A some pinching and a bit of groping does not render me safe back into bed.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Nope, this is really happening.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad](Hell)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Decisions\")"
    fnInstruction(sString)


--Survey surroundings
elseif(sTopicString == "Survey") then
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I take a deep breath and try to take in my surroundings calmly. [BLOCK][CLEAR]") ]])
    
    -- [Neutral]
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (I'm just lost in an extremely dangerous forest with no gear, no food and no real hope of rescue.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Sad] (Fuck)[BLOCK][CLEAR]") ]])
    
    --[sad/mad]
    fnInstruction([[ WD_SetProperty("Append", "Narrator: There is a noise in the distance and I spring to face it. Moments pass as I sweat nervously.[BLOCK][CLEAR]") ]])
  --[EMOTION|Irene|Special] 
     if(sGender == "Boy") then
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (This is it, this is where I die. Oh god, even if I survive how could I face my family? I don't look like myself.)[BLOCK][CLEAR]") ]])
else
      fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (This is it, this is where I die. Oh god, even if I survive how could I face my family? I've gotten everyone else killed)[BLOCK][CLEAR]") ]])
end
    fnInstruction([[ WD_SetProperty("Append", "Narrator: Terror black as midnight threatens and then I take a deep breath.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I need to get my mind right.[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    
    local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Mind\")"
    fnInstruction(sString)
  
	--Go to: get your mind right

--Get your mind right
elseif(sTopicString == "Mind") then
    WD_SetProperty("Hide")
    fnInstruction([[ WD_SetProperty("Show") ]])
    fnInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
    fnInstruction([[ WD_SetProperty("Visual Novel", true) ]])
    fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: [EMOTION|Irene|Neutral] I take a deep breath and focus inward[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Cohen often preached the virtues of staying calm and getting your mind right for the situation.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (He told a lot of stories about how keeping calm and staying focused saved his life.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Narrator: I repeat his mantra: [BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Fear is the mind killer.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: Fear is the little death that brings total oblivion.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I shall face my fear.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: I shall allow it pass through and around me.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Irene: When it is gone only I will remain.[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] (Right; important thoughts to the front. Unimportant thoughts to back.)[BLOCK][CLEAR]") ]])
    fnInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Isaac] [EMOTION|Irene|Happy] (Situation is not hopeless so long as I'm breathing.)[BLOCK][CLEAR]") ]])
    fnCutsceneBlocker()
    
    local sString = "LM_ExecuteScript(\""..LM_GetCallStack(0).."\", \"Exunt\")"
    fnInstruction(sString)
  
elseif(sTopicString == "Exunt") then
    LM_ExecuteScript(gsRoot .. "Scenario 100 The Caravan/210 choices and finding mushrooms.lua", "Begin Scene")

end