--[Monsters of the Forest Audio Routing]
--Loads audio files needed by Monsters of the Forest.

--[Variables]
local sBasePath = fnResolvePath()

--[Registration Function]
--Loads music with set loops.
local function fnRegisterMusic(psMusicName, psPath, pfLoopStart, pfLoopEnd)
	
	--Arg check.
	if(psMusicName == nil) then return end
	if(psPath == nil) then return end
	
	--If the pfLoopStart and/or pfLoopEnd are nil, then this doesn't loop.
	if(pfLoopStart == nil or pfLoopEnd == nil) then
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath)
		
	--Otherwise, register with looping.
	else
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath, pfLoopStart, pfLoopEnd)
	end

end

--[Registration]
--Fixed Run Tracks
--fnRegisterMusic("Doll|BattleTheme",      sBasePath .. "DollManorMusic/BattleTheme.ogg", 1.000, 58.000)

--[SFX for Doll Manor Only]
AudioManager_Register("MOTF|Voice|Alonso",     "AsSound", "AsSample", sBasePath .. "Voices/Alonso.ogg")
AudioManager_Register("MOTF|Voice|Cohen",      "AsSound", "AsSample", sBasePath .. "Voices/Cohen.ogg")
AudioManager_Register("MOTF|Voice|Frederick",  "AsSound", "AsSample", sBasePath .. "Voices/Frederick.ogg")
AudioManager_Register("MOTF|Voice|Handmaiden", "AsSound", "AsSample", sBasePath .. "Voices/Handmaiden.ogg")
AudioManager_Register("MOTF|Voice|IreneF",     "AsSound", "AsSample", sBasePath .. "Voices/IreneF.ogg")
AudioManager_Register("MOTF|Voice|IreneM",     "AsSound", "AsSample", sBasePath .. "Voices/IreneM.ogg")
AudioManager_Register("MOTF|Voice|Ron",        "AsSound", "AsSample", sBasePath .. "Voices/Ron.ogg")
AudioManager_Register("MOTF|Voice|SpiderA",    "AsSound", "AsSample", sBasePath .. "Voices/SpiderA.ogg")
AudioManager_Register("MOTF|Voice|SpiderB",    "AsSound", "AsSample", sBasePath .. "Voices/SpiderB.ogg")
AudioManager_Register("MOTF|Voice|SpiderC",    "AsSound", "AsSample", sBasePath .. "Voices/SpiderC.ogg")
