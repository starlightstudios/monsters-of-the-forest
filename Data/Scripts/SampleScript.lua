--[Sampler]
--Not an actual script meant to be executed. Contains a variety of useful code snippets.

--[Argument Stuff]
--Use the argument checker to make sure the args are valid. Use LM_GetScriptArgument to get the args.
-- Remember, args count from zero!
if(fnArgCheck(1) == true) then
	sArgument = LM_GetScriptArgument(0)
	iArgument = LM_GetScriptArgument(1, "N")
end

--[ ================================== Copyable Divider to 100 ================================== ]
--[Examination]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Examination.)") ]])
fnCutsceneBlocker()

--[Dialogue with an NPC]
TA_SetProperty("Face Character", "PlayerEntity")
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

--[Minor Dialogue]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[VOICE|Mei] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

--[Major Dialogue]
--Function
fnStandardMajorDialogue()

--Or...
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Christine", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Sophie", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

--Or...
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

--Or...
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogueOpposite)
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
fnInstruction([[ WD_SetProperty("Restore Previous Actors") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Christine:[E|Neutral] Dialogue goes here.[BLOCK][CLEAR]") ]])
fnCutsceneBlocker()

--[Scene]
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/Portraits/Combat/Mei_Human") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Dogs![BLOCK][CLEAR]") ]])

--[Fading]
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

--[Useful Tags]
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral][VOICE|MercF] Dialogue goes here.[SOUND|World|TakeItem][BLOCK][CLEAR]") ]])

--[Camera Events]
Cutscene_CreateEvent("CameraEvent", "Camera")
	CameraEvent_SetProperty("Max Move Speed", 5.0)
	CameraEvent_SetProperty("Focus Actor ID", 0)
	CameraEvent_SetProperty("Focus Actor Name", "Mei")
	CameraEvent_SetProperty("Focus Position", (1.25 * gciSizePerTile), (1.50 * gciSizePerTile))
DL_PopActiveObject()

--[Movement]
--Baseline.
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Move To", (1.25 * gciSizePerTile), (1.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Teleport To", (1.25 * gciSizePerTile), (1.50 * gciSizePerTile))
DL_PopActiveObject()
fnCutsceneBlocker()

--Function versions.
fnCutsceneMove("Mei", 1.25, 1.50)
fnCutsceneMove("Mei", 1.25, 1.50, 1.00) --Specifies the speed.
fnCutsceneMoveFace("Mei", 1.25, 1.50, 0, 1) --Specifies the facing.
fnCutsceneMoveFace("Mei", 1.25, 1.50, 0, 1, 1.00) --Specifies the facing and speed.
fnCutsceneFace("Florentina", 0, 1)
fnCutsceneFaceTarget("Christine", "Sophie")
fnCutsceneTeleport("Mei", 1.25, 1.50)

--[Change Facing]
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Face",  0, 1)
DL_PopActiveObject()
fnCutsceneBlocker()

--[Special Frames]
Cutscene_CreateEvent("ActorEvent", "Actor")
	ActorEvent_SetProperty("Subject Name", "Mei")
	ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Quick version.
fnCutsceneSetFrame("Mei", "Crouch")

--[Timing]
--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()

--[Dialogue Decisions]
--Setup.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Dogs or cats?[BLOCK]") ]])

--Decision script is this script. It must be surrounded by quotes.
local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Dogs\", " .. sDecisionScript .. ", \"Dogs\") ")
fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Cats\",  " .. sDecisionScript .. ", \"Cats\") ")
fnCutsceneBlocker()

--[Post Decision]
if(sTopicName == "Dogs") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Mei", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Alraune", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Mei:[E|Neutral] Yep.[BLOCK][CLEAR]") ]])
end

--[Party Folding]
--Move everyone onto the leader following the walk logic. Used at the start of cutscenes to make sure
-- characters don't get stuck on corners.
fnCutsceneMergeParty()
fnCutsceneBlocker()

--Move everyone onto the leader, and call the fold automatically.
fnAutoFoldParty()
fnCutsceneBlocker()

--Version where everyone is moved manually onto the party leader.
fnCutsceneInstruction([[ AL_SetProperty("Fold Party") ]])
fnCutsceneBlocker()

--[Animating with Layers]
AL_SetProperty("Set Layer Disabled", sLayerName, bFlag)
fnCutsceneLayerDisabled(sLayerName, bIsDisabled) --In-cutscene stride

--[Change Actor Collision Flag]
--Useful during cutscenes when actors might walk through one another.
TA_ChangeCollisionFlag("Sophie", true)

--[Map Transition]
--Scene transit. MAPNAME can be LASTSAVE to go to the last save point the player used.
fnCutsceneInstruction([[ AL_BeginTransitionTo("MAPNAME", gsRoot .. "Chapter1Scenes/Defeat_Cultists/Scene_PostTransition.lua") ]])
fnCutsceneInstruction([[ AL_BeginTransitionTo("MAPNAME", "FORCEPOS:10.0x12.0x0") ]])
fnCutsceneBlocker()

--Exits using auto-doors, access is denied.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "[SOUND|World|AutoDoorFail] Access denied.") ]])
fnCutsceneBlocker()

--Auto door SFX.
AudioManager_PlaySound("World|AutoDoorOpen")

--Door sample, with face-changer.
if(sObjectName == "ToHydroponicsBLft") then
    gi_Force_Facing = gci_Face_South
    AudioManager_PlaySound("World|AutoDoorOpen")
    AL_BeginTransitionTo("RegulusBiolabsHydroponicsB", "FORCEPOS:4.0x7.0x0")
end

--Ladders.
AudioManager_PlaySound("World|ClimbLadder")

--[Topics]
WD_SetProperty("Unlock Topic", "Rilmani", 1)

--[NPC Creation]
TA_Create("NPCName")
	TA_SetProperty("Position", 10, 10)
	TA_SetProperty("Facing", gci_Face_South)
	TA_SetProperty("Clipping Flag", true)
	TA_SetProperty("Activation Script", gsRoot .. "CharacterDialogue/TranEquipVendor/Root.lua")
	fnSetCharacterGraphics("Root/Images/Sprites/Doll/", false)
DL_PopActiveObject()
fnSpecialCharacter("Florentina", "Alraune", -100, -100, gci_Face_South, false, nil)

--Preset spawners.
fnSpawnNPCPattern("Golem", "A", "F")
fnStandardNPCByPosition("Isabelle")

--[Visual Novel Stuff]
--These are tags usable only in visual-novel mode.
[MOVECHAR|Charname|Formula|Movetype]
Where: Charname is the base name (not an alias) of the character
Formula is the location to move to. It can include constants, character names, or alpha constants.
Movetype is one of "Teleport", "Linear", "QuadIn", "QuadOut", or "QuadInOut". These describe how the character will move.

An alternate tag is:
[MOVECHARTIME|Charname|Formula|Movetype|Ticks]
Where the amount of time needed is specified. The default is 30 ticks.

The constants that can be used in the formula are:
'Body' which is roughly the width of a characters body.
'HfBody' is half the width of a body. Used for centering characters.
'Middle' which is the middle of the screen.
'LEdge' which is always 0, representing the leftmost edge of the screen.
'REdge' which is the virtual canvas width, representing the rightmost edge of the screen.

Examples:
[MOVECHAR|Irene|Irene-Body+150+Body|QUADINOUT]
[MOVECHARTIME|Irene|Irene-Body+150+Body|QUADINOUT|60]
